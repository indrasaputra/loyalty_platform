module.exports = {
  
    list: function(req, res) {
    var category_id = (typeof req.body.category_id != 'undefined') ? req.body.category_id : '';
    var limit       = (typeof req.body.limit != 'undefined') ? req.body.limit : 10;
    var offset      = (typeof req.body.offset != 'undefined') ? req.body.offset : 0;
    if(token != ''){
      jwt.verify(token, apikey, (err, data) => {
        if(err){
          response.status_401("Failed", "Unauthorized", function(cb){
          res.json(cb)
          });
        }else{ 
          member.list(decoded.payload.cif, 1, 0, function(cif){
            if(cif.length > 0){
              category.list(category_id, limit, offset, function(rows){
                if(rows.length > 0){
                  response.status_200("Success", rows, function(cb){
                  res.json(cb)
                  });
                }else{
                  response.status_204("Failed", "Data not found", function(cb){
                  res.json(cb)
                  }); 
                }
              });
            }else{
              response.status_401("Failed", "Profile not found", function(cb){
              res.json(cb)
              });  
            }
          });
        }
      });
    }else{
      response.status_401("Failed", "Token null", function(cb){
      res.json(cb)
      }); 
    }
    },

    create: function(req, res){
        var name= (typeof req.body.name != 'undefined') ? req.body.name : '';
        category.get_name(name, function(rows){
            console.log("rows "+ rows.length);
            if(rows.length>0){
                response.status_204("Failed", "Name already taken", function(cb){
                    res.json(cb);
                    }); 
            }else{
                category.create(name, function(rows){
                    if(typeof rows.affectedRows != 'undefined' && (rows.affectedRows == 1 || rows.affectedRows == 0)){
                    response.status_200("Success", rows, function(cb){
                    res.json(cb);
                    });
                    }else{
                    response.status_204("Failed", "insert failed", function(cb){
                    res.json(cb);
                    }); 
                    }
                });
            }

        });
        
    },
};
module.exports = {
  merge: function() {
    var truncate="TRUNCATE TABLE M_voucher_list";
    var path = '/var/www/html/csv_storage/db_giift_list.csv'; 
    var sql = 
    "LOAD DATA LOCAL INFILE '"+path+"' INTO TABLE M_voucher_list FIELDS TERMINATED BY '|' LINES TERMINATED BY '`' "+
    "(@col1,@col2,@col3,@col4,@col5,@col6,@col7,@col8,@col9,@col10,@col11,@col12,@col13,@col14) "+
    "set dmo_id=@col1,type=1,last_update='" + moment().format('Y-M-D HH:mm:ss') + "'";
    // console.log("sql "+sql);
    pool.getConnection(function(err,connection){
        if (err) {
            return console.error(moment().format('Y-M-D HH:mm:ss.SSS')+' error: ' + err.message);
        }
        console.log(moment().format('Y-M-D HH:mm:ss.SSS')+'  voucher.js#merge Connected to the MySQL server Pool.');
        connection.query(truncate,function(err,rows){
            if (err) {
                console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' voucher.js#merge error execute query: ' + err.stack);
                return;
            }
            console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" voucher.js#merge truncate result == "+rows.affectedRows+" "+moment().format('Y-M-D h:mm:ss'));
            console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" voucher.js#merge truncate table done");
            connection.query(sql,function(err,rows){
                connection.release();
                if (err) {
                    console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' voucher.js#merge error executing query: ' + err.stack);
                    return;
                }
                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" voucher.js#merge insert result == "+rows.affectedRows+" "+moment().format('Y-M-D h:mm:ss'));
                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" voucher.js#merge insert table done");
                var sql1 = "INSERT INTO M_voucher_list (dmo_id, last_update) SELECT M_merchant_btn.dmo_id, M_merchant_btn.last_update FROM M_merchant_btn";
                pool.getConnection(function(err,connection){
                    if (err) {
                        return console.error(moment().format('Y-M-D HH:mm:ss.SSS')+' error: ' + err.message);
                    }
                    console.log(moment().format('Y-M-D HH:mm:ss.SSS')+' voucher.js#merge Connected to the MySQL server Pool.');
                    connection.query(sql1,function(err,rows){
                        connection.release();
                        if (err) {
                            console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' voucher.js#merge error executing query: ' + err.stack);
                            return;
                        }
                        response.status_200("Success", rows, function(cb){
                          res.json(cb)
                        });
                    });
                    connection.on('error', function(err) {      
                        console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' voucher.js#merge error in connection database, status 100: ' + err.stack);
                        return;     
                    });
                });
            });
        });
        connection.on('error', function(err) {      
            console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' voucher.js#merge error in connection database, status 100: ' + err.stack);
            return;     
        });
    });
  }, 
  create: function(req){
      var query= "INSERT INTO M_merchant_btn (dmo_id, type, name, img, value, price, points, unit, bonus, fees, tc, description, country, category, last_update) VALUES ('" + req.body.dmo_id + "','" + req.body.type + "','" + req.body.name + "','" + req.body.img + "','" + req.body.value + "','" + req.body.price + "','" + req.body.points + "','" + req.body.unit + "','" + req.body.bonus + "','" + req.body.fees + "','" + req.body.tc + "','" + req.body.description + "','" + req.body.country + "','" + req.body.category + "','" + moment().format('Y-M-D HH:mm:ss') + "')";
      var options = {sql: query};
          sql.proccess_query(options, function(rows){
              callback(rows);
          });
      
  },
    list: function(req, res) {
    var category_id = (typeof req.body.category_id != 'undefined') ? req.body.category_id : '';
    var limit       = (typeof req.body.limit != 'undefined') ? req.body.limit : 10;
    var offset      = (typeof req.body.offset != 'undefined') ? req.body.offset : 0;
    if(token != ''){
      jwt.verify(token, apikey, (err, data) => {
        if(err){
          response.status_401("Failed", "Unauthorized", function(cb){
          res.json(cb)
          });
        }else{ 
          member.list(decoded.payload.cif, 1, 0, function(cif){
            if(cif.length > 0){
              category.list(category_id, limit, offset, function(rows){
                if(rows.length > 0){
                  response.status_200("Success", rows, function(cb){
                  res.json(cb)
                  });
                }else{
                  response.status_204("Failed", "Data not found", function(cb){
                  res.json(cb)
                  }); 
                }
              });
            }else{
              response.status_401("Failed", "Profile not found", function(cb){
              res.json(cb)
              });  
            }
          });
        }
      });
    }else{
      response.status_401("Failed", "Token null", function(cb){
      res.json(cb)
      }); 
    }
    },

    create: function(req, res){
        var name= (typeof req.body.name != 'undefined') ? req.body.name : '';
        category.get_name(name, function(rows){
            console.log("rows "+ rows.length);
            if(rows.length>0){
                response.status_204("Failed", "Name already taken", function(cb){
                    res.json(cb);
                    }); 
            }else{
                category.create(name, function(rows){
                    if(typeof rows.affectedRows != 'undefined' && (rows.affectedRows == 1 || rows.affectedRows == 0)){
                    response.status_200("Success", rows, function(cb){
                    res.json(cb);
                    });
                    }else{
                    response.status_204("Failed", "insert failed", function(cb){
                    res.json(cb);
                    }); 
                    }
                });
            }

        });
        
    },
};
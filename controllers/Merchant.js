module.exports = {
  /*Function get Token*/
  list: function(req, res) {
    var category_id   = (typeof req.body.category_id != 'undefined') ? req.body.category_id : '';
    var limit         = (typeof req.body.limit != 'undefined') ? req.body.limit : 1000;
    var offset        = (typeof req.body.offset != 'undefined') ? req.body.offset : 0;
    if(token != ''){
      jwt.verify(token, apikey, (err, data) => {
        if(err){
          response.status_401("Failed", "Unauthorized", function(cb){
          res.json(cb)
          });
        }else{
          member.list(decoded.payload.cif, 1, 0, function(cif){
            if(cif.length > 0){
              merchant.list(category_id, limit, offset, function(rows){
                if(rows.length > 0){
                  response.status_200("Success", rows, function(cb){
                  res.json(cb)
                  });
                }else{
                  response.status_204("Failed", "Data not found", function(cb){
                  res.json(cb)
                  }); 
                }
              });
            }else{
              response.status_401("Failed", "Profile not found", function(cb){
              res.json(cb)
              });
            }
          });
        }
      });
    }else{
      response.status_401("Failed", "Token null", function(cb){
      res.json(cb)
      }); 
    }
  },
  list_by_item: function(req, res) {
    var merchant_id   = (typeof req.body.merchant_id != 'undefined') ? req.body.merchant_id : '';
    var limit         = (typeof req.body.limit != 'undefined') ? req.body.limit : 10;
    var offset        = (typeof req.body.offset != 'undefined') ? req.body.offset : 0;
    if(token != ''){
      jwt.verify(token, apikey, (err, data) => {
        if(err){
          response.status_401("Failed", "Unauthorized", function(cb){
          res.json(cb)
          });
        }else{
          member.list(decoded.payload.cif, 1, 0, function(cif){
            if(cif.length > 0){
              merchant.listbyid(merchant_id, limit, offset, function(rows){
                if(rows.length > 0){
                  response.status_200("Success", rows, function(cb){
                  res.json(cb)
                  });
                }else{
                  response.status_204("Failed", "Data not found", function(cb){
                  res.json(cb)
                  }); 
                }
              });
            }else{
              response.status_401("Failed", "Profile not found", function(cb){
              res.json(cb)
              });
            }
          });
        }
      });
    }else{
      response.status_401("Failed", "Token null", function(cb){
      res.json(cb)
      }); 
    }
  },
  create_item: function(req, res) {
    var category_id     = (typeof req.body.category_id != 'undefined') ? req.body.category_id : '';
    var merchant_name   = (typeof req.body.merchant_name != 'undefined') ?  req.body.merchant_name : '';
    var merchant_image  = (typeof req.body.merchant_image != 'undefined') ? req.body.merchant_image : '';

    var merchant_phone  = (typeof req.body.merchant_phone != 'undefined') ? req.body.merchant_phone : '';
    var merchant_email  = (typeof req.body.merchant_email != 'undefined') ? req.body.merchant_email : '';
    var merchant_address  = (typeof req.body.merchant_address != 'undefined') ? req.body.merchant_address : '';
    var merchant_description  = (typeof req.body.merchant_description != 'undefined') ? req.body.merchant_description : '';
    var status  = (typeof req.body.status != 'undefined') ? req.body.status : '';
    req.checkBody('category_id', "category id can't be empty and must as numeric").notEmpty().isInt();
    req.checkBody('merchant_name', "merchant name can't be empty ").notEmpty();
    req.checkBody('merchant_phone', "merchant phone can't be empty and must as numeric").notEmpty().isInt();
    req.checkBody('merchant_email', "merchant email can't be empty").notEmpty();
    var errors  = req.validationErrors();
    if (errors) {
      response.status_204("Failed", errors, function(cb){
      res.json(cb)
      });
    }else{
        merchant.create_item(merchant_name, merchant_image, merchant_phone, merchant_email, merchant_address, merchant_description, category_id, status,  function(rows){
          if(typeof rows.affectedRows != 'undefined' && (rows.affectedRows == 1 || rows.affectedRows == 0)){
            response.status_200("Success", "Insert success", function(cb){
            res.json(cb)
            });
          }else{
            response.status_204("Failed", "Insert failed", function(cb){
            res.json(cb)
            }); 
          }
        });
    }
  },
  create_category: function(req, res) {
    var category_name    = (typeof req.body.category_name != 'undefined') ? req.body.category_name : '';
    
    req.checkBody('category_name', "category name can't be empty ").notEmpty();
    var errors  = req.validationErrors();
    if (errors) {
      response.status_204("Failed", errors, function(cb){
      res.json(cb)
      });
    }else{
        // category.create(category_name,  function(rows){
        //   if(typeof rows.affectedRows != 'undefined' && (rows.affectedRows == 1 || rows.affectedRows == 0)){
        //     response.status_200("Success", "Insert success", function(cb){
        //     res.json(cb)
        //     });
        //   }else{
        //     response.status_204("Failed", "Insert failed", function(cb){
        //     res.json(cb)
        //     }); 
        //   }
        // });
        category.get_name(category_name, function(rows){
          console.log("rows "+ rows.length);
          if(rows.length>0){
              response.status_204("Failed", "Name already taken", function(cb){
                  res.json(cb);
                  }); 
          }else{
              category.create(category_name, function(rows){
                  if(typeof rows.affectedRows != 'undefined' && (rows.affectedRows == 1 || rows.affectedRows == 0)){
                    response.status_200("Success", rows, function(cb){
                    res.json(cb);
                    });
                  }else{
                    response.status_204("Failed", "insert failed", function(cb){
                    res.json(cb);
                    }); 
                  }
                });
          }
  
      });
    }
  },
};
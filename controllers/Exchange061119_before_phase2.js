module.exports = {
  /*Function get Token*/
  list: function(req, res) {
    var program_code  = (typeof req.body.program_code != 'undefined') ? req.body.program_code : '';
    var to_program    = (typeof req.body.to_program != 'undefined') ? req.body.to_program : '';
    if(token != ''){
      jwt.verify(token, apikey, (err, data) => {
        if(err){
          response.status_401("Failed", "Unauthorized", function(cb){
          res.json(cb)
          });
        }else{ 
          member.list(decoded.payload.cif, 1, 0, function(cif){
            if(cif.length > 0){
              exchange.list(program_code, to_program, function(rows){
                if(rows.length > 0){
                  response.status_200("Success", rows, function(cb){
                  res.json(cb)
                  });
                }else{
                  response.status_204("Failed", "Data not found", function(cb){
                  res.json(cb)
                  }); 
                }
              });
            }else{
              response.status_401("Failed", "Profile not found", function(cb){
              res.json(cb)
              });
            }
          });
        }
      });
    }else{
      response.status_401("Failed", "Token null", function(cb){
      res.json(cb)
      }); 
    }
  },
  proccess: function(req, res) {
    var program_code  = (typeof req.body.program_code != 'undefined') ? req.body.program_code : '';
    var to_program    = (typeof req.body.to_program != 'undefined') ? req.body.to_program : '';
    var point         = (typeof req.body.point != 'undefined') ? req.body.point : '';
    var cif           = (typeof req.body.cif != 'undefined') ? req.body.cif : '';
    var phone_tujuan  = (typeof req.body.phone_tujuan != 'undefined') ? req.body.phone_tujuan : '';
    var id_tujuan  = (typeof req.body.id_tujuan != 'undefined') ? req.body.id_tujuan : '';
    var code_tujuan  = (typeof req.body.code_tujuan != 'undefined') ? req.body.code_tujuan : '';
    var type = 'exchange';
    var category_name='';
    //get balck list
    member.get_blacklist_cif(decoded.payload.cif, function(rows){
      if(rows.length > 0){
        response.status_204("Failed", "Check Customer Service code 2", function(cb){
          res.json(cb);
      });
      }

    });
    //get category id  from M_program
    program.list_program_by_code(to_program, function(rows){
        var category_id = rows[0].category_id;
        console.log("category_id "+category_id);
        //get category name  from M_category
        category.get(category_id, function(rows){
            category_name = rows[0].name;
            console.log("var category_name = "+category_name);
        });
    });

    if(code_tujuan =='' && id_tujuan =='' &&  phone_tujuan ==''){
        response.status_204("Failed", "one of field id , code, phone tujuan can't be empty", function(cb){
            res.json(cb);
        });
    }

    //if gojek grab GJAC098
    if(phone_tujuan =='' ){
       if (to_program != 'GJAC098' || to_program != 'GRBAC098'){
            console.log("bukan gojek or grab");
       }else{
            req.checkBody('phone tujuan', "phone tujuan can't be empty and must as numeric").notEmpty().isInt();
       }
    }
    
    if(id_tujuan =='' ){
        //case garuda milesage
        if(to_program != 'GAAC098' || category_name != 'Airlines') {
            console.log("bukan Airlines");
        }else{
            req.checkBody('id tujuan', "id tujuan can't be empty ").notEmpty();
        }
       
    }

    if(parseInt(point) ==0 || parseInt(point) < 0){
      response.status_204("Failed", "point value not valid", function(cb){
        res.json(cb);
        });
        process.exit();
    }
    var errors  = req.validationErrors();
    if(token != ''){
      jwt.verify(token, apikey, (err, data) => {
        if(err){
          response.status_401("Failed", "Unauthorized", function(cb){
          res.json(cb)
          });
        }else{
            if (errors) {
                response.status_204("Failed", errors, function(cb){
                res.json(cb);
                });
              }else{
             
                member.list(decoded.payload.cif, 1, 0, function(rows){
                    if(rows.length > 0){
                        var member_list = rows;
                        if(member_list.length > 0){
                            userCIF = rows[0].cif;
                            userID=rows[0].member_id;
                            var type = 'exchange';
                           // var program_code = "BTNC098";
                            //var to_program = "GJAC098";
                            var new_current_poin = rows[0].point;//point di M_program_join dari query tidak valid ini dirubah get M_cif_total_point
                            //var id_tujuan="";
                            //var code_tujuan ="";
                            var cifAccGanda =false;
                          
                            
        
                            //check black list
                            member.get_blacklist_cif(userCIF, function(blacklist){
                                if(blacklist.length > 0){
                                    response.status_204("Failed", "Check Customer Service code 2", function(cb){
                                        res.json(cb);
                                        //callback(cb);
                                    });
                                }
                          
                              });
                            // =====get total point ====
                            coreModel.get_total_point(userCIF, function(total_point){
                                console.dir(total_point)
                                console.log("get total point "+ total_point[0].Total);
                                new_current_poin=total_point[0].Total;
                                var rek_point=0;
                                var ref_number=ref_number=Math.floor(100000 + Math.random() * 900000) //""
                                    console.log("get new_current_poin = "+ new_current_poin);
                                    if(new_current_poin>=500){
                                        if(new_current_poin < point){
                                            result = {program_name: program_code, to_program: to_program, ref_number: ref_number, exchange: {"point": point , "result":"","balance": new_current_poin, data_member: member_list}};
                                            response.status_204("Tingkatkan poin Anda untuk dapat melakukan penukaran poin",result , function(cb){ //"Failed Point yang Anda tukar melebihi point yang anda punya",result , function(cb){
                                                res.json(cb);
                                                //callback(cb);
                                            });
                                        }else{
                                            core.verifiedPoint(userCIF,point,rek_point, new_current_poin, program_code,to_program,type,phone_tujuan,userID,ref_number,member_list, function(rows){
                                                //callback(rows);
                                                res.json(rows);
                                                console.dir(" hasil callback "+rows)
                                                console.dir(rows)
                                            });
                                        }
                                    
                                    
                                    } else{
                                        if(new_current_poin == 0) {
                                            result = {program_name: program_code, to_program: to_program, ref_number: ref_number, exchange: {"point": point , "result":"","balance": new_current_poin, data_member: member_list}};
                                            response.status_204("Cek kembali jumlah poin Anda",result , function(cb){ //"Failed Point Anda tidak mencukupi",result , function(cb){
                                                res.json(cb);
                                                //callback(cb);
                                            });
                                        } else {
                                            result = {program_name: program_code, to_program: to_program, ref_number: ref_number, exchange: {"point": point , "result":"","balance": new_current_poin, data_member: member_list}};
                                            response.status_204("Tingkatkan poin Anda untuk dapat melakukan penukaran poin",result , function(cb){ //"Failed Point Anda tidak mencukupi",result , function(cb){
                                                res.json(cb);
                                                //callback(cb);
                                            });
                                        }
                                    }
                                   
                            });
        
                        }else{
                          response.status_204("Failed", "Member not found", function(cb){
                          //res.json(cb)
                          //console.log("failed"+res.json(cb));
                            console.dir("failed"+cb);
                            callback(cb);
                          }); 
                        }
                    }
                });
                
                //end test
              }
         
        }
      });
    }else{
      response.status_401("Failed", "Token null", function(cb){
      res.json(cb)
      }); 
    }
  },
  proccess_wsdl: function(req, res) {
    var program_code  = (typeof req.body.program_code != 'undefined') ? req.body.program_code : '';
    var to_program    = (typeof req.body.to_program != 'undefined') ? req.body.to_program : '';
    var point         = (typeof req.body.point != 'undefined') ? req.body.point : '';
    var cif           = (typeof req.body.cif != 'undefined') ? req.body.cif : '';
    var phone         = (typeof req.body.phone != 'undefined') ? req.body.phone : '';
    var type = 'exchange';
    req.checkBody('phone', "phone can't be empty and must as numeric").notEmpty().isInt();
    var errors  = req.validationErrors();
    if(token != ''){
      jwt.verify(token, apikey, (err, data) => {
        if(err){
          response.status_401("Failed", "Unauthorized", function(cb){
          res.json(cb)
          });
        }else{
            if (errors) {
                response.status_204("Failed", errors, function(cb){
                res.json(cb);
                });
              }else{
                member.list(decoded.payload.cif, 1, 0, function(cif){
                    if(cif.length > 0){
                      exchange.get(program_code, to_program, point, function(rows){
                        var exchange_list = rows;
                        member.list(decoded.payload.cif, 1, 0, function(rows){
                          var member_list = rows;
                          
                          console.log("member_list[0] "+member_list[0]);
                          console.log("member_list size "+member_list.length);
                          if(member_list.length > 0){
                            if(exchange_list.length > 0){
                              if(point == exchange_list[0].from_point){
                              var balance = 0;
                              }else{
                              var balance = point - exchange_list[0].from_point;
                              }
                              //update_point_to: function(program_id, point, member_id, action, callback)
                              var result    = {program_name: exchange_list[0].program_name, to_program: exchange_list[0].to_program_name, exchange: {"point": point, "result": exchange_list[0].to_point, balance: balance}};
                              log.update_point_to(exchange_list[0].to_program_id, exchange_list[0].to_point, member_list[0].member_id, 'plus', program_code, type, function(rows){
                                if(typeof rows.affectedRows != 'undefined' && (rows.affectedRows == 1 || rows.affectedRows == 0)){
                                  log.update_point_to(exchange_list[0].from_program_id, point, member_list[0].member_id, 'minus', to_program, type, function(rows){
                                    response.status_200("Success", result, function(cb){
                                    res.json(cb);
                                    });
                                  });
                                }
                              });
                            }else{
                              response.status_204("Failed", "Data not found", function(cb){
                              res.json(cb)
                              }); 
                            }
                          }else{
                            response.status_204("Failed", "User not found", function(cb){
                            res.json(cb)
                            });
                          }
                        });
                      });
                    }else{
                      response.status_401("Failed", "Profile not found", function(cb){
                      res.json(cb)
                      }); 
                    }
                  });
              }
         
        }
      });
    }else{
      response.status_401("Failed", "Token null", function(cb){
      res.json(cb)
      }); 
    }
  },
  listAll: function(req, res) {
    var program_code  = (typeof req.body.program_code != 'undefined') ? req.body.program_code : '';
    //var to_program    = (typeof req.body.to_program != 'undefined') ? req.body.to_program : '';
    if(token != ''){
      jwt.verify(token, apikey, (err, data) => {
        if(err){
          response.status_401("Failed", "Unauthorized", function(cb){
          res.json(cb);
          });
        }else{ 
          member.list(decoded.payload.cif, 1, 0, function(cif){
            if(cif.length > 0){
              exchange.listAll(program_code, function(rows){
                if(rows.length > 0){
                  response.status_200("Success", rows, function(cb){
                  res.json(cb);
                  });
                }else{
                  response.status_204("Failed", "Data not found", function(cb){
                  res.json(cb);
                  }); 
                }
              });
            }else{
              response.status_401("Failed", "Profile not found", function(cb){
              res.json(cb)
              });
            }
          });
        }
      });
    }else{
      response.status_401("Failed", "Token null", function(cb){
      res.json(cb)
      }); 
    }
  },
  
};
function generate(count, k) {
    var _sym = 'abcdefghijklmnopqrstuvwxyz1234567890'
    var str= '';

    for(var i = 0; i < count; i++) {
        str += _sym[parseInt(Math.random() * (_sym.length))];
        if(i==(count-1)){
            return str;
        }
    }

    //ini ambil dari db bahan untuk voucehr generator
    // base.getID(str, function(err, res) {
    //     if(!res.length) {
    //       k(str)                   // use the continuation
    //     } else generate(count, k)  // otherwise, recurse on generate
    // });
}
module.exports = {
  /*Function get Token*/
  /*
  todo hubungkan process dengan rule
  todo hubungkan member creation dengan poin expire
  */
    adjust_point_by_rule_code: function(req, res) {
        var rule_code         = (typeof req.body.rule_code != 'undefined') ? req.body.rule_code : '';
        var date_start         = (typeof req.body.date_start != 'undefined') ? req.body.date_start : '';
        var date_end         = (typeof req.body.date_end != 'undefined') ? req.body.date_end : '';
        req.checkBody('rule_code', "rule_code can't be empty").notEmpty();
        req.checkBody('date_start', "date_start can't be empty").notEmpty();
        req.checkBody('date_end', "date_end can't be empty").notEmpty();
        var errors  = req.validationErrors();
        if(errors){
            response.status_401("Failed", errors, function(cb){
                res.json(cb)
            });
        }else{
            coreModel.adjust_m_poin_history_by_rule_code(rule_code, date_start, date_end, function(rows){
                console.log("adjust m_poin_history:" + rows.affectedRows);
                if (rows.affectedRows>0) {
                    coreModel.adjust_rek_by_rule_code(rule_code, date_start, date_end, function(result){
                        console.log("adjust m_poin_history:" + result.affectedRows);
                        response.status_200("Success", result, function(cb){
                            res.json(cb);
                        });
                    });
                } else {
                    response.status_204("Failed", "No transaction on the specified rule code and date", function(cb){
                        res.json(cb);
                    }); 
                }
            });
        }
    },
    manual_burn_point: function(req, res) {
        core.manual_burn_point(function(result) {
            response.status_200("Success", "Burning point record: "+ result, function(cb){
                res.json(cb)
            });
        });
    },
    proccess_old: function(req, res) {
        var cif         = (typeof req.body.cif != 'undefined') ? req.body.cif : '';
        var point       = (typeof req.body.point != 'undefined') ? req.body.point : '';
        var point_type= (typeof req.body.point_type != 'undefined') ? req.body.point_type : '';
        var nominal = (typeof req.body.nominal != 'undefined') ? req.body.nominal : '';
        var description = (typeof req.body.description != 'undefined') ? req.body.description : '';
        var btn_transcode = (typeof req.body.btn_transcode != 'undefined') ? req.body.btn_transcode : '';
        var event = (typeof req.body.event != 'undefined') ? req.body.event : '';
        //var transcode = (typeof req.body.nominal != 'undefined') ? req.body.nominal : '';
        req.checkBody('point', "point can't be empty and must as numeric").notEmpty().isInt();
        req.checkBody('cif', "cif can't be empty and must as numeric").notEmpty().isInt();
        req.checkBody('nominal', "nominal can't be empty and must as numeric").notEmpty().isInt();
        var errors  = req.validationErrors();
        if(parseInt(point)==0 || parseInt(point<0)){
        response.status_204("Failed", "point value not valid", function(cb){
            res.json(cb);
            });
        }
        if (errors) {
        response.status_204("Failed", errors, function(cb){
        res.json(cb);
        });
        }else{
        //tambahakan kondisi expire & check rule
        member.list(cif, 1, 0, function(rows){
            var member_list = rows;
                if(member_list.length > 0){
                program.list_program_by_code(point_type, function(rows){
                    var program = rows;
                    if(rows.length > 0){
                        log.list(member_list[0].member_id, program[0].id, function(rows){
                        if(rows.length > 0){
                            log.total_poin(member_list[0].member_id, function(rows){
                            if(rows.length>0){
                                var new_current_poin = rows[0].point;
                                var poin_expire_date = moment.utc(rows[0].point_expire, "Y-M-D h:mm:ss ");
                                //console.log("log process: poin_expire_date "+poin_expire_date+" utc now "+moment().utc());
                                if(moment(poin_expire_date) <= moment() ) {
                                //  console.log("log process: point expired");
                                    //do some blocking to not continue the code
                                    point = -new_current_poin;
                                    log.update(program[0].id, member_list[0].member_id, point, nominal, description, function(rows){
                                    response.status_200("Success", rows, function(cb){
                                        res.json(cb);
                            
                                    });
                                    
                                    }); 
                                }else{
                                //console.log("log process: :  point valid not yet expire");
                                log.update(program[0].id, member_list[0].member_id, point, nominal, description, function(rows){
                                    response.status_200("Success", rows, function(cb){
                                    //res.json(cb);
                                    //program_id, member_id, point, nominal, description, event, btn_transcode, callback
                                    //log.event(program[0].id, member_list[0].member_id, point, nominal, description, event, btn_transcode, function(rows){
                                    log.event(point_type, member_list[0].member_id, point, nominal, description, event, btn_transcode, function(rows){ 
                                    // console.log("event start conte");
                                        response.status_200("Success", rows, function(cb){
                                        res.json(cb);
                                        });
                                        
                                    }); 
                                    });
                                
                                }); 
                                }
                            }

                            });
                            

                            
                        }else{
                            log.create(program[0].id, member_list[0].member_id, '', '', point, nominal, description, function(rows){
                                response.status_200("Success", rows, function(cb){
                                res.json(cb);
                                });
                            });
                        }
                        });
                    }else{
                        response.status_204("Failed", "Program not found", function(cb){
                        res.json(cb);
                        }); 
                    }
                });
                }else{
                response.status_204("Failed", "User not found", function(cb){
                    res.json(cb);
                });
                }
        });

        }
    },
    proccess: function(req, res) {
        var cif         = (typeof req.body.cif != 'undefined') ? req.body.cif : '';
        var point       = (typeof req.body.point != 'undefined') ? req.body.point : '';
        var point_type= (typeof req.body.point_type != 'undefined') ? req.body.point_type : '';
        var nominal = (typeof req.body.nominal != 'undefined') ? req.body.nominal : '';
        var description = (typeof req.body.description != 'undefined') ? req.body.description : '';
        var btn_transcode = (typeof req.body.btn_transcode != 'undefined') ? req.body.btn_transcode : '';
        var event = (typeof req.body.event != 'undefined') ? req.body.event : '';
        //var transcode = (typeof req.body.nominal != 'undefined') ? req.body.nominal : '';
        req.checkBody('point', "point can't be empty and must as numeric").notEmpty().isInt();
        req.checkBody('cif', "cif can't be empty and must as numeric").notEmpty().isInt();
        req.checkBody('nominal', "nominal can't be empty and must as numeric").notEmpty().isInt();
        var errors  = req.validationErrors();
        if(parseInt(point)==0 || parseInt(point<0)){
        response.status_204("Failed", "point value not valid", function(cb){
            res.json(cb);
            });
        }
        if (errors) {
        response.status_204("Failed", errors, function(cb){
        res.json(cb);
        });
        }else{
        //tambahakan kondisi expire & check rule
        member.list(cif, 1, 0, function(rows){
            var member_list = rows;
                if(member_list.length > 0){
                program.list_program_by_code(point_type, function(rows){
                    var program = rows;
                    if(rows.length > 0){
                        log.list(member_list[0].member_id, program[0].id, function(rows){
                        if(rows.length > 0){
                            log.total_poin_by_cif(member_list[0].cif, function(rows){
                            if(rows.length>0){
                                var new_current_poin = rows[0].total;;
                                log.update(program[0].id, member_list[0].member_id, point, nominal, description, function(rows){
                                    response.status_200("Success", rows, function(cb){
                                    //res.json(cb);
                                    //program_id, member_id, point, nominal, description, event, btn_transcode, callback
                                    //log.event(program[0].id, member_list[0].member_id, point, nominal, description, event, btn_transcode, function(rows){
                                    log.event(point_type, member_list[0].member_id, point, nominal, description, event, btn_transcode, function(rows){ 
                                    // console.log("event start conte");
                                        response.status_200("Success", rows, function(cb){
                                        res.json(cb);
                                        });
                                        
                                    }); 
                                    });
                                
                                }); 
                            }

                            });
                            

                            
                        }else{
                            log.create(program[0].id, member_list[0].member_id, '', '', point, nominal, description, function(rows){
                                response.status_200("Success", rows, function(cb){
                                res.json(cb);
                                });
                            });
                        }
                        });
                    }else{
                        response.status_204("Failed", "Program not found", function(cb){
                        res.json(cb);
                        }); 
                    }
                });
                }else{
                response.status_204("Failed", "User not found", function(cb){
                    res.json(cb);
                });
                }
        });

        }
    },
    redeem_history: function(req, res) {
        var cif         = (typeof req.body.cif != 'undefined') ? req.body.cif : '';
        var merchant_id   = (typeof req.body.merchant_id != 'undefined' && req.body.merchant_id != '') ? req.body.merchant_id : 0;
        if(token != ''){
            jwt.verify(token, apikey, (err, data) => {
            if(err){
                response.status_401("Failed", "Unauthorized", function(cb){
                res.json(cb)
                });
            }else{ 
                member.list(decoded.payload.cif, 1, 0, function(rows){
                    var member_list = rows;
                if(member_list.length > 0){
                    log.redeem_history(member_list[0].member_id, merchant_id, function(rows){
                    if(rows.length > 0){
                        response.status_200("Success", rows, function(cb){
                        res.json(cb);
                        });
                    }else{
                        response.status_204("Failed", "Data not found", function(cb){
                        res.json(cb)
                        }); 
                    }
                    });
                }else{
                    response.status_401("Failed", "Profile not found", function(cb){
                    res.json(cb)
                    });
                }
                });
            }
            });
        }else{
            response.status_401("Failed", "Token null", function(cb){
            res.json(cb)
            }); 
        }
    },
    redeem_history_by_user: function(req, res) {
        var cif         = (typeof req.body.cif != 'undefined') ? req.body.cif : '';
        //var merchant_id   = (typeof req.body.merchant_id != 'undefined' && req.body.merchant_id != '') ? req.body.merchant_id : 0;
        if(token != ''){
            jwt.verify(token, apikey, (err, data) => {
            if(err){
                response.status_401("Failed", "Unauthorized", function(cb){
                res.json(cb)
                });
            }else{ 
                member.list(decoded.payload.cif, 1, 0, function(rows){
                    var member_list = rows;
                if(member_list.length > 0){
                    log.redeem_history_by_user(member_list[0].member_id, function(rows){
                    if(rows.length > 0){
                        response.status_200("Success", rows, function(cb){
                        res.json(cb);
                        });
                    }else{
                        response.status_204("Failed", "Data not found", function(cb){
                        res.json(cb)
                        }); 
                    }
                    });
                }else{
                    response.status_401("Failed", "Profile not found", function(cb){
                    res.json(cb)
                    });
                }
                });
            }
            });
        }else{
            response.status_401("Failed", "Token null", function(cb){
            res.json(cb)
            }); 
        }
    },
    exchange_history: function(req, res) {
        var cif         = (typeof req.body.cif != 'undefined') ? req.body.cif : '';
        var program_id   = (typeof req.body.program_id != 'undefined' && req.body.program_id != '') ? req.body.program_id : 0;
        if(token != ''){
            jwt.verify(token, apikey, (err, data) => {
            if(err){
                response.status_401("Failed", "Unauthorized", function(cb){
                res.json(cb)
                });
            }else{ 
                member.list(decoded.payload.cif, 1, 0, function(rows){
                //console.log("cif"+decoded.payload.cif);
                    var member_list = rows;
                    //console.dir(member_list);
                if(member_list.length > 0){
                    log.exchange_history(member_list[0].member_id, program_id, function(rows){
                    if(rows.length > 0){
                        response.status_200("Success", rows, function(cb){
                        res.json(cb);
                        });
                    }else{
                        response.status_204("Failed", "Data not found", function(cb){
                        res.json(cb)
                        }); 
                    }
                    });
                }else{
                    response.status_401("Failed", "Profile not found", function(cb){
                    res.json(cb)
                    });
                }
                });
            }
            });
        }else{
            response.status_401("Failed", "Token null", function(cb){
            res.json(cb)
            }); 
        }
    },
    exchange_history_by_user: function(req, res) {
        var cif         = (typeof req.body.cif != 'undefined') ? req.body.cif : '';
        //var program_id   = (typeof req.body.program_id != 'undefined' && req.body.program_id != '') ? req.body.program_id : 0;
        if(token != ''){
            jwt.verify(token, apikey, (err, data) => {
            if(err){
                response.status_401("Failed", "Unauthorized", function(cb){
                res.json(cb)
                });
            }else{ 
                member.list(decoded.payload.cif, 1, 0, function(rows){
                    var member_list = rows;
                if(member_list.length > 0){
                    log.exchange_history_by_user(member_list[0].member_id, function(rows){
                    if(rows.length > 0){
                        response.status_200("Success", rows, function(cb){
                        res.json(cb);
                        });
                    }else{
                        response.status_204("Failed", "Data not found", function(cb){
                        res.json(cb)
                        }); 
                    }
                    });
                }else{
                    response.status_401("Failed", "Profile not found", function(cb){
                    res.json(cb)
                    });
                }
                });
            }
            });
        }else{
            response.status_401("Failed", "Token null", function(cb){
            res.json(cb)
            }); 
        }
    },
    history_by_user: function(req, res) {
        var cif         = (typeof req.body.cif != 'undefined') ? req.body.cif : '';
        //var program_id   = (typeof req.body.program_id != 'undefined' && req.body.program_id != '') ? req.body.program_id : 0;
        if(token != ''){
            jwt.verify(token, apikey, (err, data) => {
            if(err){
                response.status_401("Failed", "Unauthorized", function(cb){
                res.json(cb)
                });
            }else{ 
                member.list(decoded.payload.cif, 1, 0, function(rows){
                    var member_list = rows;
                if(member_list.length > 0){
                    log.history_by_user(decoded.payload.cif, function(rows){
                    if(rows.length > 0){
                        response.status_200("Success", rows, function(cb){
                        res.json(cb);
                        });
                    }else{
                        response.status_204("Failed", "Data not found", function(cb){
                        res.json(cb)
                        }); 
                    }
                    });
                }else{
                    response.status_401("Failed", "Profile not found", function(cb){
                    res.json(cb)
                    });
                }
                });
            }
            });
        }else{
            response.status_401("Failed", "Token null", function(cb){
            res.json(cb)
            }); 
        }
    },
    total_poin: function(req, res){
        var cif         = (typeof req.body.cif != 'undefined') ? req.body.cif : '';
        var point       = (typeof req.body.point != 'undefined') ? req.body.point : '';
        var program_code= (typeof req.body.program_code != 'undefined') ? req.body.program_code : '';
        if(token != ''){
            jwt.verify(token, apikey, (err, data) => {
            if(err){
                response.status_401("Failed", "Unauthorized", function(cb){
                res.json(cb)
                });
            }else{ 
                member.list(decoded.payload.cif, 1, 0, function(rows){
                    var member_list = rows;
                if(member_list.length > 0){
                    log.total_poin_cif(member_list[0].member_id, decoded.payload.cif,function(rows){
                    if(rows.length > 0){
                        response.status_200("Success", rows, function(cb){
                        res.json(cb);
                        });
                    }else{
                        response.status_204("Failed", "Data not found", function(cb){
                        res.json(cb)
                        }); 
                    }
                    });
                }else{
                    response.status_401("Failed", "Profile not found", function(cb){
                    res.json(cb);
                    });
                }
                });
            }
            });
        }else{
            response.status_401("Failed", "Token null", function(cb){
            res.json(cb)
            }); 
        }
    },
    exchange_report: function(req, res){
        var cif         = (typeof req.body.cif != 'undefined') ? req.body.cif : '';
        var from_date = (typeof req.body.from_date != 'undefined') ? req.body.from_date : '';
        var to_date = (typeof req.body.to_date != 'undefined') ? req.body.to_date : '';
        var program_code= (typeof req.body.program_code != 'undefined') ? req.body.program_code : '';
        var type = (typeof req.body.type != 'undefined') ? req.body.type : '';
        log.exchange_report(program_code, from_date, to_date,type, function(rows){
        if(rows.length > 0){
            response.status_200("Success", rows, function(cb){
            res.json(cb);
            });
        }else{
            response.status_204("Failed", "Data not found", function(cb){
            res.json(cb);
            }); 
        }
        });
    },
    redeem_report: function(req, res) {
        var cif         = (typeof req.body.cif != 'undefined') ? req.body.cif : '';
        var merchant_id   = (typeof req.body.merchant_id != 'undefined' && req.body.merchant_id != '') ? req.body.merchant_id : 0;
        var from_date = (typeof req.body.from_date != 'undefined') ? req.body.from_date : '';
        var to_date = (typeof req.body.to_date != 'undefined') ? req.body.to_date : '';
        //if(merchant_id != '' && merchant_id.isInt ){
            log.redeem_report(merchant_id, from_date, to_date, function(rows){
                if(rows.length > 0){
                response.status_200("Success", rows, function(cb){
                res.json(cb);
                });
                }else{
                response.status_204("Failed", "Data not found", function(cb){
                res.json(cb);
                }); 
                }
            });
    // }

    /*
    if(token != ''){
            jwt.verify(token, apikey, (err, data) => {
            if(err){
                response.status_401("Failed", "Unauthorized", function(cb){
                res.json(cb)
                });
            }else{ 
                member.list(decoded.payload.cif, 1, 0, function(rows){
                    var member_list = rows;
                if(member_list.length > 0){
                    log.redeem_report(merchant_id, from_date, to_date, function(rows){
                    if(rows.length > 0){
                        response.status_200("Success", rows, function(cb){
                        res.json(cb);
                        });
                    }else{
                        response.status_204("Failed", "Data not found", function(cb){
                        res.json(cb)
                        }); 
                    }
                    });
                }else{
                    response.status_401("Failed", "Profile not found", function(cb){
                    res.json(cb)
                    });
                }
                });
            }
            });
        }else{
            response.status_401("Failed", "Token null", function(cb){
            res.json(cb)
            }); 
        }
        */
        },
    /*
        rule code = rule yg bakal di relasi ke table lain
        rule type = "feeder_daily, feeder_monthly, event_spesial, event_other"
        rule_expire = numeric 1 ... n
        rule_expire_type = jenis satuan expire day, month, year
        program_id = program id dari program list
    */
    rule_create: function(req, res){

        var program_id   = (typeof req.body.program_id != 'undefined') ? req.body.program_id : '';
        var nominal = (typeof req.body.nominal != 'undefined') ? req.body.nominal : '';
        var point   = (typeof req.body.point != 'undefined') ? req.body.point : '';
        var rule_code   = "";//(typeof req.body.rule_code != 'undefined') ? req.body.rule_code : '';
        var rule_type   = (typeof req.body.rule_type != 'undefined') ? req.body.rule_type : '';
        var rule_expire = (typeof req.body.rule_expire != 'undefined') ? req.body.rule_expire : '';
        var rule_expire_type = (typeof req.body.rule_expire_type != 'undefined') ? req.body.rule_expire_type : '';
        var status = (typeof req.body.status != 'undefined') ? req.body.status : '';
        var transcode = (typeof req.body.transcode != 'undefined') ? req.body.transcode : '';
        req.checkBody('point', "point can't be empty and must as numeric").notEmpty().isInt();
        req.checkBody('rule_expire', "rule_expire can't be empty and must as numeric").notEmpty().isInt();
        req.checkBody('status', "status can't be empty and must as numeric").notEmpty().isInt();
        var errors  = req.validationErrors();

        if(parseInt(nominal)==0 || parseInt(nominal<0)){
        response.status_204("Failed", "nominal value not valid", function(cb){
            res.json(cb);
            });
        }
        if(parseInt(point)==0 || parseInt(point<0)){
        response.status_204("Failed", "point value not valid", function(cb){
            res.json(cb);
            });
        }
        if(parseInt(rule_expire)==0 || parseInt(rule_expire<0)){
        response.status_204("Failed", "rule_expire value not valid", function(cb){
            res.json(cb);
            });
        }
        if(parseInt(status)==0 || parseInt(status<0)){
        response.status_204("Failed", "status value not valid", function(cb){
            res.json(cb);
            });
        }

        if (errors) {
        response.status_204("Failed", errors, function(cb){
        res.json(cb);
        });
        }else{
        
        var utcMoment = moment().utc();
        switch(rule_type) {
            case "feeder_monthly":
                rule_code ="FM-"+utcMoment.valueOf();
                if(transcode == 'undefined' || transcode ==''){
                transcode = "FM";
                }else{
                transcode = transcode;//"BTN-"+transcode;
                }
            break;
            case "feeder_daily":
                rule_code ="FD-"+utcMoment.valueOf();
                if(transcode == 'undefined' || transcode ==''){
                transcode = "FD";
                }else{
                transcode = transcode;//"BTN-"+transcode;
                }
            break;
            case "event_other":
            rule_code ="EO-"+utcMoment.valueOf();
            transcode ="EO";
            break;
            case "event_spesial":
            rule_code ="ES-"+utcMoment.valueOf();
            transcode ="ES";
            break;
            default:
            //daily
            rule_code ="FD-"+utcMoment.valueOf();
            if(transcode == 'undefined' || transcode ==''){
                transcode = "FD";
            }else{
                transcode = transcode;//"BTN-"+transcode;
            }
        }

        

        log.rule_create(program_id, transcode, point, nominal, status, rule_code, rule_type, rule_expire, rule_expire_type, function(rows){
            
            //console.dir(rows);

            if(typeof rows.affectedRows != 'undefined' && (rows.affectedRows == 1 || rows.affectedRows == 0)){
            response.status_200("Success", "Insert success", function(cb){
            res.json(cb);
            });
            }else{
            response.status_204("Failed", "Insert failed", function(cb){
            res.json(cb);
            }); 
            }
        });
        }
        
        },
    /*
        rule code = rule yg bakal di relasi ke table lain
        rule type = "feeder_daily, feeder_monthly, event_spesial, event_other"
        rule_expire = numeric 1 ... n
        rule_expire_type = jenis satuan expire day, month, year
        program_id = program id dari program list
    */
    rule_update: function(req, res){

        var program_id   = (typeof req.body.program_id != 'undefined') ? req.body.program_id : '';
        var nominal = (typeof req.body.nominal != 'undefined') ? req.body.nominal : '';
        var point   = (typeof req.body.point != 'undefined') ? req.body.point : '';
        var rule_code   = (typeof req.body.rule_code != 'undefined') ? req.body.rule_code : '';
        var rule_type   = (typeof req.body.rule_type != 'undefined') ? req.body.rule_type : '';
        var rule_expire = (typeof req.body.rule_expire != 'undefined') ? req.body.rule_expire : '';
        var rule_expire_type = (typeof req.body.rule_expire_type != 'undefined') ? req.body.rule_expire_type : '';
        var status = (typeof req.body.status != 'undefined') ? req.body.status : '';
        var transcode = (typeof req.body.transcode != 'undefined') ? req.body.transcode : '';
        req.checkBody('point', "point can't be empty and must as numeric").notEmpty().isInt();
        req.checkBody('rule_expire', "rule_expire can't be empty and must as numeric").notEmpty().isInt();
        req.checkBody('status', "status can't be empty and must as numeric").notEmpty().isInt();
        req.checkBody('transcode', "transcode can't be empty ").notEmpty();
        var errors  = req.validationErrors();
        if(parseInt(nominal)==0 || parseInt(nominal<0)){
        response.status_204("Failed", "nominal value not valid", function(cb){
            res.json(cb);
            });
        }

        if(parseInt(program_id)==0 || parseInt(program_id<0)){
        response.status_204("Failed", "program_id value not valid", function(cb){
            res.json(cb);
            });
        }
        if(parseInt(point)==0 || parseInt(point<0)){
        response.status_204("Failed", "point value not valid", function(cb){
            res.json(cb);
            });
        }
        if(parseInt(rule_expire)==0 || parseInt(rule_expire<0)){
        response.status_204("Failed", "rule_expire value not valid", function(cb){
            res.json(cb);
            });
        }
        if(parseInt(status)==0 || parseInt(status<0)){
        response.status_204("Failed", "status value not valid", function(cb){
            res.json(cb);
            });
        }
        if(transcode == 'undefined' || transcode == ''){
        response.status_204("Failed", "transcode value not valid", function(cb){
            res.json(cb);
            });
        }
        if(rule_code == 'undefined' || rule_code == ''){
        response.status_204("Failed", "rule_code value not valid", function(cb){
            res.json(cb);
            });
        }
        if(rule_type == 'undefined' || rule_type == ''){
        response.status_204("Failed", "rule_type value not valid", function(cb){
            res.json(cb);
            });
        }
        if(rule_expire_type == 'undefined' || rule_expire_type == ''){
        response.status_204("Failed", "rule_expire_type value not valid", function(cb){
            res.json(cb);
            });
        }

        if (errors) {
        response.status_204("Failed", errors, function(cb){
        res.json(cb);
        });
        }else{
        /*
            var utcMoment = moment().utc();
            switch(rule_type) {
            case "feeder_monthly":
                rule_code ="FM-"+utcMoment.valueOf();
                if(transcode == 'undefined' || transcode ==''){
                transcode = "FM";
                }else{
                transcode = "BTN-"+transcode;
                }
                break;
            case "event_other":
                rule_code ="EO-"+utcMoment.valueOf();
                transcode ="EO";
                break;
            case "event_spesial":
                rule_code ="ES-"+utcMoment.valueOf();
                transcode ="ES";
                break;
            default:
            //daily
                rule_code ="FD-"+utcMoment.valueOf();
                if(transcode == 'undefined' || transcode ==''){
                transcode = "FD";
                }else{
                transcode = "BTN-"+transcode;
                }
            }

        */

        log.rule_update(program_id, transcode, point, nominal, status, rule_code, rule_type, rule_expire, rule_expire_type, function(rows){
            console.log("rule_code = "+rule_code);
            console.dir(rows);

            if(typeof rows.affectedRows != 'undefined' && (rows.affectedRows == 1 || rows.affectedRows == 0)){
            response.status_200("Success", "update success", function(cb){
            res.json(cb);
            });
            }else{
            response.status_204("Failed", "update failed", function(cb){
            res.json(cb);
            }); 
            }
        });
        }
    
        },
    rule_list: function(req, res){
        var offset = (typeof req.body.offset != 'undefined') ? req.body.offset : '';
        var limit = (typeof req.body.limit != 'undefined') ? req.body.limit : '';
        log.rule_list(offset, limit, function(rows){           
            if(rows.length > 0){
            response.status_200("Success", rows, function(cb){
            res.json(cb);
            });
            }else{
            response.status_204("Failed", "Data not found", function(cb){
            res.json(cb);
            }); 
            }         
        });
        },
    process_feed_daily: function(){
        //get from cron job check history
        //and execute to do the process
        //atau langsung ke log prosess tdk perlu karena sepesific processnya
        //console.log("process_feed_daily");
        
    

    },
    process_feed_monthly: function(req, res){

    },
    process_event_special: function(){
        var currdate=moment().format('YYYY-MM-DD');

        var check = moment(currdate, 'YYYY-MM-DD');
        var month = check.format('M');
        var day   = check.format('D');
        var year  = check.format('YYYY');
        var dob = ""+month+"-"+day;
        //console.log(month, day, year);
        //console.log("process_event_special");
        member.list_dob(dob, function(rows){
        if(rows.length >0){

        }else{

        }
        });
        
        
        
    },
    process_event_other: function(req, res){

    },
    news_list: function(req, res){
        var offset = (typeof req.body.offset != 'undefined') ? req.body.offset : '';
        var limit = (typeof req.body.limit != 'undefined') ? req.body.limit : '';
        log.news_list(offset, limit, function(rows){           
            if(rows.length > 0){
            response.status_200("Success", rows, function(cb){
            res.json(cb);
            });
            }else{
            response.status_204("Failed", "Data not found", function(cb){
            res.json(cb);
            }); 
            }         
        });
    },
    news_cat_list: function(req, res){
        var offset = (typeof req.body.offset != 'undefined') ? req.body.offset : '';
        var limit = (typeof req.body.limit != 'undefined') ? req.body.limit : '';
        log.news_cat_list(offset, limit, function(rows){           
            if(rows.length > 0){
            response.status_200("Success", rows, function(cb){
            res.json(cb);
            });
            }else{
            response.status_204("Failed", "Data not found", function(cb){
            res.json(cb);
            }); 
            }         
        });
    },
    news_by_cat: function(req, res){
        var cat_id = (typeof req.body.cat_id != 'undefined') ? req.body.cat_id : '';
        var offset = (typeof req.body.offset != 'undefined') ? req.body.offset : '';
        var limit = (typeof req.body.limit != 'undefined') ? req.body.limit : '';
        log.news_by_cat(cat_id,offset, limit, function(rows){           
            if(rows.length > 0){
            response.status_200("Success", rows, function(cb){
            res.json(cb);
            });
            }else{
            response.status_204("Failed", "Data not found", function(cb){
            res.json(cb);
            }); 
            }         
        });
    },
    news_by_id: function(req, res){
        var news_id = (typeof req.body.news_id != 'undefined') ? req.body.news_id : '';
        var offset = (typeof req.body.offset != 'undefined') ? req.body.offset : '';
        var limit = (typeof req.body.limit != 'undefined') ? req.body.limit : '';
        log.news_by_id(news_id,offset, limit, function(rows){           
            if(rows.length > 0){
            response.status_200("Success", rows, function(cb){
            res.json(cb);
            });
            }else{
            response.status_204("Failed", "Data not found", function(cb){
            res.json(cb);
            }); 
            }         
        });
    },

};
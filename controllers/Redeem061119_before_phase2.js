module.exports = {
    /*Function get Token*/
    voucher_history: function(req, res) {
        var member_id   = (typeof req.body.member_id != 'undefined' && req.body.member_id != '') ? req.body.member_id : '';
        var offset   = (typeof req.body.offset != 'undefined' && req.body.offset != '') ? req.body.offset : 0;
        var limit   = (typeof req.body.limit != 'undefined' && req.body.limit != '') ? req.body.limit : 10;
        var search   = (typeof req.body.search != 'undefined' && req.body.search != '') ? req.body.search : '';
        var order   = (typeof req.body.order != 'undefined' && req.body.order != '') ? req.body.order : 'latest';
        if(token != ''){
            jwt.verify(token, apikey, (err, data) => {
            if(err){
                response.status_401("Failed", "Unauthorized", function(cb){
                res.json(cb)
                });
            }else{
                member.list(decoded.payload.cif, 1, 0, function(cif){
                if(cif.length > 0){
                    //conditional mechant id 0 untuk get semua list
                    redeem.get_voucher_history(member_id, limit, offset, search.trim(), order, function(rows){
                    if(rows.length > 0){
                        //var result    = {rows};
                        var result = [];
                        rows.forEach(function(item) {
                            result.push(
                                {
                                    id: item.id,
                                    member_id: item.member_id,
                                    transcode_btn: item.transcode_btn,
                                    transcode_btn_poin: item.transcode_btn_poin,
                                    transcode_btn_date: item.transcode_btn_date,
                                    redeem_date: item.redeem_date,
                                    redeem_poin: item.redeem_poin,
                                    current_point: item.current_point,
                                    email_tujuan: item.email_tujuan,
                                    status: item.status,
                                    giift_dmo_id: item.giift_dmo_id,
                                    giift_name: item.giift_name,
                                    giift_order_id: item.giift_order_id,
                                    giift_status: item.giift_status,
                                    giift_value: item.giift_value,
                                    giift_number: core.decrypt(item.giift_number),
                                    giift_ccv: item.giift_ccv,
                                    giift_expiration: item.giift_expiration,
                                    giift_img: item.giift_img,
                                    giift_extra_login: item.giift_extra_login,
                                    giift_extra_password: item.giift_extra_password
                                }
                            )
                        });
                        response.status_200("Success", result, function(cb){
                            res.json(cb)
                        });
                    }else{
                        response.status_204("Failed", "No record found", function(cb){
                            res.json(cb)
                        }); 
                    }
                    });
                }else{
                    response.status_401("Failed", "Profile not found", function(cb){
                    res.json(cb)
                    }); 
                }
                });
            }
            });
        }else{
            response.status_401("Failed", "Token null", function(cb){
            res.json(cb)
            });
        }
    },
    list: function(req, res) {
      var category_id   = (typeof req.body.category_id != 'undefined' && req.body.category_id != '') ? req.body.category_id : 0;
      var merchant_id   = (typeof req.body.merchant_id != 'undefined' && req.body.merchant_id != '') ? req.body.merchant_id : 0;
      var program_code  = (typeof req.body.program_code != 'undefined') ? req.body.program_code : '0';
      if(token != ''){
        jwt.verify(token, apikey, (err, data) => {
          if(err){
            response.status_401("Failed", "Unauthorized", function(cb){
            res.json(cb)
            });
          }else{
            member.list(decoded.payload.cif, 1, 0, function(cif){
              if(cif.length > 0){
                  //conditional mechant id 0 untuk get semua list
                merchant.list(category_id, 100, 0, function(rows){
                  if(rows.length > 0){
                    var merchant = rows;
                    redeem.list(merchant_id, program_code, function(rows){
                      var result    = {merchant: merchant, redeem_list: {"data": rows}};
                      response.status_200("Success", result, function(cb){
                      res.json(cb)
                      });
                    });
                  }else{
                    response.status_204("Failed", "Merchant not found", function(cb){
                    res.json(cb)
                    }); 
                  }
                });
              }else{
                response.status_401("Failed", "Profile not found", function(cb){
                res.json(cb)
                }); 
              }
            });
          }
        });
      }else{
        response.status_401("Failed", "Token null", function(cb){
        res.json(cb)
        });
      }
    },
    proccess: function(req, res) {
      var merchant_id   = (typeof req.body.merchant_id != 'undefined') ? req.body.merchant_id : '';
      var program_code  = (typeof req.body.program_code != 'undefined') ? req.body.program_code : '';
      var point         = (typeof req.body.point != 'undefined') ? req.body.point : '';
      var category_id   = (typeof req.body.category_id != 'undefined') ? req.body.category_id : '';
      var cif           = (typeof req.body.cif != 'undefined') ? req.body.cif : '';
      var type = 'redeem';
      var phone_tujuan, id_tujuan, code_tujuan="";
      var normal_point=point;
      var phone =(typeof req.body.phone != 'undefined') ? req.body.phone : '';
     // var userCIF=decoded.payload.cif;
  
      member.get_blacklist_cif(decoded.payload.cif, function(rows){
          if(rows.length > 0){
              response.status_204("Failed", "Check Customer Service code 2", function(cb){
                  res.json(cb);
              });
          }else{
  
              if(token != ''){
              jwt.verify(token, apikey, (err, data) => {
                  if(err){
                      response.status_401("Failed", "Unauthorized", function(cb){
                      res.json(cb)
                      });
                  }else{
  
                      member.authentication_with_phone(phone, function(rows){ 
                          
                          //not yet
                          if(rows.length > 0 ){
                              var member_list = rows;
                              
                              var apiKey = 'NxoklkZDL0Cz8GrHAFYzViA8cVv16wP5';
                              var new_current_poin = rows[0].total_point;
                              var poin_expire_date = moment.utc(rows[0].point_expire, "Y-M-D h:mm:ss ");
                              var member_id =rows[0].member_id;
                              var userCIF = rows[0].cif;
                              //var totalp
  
                              //console.dir(rows);
                              console.log("length rows "+rows.length+ " new_current_poin "+new_current_poin);
                              //check black list
                              member.get_blacklist_cif(userCIF, function(rows){
                                  if(rows.length > 0){
                                      response.status_204("Failed", "Check Customer Service code 2", function(cb){
                                          //callback(cb);
                                          res.json(cb);
                                      });
                                  }
                                  coreModel.get_total_point(userCIF, function(total_point){
                                      console.dir(total_point)
                                      console.log("get total point "+ total_point[0].Total);
                                      new_current_poin=total_point[0].Total;
                                      normal_point=point;
                                      if(total_point.length>0){
              
                                          if(new_current_poin>=500){
                                              program.list_program_by_code(program_code, function(rows){
                                                  var program = rows;
                                                  merchant.list(category_id, 1000000, 0, function(merchant_rows){
                                                      var redeemAmountArr =[];
                                                      // console.dir(merchant_rows)
                                                      if(merchant_rows.length>0){
                                                          console.log("normal_point 1 = "+normal_point);
                                                          
                                                          redeem.get(program_code, merchant_id, normal_point, function(rows){
                                                              var redeem_rows = rows;
                                                              console.log("redeem start rows length = "+redeem.length);
                                                              console.dir(redeem);
                  
                                                              if(redeem_rows.length>0){
                  
                                                                  if(parseInt(new_current_poin)!=0 && parseInt(new_current_poin)>=redeem_rows[0].point && new_current_poin>=normal_point){
                                                                      
                                                                      console.dir(redeem_rows)
                                                                      console.log("nominal new_current_poin lolos "+new_current_poin+">="+redeem_rows[0].point);
                                                                      redeemAmountArr.push(redeem_rows[0].amount);
                                                                  
                                                                      console.log(Math.max(...redeemAmountArr));
                                                                      var b = [].concat(redeemAmountArr); // clones "a"
                                                                      b.sort(function (redeemAmountArr, b) { return redeemAmountArr.x - b.x; });
                                                                      var minAmount = b[0];
                                                                      var maxAmount = b[b.length - 1];
                                                                      //console.log("program name = "+redeem[0].merchant_id);
                                                                      console.log("maxAmount");
                                                                      console.dir(maxAmount);
                                                                      console.log("minAmount");
                                                                      console.dir(minAmount);
                                                                      var rek_point=0;
                  
                                                                      core.verifiedRedeemPoint(userCIF,point,rek_point, new_current_poin, program_code,program,type,minAmount,
                                                                          maxAmount,phone_tujuan,normal_point,redeem_rows,member_id,merchant_id,member_list,function(rows){
                                                                              //callback(rows);
                                                                              res.json(rows);
                                                                      });
                                                                      
                                                                      //end
                                                                  }else{
                                                                      //poin tidak cukup
                                                                      response.status_204("Failed", "Not Enough poin", function(cb){
                                                                              res.json(cb);
                                                                              //callback(cb);
                                                                          
                                                                      }); 
                                                                  }
                  
                                                              }else{
                                                                  //failed data not found
                                                                  response.status_204("Failed", "redeem data not exist", function(cb){
                                                                          res.json(cb);
                                                                          //callback(cb)
                                                                      
                                                                  }); 
                                                              }
                                                              
                                                              
                                                          });
                                                          
                                                      }else{
                                                          response.status_204("Failed", "redeem data not exist", function(cb){
                                                              // res.json(cb);
                                                                  callback(cb)
                                                                  
                                                              }); 
                                                      }
                                                      
                                                  });
                          
                                              });
                                          }else{
                                              response.status_204("Failed", "Point Anda tidak mencukupi", function(cb){
                                                  res.json(cb);
                                                  //callback(cb);
                                                  
                                              }); 
                                          }
                                      }
              
                                  });
                          
                              });
                              
                              
                              
                          }else{
                                  //
                              response.status_204("Failed", "User not found", function(cb){
                                  res.json(cb);
                                  //callback(cb)
                                  
                              }); 
                          }
                      });
  
                    
                  }
              });
              }else{
                  response.status_401("Failed", "Token null", function(cb){
                  res.json(cb)
              });
              }
  
          }
      
      });
    },
    process_giift: function(req, res) {
      var merchant_id   = (typeof req.body.merchant_id != 'undefined') ? req.body.merchant_id : '';
      var merchant_name   = (typeof req.body.merchant_name != 'undefined') ? req.body.merchant_name : '';
      var point         = (typeof req.body.point != 'undefined') ? req.body.point : '';
      var amount         = (typeof req.body.amount != 'undefined') ? req.body.amount : '';
      var email_tujuan   = (typeof req.body.email_tujuan != 'undefined') ? req.body.email_tujuan : '';
      var type = 'redeem';
      var phone_tujuan, id_tujuan, code_tujuan="";
      var normal_point=point;
      var phone =(typeof req.body.phone != 'undefined') ? req.body.phone : '';

      console.log(moment().format('Y-M-D HH:mm:ss')+ " process giift redeem request: phone="+phone+" merchant_id="+merchant_id+" merchant_name="+merchant_name+" point="+point+" amount="+amount+" email_tujuan="+email_tujuan);

      if (merchant_id=='' || point=='' || amount=='' || email_tujuan=='' || phone=='') {
        response.status_204("Failed", "Data not complete, please check again.", function(cb){
            res.json(cb);
        });
      } else {
        member.get_blacklist_cif(decoded.payload.cif, function(rows){
            if(rows.length > 0){
                response.status_204("Failed", "Check Customer Service code 2", function(cb){
                    res.json(cb);
                });
            }else{
    
                if(token != ''){
                jwt.verify(token, apikey, (err, data) => {
                    if(err){
                        response.status_401("Failed", "Unauthorized", function(cb){
                        res.json(cb)
                        });
                    }else{
    
                        member.authentication_with_phone(phone, function(rows){ 
                            
                            //not yet
                            if(rows.length > 0 ){
                                var member_list = rows;
                                var apiKey = 'NxoklkZDL0Cz8GrHAFYzViA8cVv16wP5';
                                var member_id =rows[0].member_id;
                                var new_current_poin = rows[0].total_point;
                                var userCIF = rows[0].cif;
                                var dataMember = rows;
                                var rekening = rows[0].rekening;
                                //var totalp
    
                                //console.dir(rows);
                                console.log("length rows "+rows.length+ " new_current_poin "+new_current_poin);
                                //check black list
                              //   member.get_blacklist_cif(userCIF, function(rows){
                              //       if(rows.length > 0){
                              //           response.status_204("Failed", "Check Customer Service code 2", function(cb){
                              //               //callback(cb);
                              //               res.json(cb);
                              //           });
                              //       }
                                    coreModel.get_total_point(userCIF, function(total_point){
                                        console.dir(total_point)
                                        console.log("get total point "+ total_point[0].Total);
                                        new_current_poin=total_point[0].Total;
                                        normal_point=point;
                                        if(total_point.length>0){
                
                                            if(new_current_poin>=500){
                                                // check merchant_id and value if exist
                                                merchant.list_giift_by_id(merchant_id, amount, function(rows) {
                                                    if (rows.length > 0) {
                                                        if(parseInt(new_current_poin)!=0 && parseInt(new_current_poin)>=rows[0].points && parseInt(new_current_poin)>=point) {
                                                            // var point_updated = new_current_poin-point;
                                                            merchant_name = rows[0].name;
                                                            var giift_img = rows[0].img;
                                                            var giift_tc = rows[0].tc;
                                                            var balance = new_current_poin-point;
                                                            // deduct point and log pending
                                                            // coreModel.update_rek_point_by_cif(userCIF,userCIF,point_updated,point_updated, function(new_updated_point_rows) {
                                                            // coreModel.deduct_rek_point_by_cif(userCIF, point, function(new_updated_point_rows) {
                                                            // coreModel.deduct_rek_point_by_cif(phone,rekening, point, function(new_updated_point_rows) {
                                                            coreModel.update_rek_point_by_cif(userCIF, userCIF, balance, balance, function(new_updated_point_rows){
                                                                console.log("update_rek_point_by_cif affectedRows = " + new_updated_point_rows.affectedRows);
                                                                // if (new_updated_point_rows.affectedRows>0) {
                                                                if(typeof new_updated_point_rows.affectedRows != 'undefined' && (new_updated_point_rows.affectedRows == 1 || new_updated_point_rows.affectedRows == 0)){
                                                                    var utcMoment = moment().utc();
                                                                    var randomstring = require('randomstring');
                                                                    // var randomCode = randomstring.generate(5);
                                                                    var randomNo = randomstring.generate({
                                                                        length: 3,
                                                                        charset: phone
                                                                    });
                                                                    var randomCode = randomstring.generate({
                                                                        length: 4,
                                                                        charset: 'alphanumeric',
                                                                        capitalization: 'uppercase'
                                                                    });
                                                                    var trans_code = randomNo+randomCode; //'BTN'+phone.slice(-3)+'-'+utcMoment.valueOf();
                                                                    log.insert_M_history(member_id, point, new_current_poin, email_tujuan, trans_code, merchant_id, amount, merchant_name, giift_img, function(history_result) {
                                                                        if (history_result.affectedRows>0) {
                                                                            // hit giift api
                                                                            core.getGiiftPurchase(merchant_id, amount, trans_code, trans_code, phone, function(result) {
                                                                                if(result.statusCode==200) {
                                                                                    var giift_dmo_id = merchant_id;
                                                                                    var giift_value = amount;
                                                                                    var giift_order_id = result.data.orderId;
                                                                                    var giift_status = result.data.cards[0].status;
                                                                                    var giift_status_name = result.data.cards[0].status_name;
                                                                                    var giift_number = core.encrypt(result.data.cards[0].fields.number); //(typeof result.data.cards[0].fields.ccv != 'undefined') ? result.data.cards[0].fields.ccv : '';
                                                                                    // if (giift_number.length>0) {
                                                                                    //     giift_number = core.encrypt(result.data.cards[0].fields.number);
                                                                                    // }
                                                                                    var giift_ccv = (typeof result.data.cards[0].fields.ccv != 'undefined') ? result.data.cards[0].fields.ccv : '';
                                                                                    var giift_expiration = (typeof result.data.cards[0].fields.expiration != 'undefined') ? result.data.cards[0].fields.expiration : '';
                                                                                    var giift_extra_login = (typeof result.data.cards[0].fields.extra_login != 'undefined') ? result.data.cards[0].fields.extra_login : '';
                                                                                    var giift_extra_password = (typeof result.data.cards[0].fields.extra_password != 'undefined') ? result.data.cards[0].fields.extra_password : '';
                                                                                    log.update_M_history_giift_success(member_id, point, trans_code, giift_dmo_id, giift_value, giift_order_id, giift_status, giift_status_name, giift_number, giift_ccv, giift_expiration, giift_extra_login, giift_extra_password, function(cb) {
                                                                                        console.log("update M_history success " + cb);
                                                                                        if(cb.affectedRows>0) {
                                                                                          var myInt = setInterval(function () {
                                                                                              core.sendEmailVoucher(trans_code, email_tujuan, dataMember, giift_tc, function(cb_email){
                                                                                                  console.log("email resp: "+ cb_email);
                                                                                              });
                                                                                              clearTimeout(myInt);
                                                                                          }, 5000);
                                                                                        }
                                                                                    });
                                                                                    response.status_200("Success", result, function(cb) {
                                                                                        var resp = {
                                                                                            status : 200,
                                                                                            message : "success",
                                                                                            display_message : "Success",
                                                                                            data : result,
                                                                                            tc : giift_tc,
                                                                                            email_tujuan : email_tujuan
                                                                                          };
                                                                                        res.json(resp);
                                                                                    });
                                                                                } else {
                                                                                    var giift_dmo_id = merchant_id;
                                                                                    var giift_value = amount;
                                                                                    var status_msg = result.data.error_description;
                                                                                    // log fail
                                                                                    log.update_M_history_giift_fail(member_id, point, trans_code, giift_dmo_id, giift_value, status_msg, function(cb){
                                                                                        console.log("update M_history fail " + cb);
                                                                                    });
                                                                                    // coreModel.update_rek_point_by_cif(userCIF,userCIF,new_current_poin,new_current_poin, function(new_updated_point_rows) {
                                                                                    // refund the point
                                                                                    // coreModel.deduct_rek_point_by_cif(userCIF, point*-1, function(cb) {
                                                                                    coreModel.get_total_point(userCIF, function(result_total){
                                                                                        new_current_poin=result_total[0].Total;
                                                                                        balance = parseInt(new_current_poin)+parseInt(point);
                                                                                        // coreModel.deduct_rek_point_by_cif(rekening, point*-1, function(cb) {
                                                                                        coreModel.update_rek_point_by_cif(userCIF, userCIF, balance, balance, function(new_updated_point_rows){
                                                                                            console.log("refund point, end balance: " + balance +" result "+ new_updated_point_rows.affectedRows);
                                                                                        });
                                                                                    });
                                                                                    var resp = {
                                                                                        status : 204,
                                                                                        message : "Failed",
                                                                                        // display_message : result.data.error_description,
                                                                                        display_message : "Penukaran voucher gagal. Silakan coba kembali atau hubungi contact center kami.",
                                                                                        data : result.data
                                                                                    };
                                                                                    res.json(resp);
                                                                                }
                                                                            });
                                                                        } else {
                                                                            coreModel.get_total_point(userCIF, function(result_total){
                                                                                new_current_poin=result_total[0].Total;
                                                                                balance = parseInt(new_current_poin)+parseInt(point);
                                                                                // coreModel.deduct_rek_point_by_cif(rekening, point*-1, function(cb) {
                                                                                coreModel.update_rek_point_by_cif(userCIF, userCIF, balance, balance, function(new_updated_point_rows){
                                                                                    console.log("refund point, end balance: " + balance +" result "+ new_updated_point_rows.affectedRows);
                                                                                });
                                                                            });
                                                                            response.status_204("Failed", "System error. Please contact customer support.", function(cb){
                                                                                res.json(cb);
                                                                            });
                                                                        }
                                                                    });
                                                                } else {
                                                                    response.status_204("Failed", "Deduct point failed. Please contact customer support.", function(cb){
                                                                        res.json(cb);
                                                                    }); 
                                                                }
                                                            });
                                                        } else {
                                                            response.status_204("Failed", "Not enough point", function(cb){
                                                                res.json(cb);
                                                            }); 
                                                        }
                                                    } else {
                                                        response.status_204("Failed", "Merchant data not found", function(cb){
                                                            res.json(cb);
                                                        }); 
                                                    }
                                                });
                                            }else{
                                                response.status_204("Failed", "Tingkatkan poin Anda untuk dapat melakukan penukaran poin", function(cb){
                                                    res.json(cb);
                                                }); 
                                            }
                                        }
                
                                    });
                            
                              //   });
                                
                                
                                
                            }else{
                                    //
                                response.status_204("Failed", "User not found", function(cb){
                                    res.json(cb);
                                    //callback(cb)
                                    
                                }); 
                            }
                        });
    
                      
                    }
                });
                }else{
                    response.status_401("Failed", "Token null", function(cb){
                    res.json(cb)
                });
                }
            }
        });
      }
    },
    purchase_giift: function(req, res) {
      var merchant_id   = (typeof req.body.merchant_id != 'undefined') ? req.body.merchant_id : '';
      var point  = (typeof req.body.point != 'undefined') ? req.body.point : '';
      var amount         = (typeof req.body.amount != 'undefined') ? req.body.amount : '';
      var receiver_name   = (typeof req.body.receiver_name != 'undefined') ? req.body.category_id : '';
      var cif           = (typeof req.body.cif != 'undefined') ? req.body.cif : '';
      var state           = (typeof req.body.state != 'undefined') ? req.body.state : '';
      var receipt_id           = (typeof req.body.receipt_id != 'undefined') ? req.body.receipt_id : '';
      var operator_id           = (typeof req.body.operator_id != 'undefined') ? req.body.operator_id : '';
      console.log("decoded cif" + decoded.payload.cif);
      core.getGiiftPurchase(merchant_id, amount, state, receipt_id, operator_id, function(result) {
          res.json(result);
      });
    },
    get_giift_list: function(req, res) {
        core.processGiiftList(function(result) {
            res.json(result);
        });
    }
  };
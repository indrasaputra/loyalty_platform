module.exports = {
  /*Function get Token*/
  list: function(req, res) {
    var program_code  = (typeof req.body.program_code != 'undefined') ? req.body.program_code : '';
    var to_program    = (typeof req.body.to_program != 'undefined') ? req.body.to_program : '';
    if(token != ''){
      jwt.verify(token, apikey, (err, data) => {
        if(err){
          response.status_401("Failed", "Unauthorized", function(cb){
          res.json(cb)
          });
        }else{ 
          member.list(decoded.payload.cif, 1, 0, function(cif){
            if(cif.length > 0){
              exchange.list(program_code, to_program, function(rows){
                if(rows.length > 0){
                  response.status_200("Success", rows, function(cb){
                  res.json(cb)
                  });
                }else{
                  response.status_204("Failed", "Data not found", function(cb){
                  res.json(cb)
                  }); 
                }
              });
            }else{
              response.status_401("Failed", "Profile not found", function(cb){
              res.json(cb)
              });
            }
          });
        }
      });
    }else{
      response.status_401("Failed", "Token null", function(cb){
      res.json(cb)
      }); 
    }
  },
  proccess: function(req, res) {
    var program_code  = (typeof req.body.program_code != 'undefined') ? req.body.program_code : '';
    var to_program    = (typeof req.body.to_program != 'undefined') ? req.body.to_program : '';
    var point         = (typeof req.body.point != 'undefined') ? req.body.point : '';
    var cif           = (typeof req.body.cif != 'undefined') ? req.body.cif : '';
    var phone_tujuan  = (typeof req.body.phone_tujuan != 'undefined') ? req.body.phone_tujuan : '';
    var id_tujuan  = (typeof req.body.id_tujuan != 'undefined') ? req.body.id_tujuan : '';
    var code_tujuan  = (typeof req.body.code_tujuan != 'undefined') ? req.body.code_tujuan : '';
    var type = 'exchange';
    var category_name='';
    //get balck list
    member.get_blacklist_cif(decoded.payload.cif, function(rows){
      if(rows.length > 0){
        response.status_204("Failed", "Check Customer Service code 2", function(cb){
          res.json(cb);
      });
      }else{

      
        //get category id  from M_program
        program.list_program_by_code(to_program, function(rows){
            var category_id = rows[0].category_id;
            console.log("category_id "+category_id);
            //get category name  from M_category
            category.get(category_id, function(rows){
                category_name = rows[0].name;
                console.log("var category_name = "+category_name);
            });
        });

        if(code_tujuan =='' && id_tujuan =='' &&  phone_tujuan ==''){
            response.status_204("Failed", "one of field id , code, phone tujuan can't be empty", function(cb){
                res.json(cb);
            });
        }

        //if gojek grab GJAC098
        if(phone_tujuan =='' ){
        if (to_program != 'GJAC098' || to_program != 'GRBAC098'){
                console.log("bukan gojek or grab");
        }else{
            req.checkBody('phone tujuan', "phone tujuan can't be empty and must as numeric").notEmpty().isInt();
        }
        }
        
        if(id_tujuan =='' ){
            //case garuda milesage
            if(to_program != 'GAAC098' || category_name != 'Airlines') {
                console.log("bukan Airlines");
            }else{
                req.checkBody('id tujuan', "id tujuan can't be empty ").notEmpty();
            }
        
        }

        if(parseInt(point) ==0 || parseInt(point) < 0){
        response.status_204("Failed", "point value not valid", function(cb){
            res.json(cb);
            });
            process.exit();
        }
        
        // if(code_tujuan ==''){
        //     //case others
        //     req.checkBody('code tujuan', "code tujuan can't be empty").notEmpty();
        // }

        
        
        var errors  = req.validationErrors();
        if(token != ''){
        jwt.verify(token, apikey, (err, data) => {
            if(err){
            response.status_401("Failed", "Unauthorized", function(cb){
            res.json(cb)
            });
            }else{
                if (errors) {
                    response.status_204("Failed", errors, function(cb){
                    res.json(cb);
                    });
                }else{
                    /*  
                        member.list(decoded.payload.cif, 1, 0, function(cif){
                            if(cif.length > 0){
                            var member_list = cif;
                            //exchange.get(program_code, to_program, point, function(rows){
                            exchange.get_dynamic(program_code, to_program, point, function(rows){
                                //exchange.list(program_code, to_program, function(rows){
                                var exchange_list = rows;
                                    //member.list(decoded.payload.cif, 1, 0, function(rows){
                                        //var member_list = rows;
                                        console.log("member_list[0] "+member_list[0]);
                                        console.log("member_list size "+member_list.length);
                                        if(member_list.length > 0){
                                            //log.total_poin(member_list[0].member_id, function(rows){
                                                //get expired
                                                member.get_blacklist_cif(member_list[0].cif, function(rows){
                                                    if(rows.length > 0){
                                                    response.status_204("Failed", "Check Customer Service code 2", function(cb){
                                                        //res.json(cb);
                                                        callback(cb);
                                                    });
                                                }
                                            
                                                });
                                                var nominal = point;
                                                
                                                if(rows.length>0){
                                                    //check cif acc ganda
                                                    var new_current_poin= member_list[0].point;
                                                    var cifAccGanda =false;
                                                    var acc_ganda=[];
                                                    if(cif.length>1){
                                                    log.get_acc_ganda_cif(decoded.payload.cif,function(acc_ganda_cif){
                                                        acc_ganda = acc_ganda_cif;
                                                        if(acc_ganda.length>1){
                                                        new_current_poin=acc_ganda[acc_ganda.length-1].total_point;
                                                        cifAccGanda =true;
                                                        point=point/acc_ganda.length;
                                                        console.log("new_current_poin 1 = "+new_current_poin+" rows.length "+acc_ganda.length);
                                                        }else{
                                                            //console.log("rows length"+rows.length);
                                                            new_current_poin=acc_ganda[0].point;
                                                            console.log("new_current_poin 2 = "+new_current_poin);
                                                        }
                                                    });
                                                    }
                                                    
                                                    
                                                    var poin_expire_date = moment.utc(cif[0].point_expire, "Y-M-D h:mm:ss ");
                                                    if(moment(poin_expire_date) <= moment() ) {
                                                        console.log("log process: point expired");
                                                        //do some blocking to not continue the code
                                                        point = -new_current_poin;
                                                        log.update(exchange_list[0].from_program_id, member_list[0].member_id, point, nominal, description, function(rows){
                                                        response.status_204("Failed", rows, function(cb){
                                                            res.json(cb);
                                                
                                                        });
                                                        
                                                        }); 
                                                    }else{
                                                        console.log("log process: :  point valid not yet expire");
                                                        exchange.get_toRatio(exchange_list[0].to_program_id, function(rows){           
                                                            if(rows.length > 0){
                                                                response.status_200("Success", rows, function(cb){
                                                                //multiply rule from get_toRation with current point
                                                                var pointRule =parseFloat(rows[0].from_ratio / rows[0].to_ratio);
                                                                console.log("rows[0].from_ratio  "+rows[0].from_ratio + " rows[0].to_ratio "+ rows[0].to_ratio);
                                                                    if(exchange_list.length > 0){
                                                                        var exchangePoint = parseInt(point * pointRule);
                                                                        var balance = new_current_poin;
                                                                        var new_balance=0;
                                                                        if(cifAccGanda==true){
                                                                        exchangePoint = parseInt(point * acc_ganda.length * pointRule );
                                                                        // balance = new_current_poin - parseInt(point *acc_ganda.length);
                                                                        console.log("cifAccGanda point "+point+" new_current_poin "+new_current_poin)
                                                                        if(point > new_current_poin){
                                                                            
                                                                            balance = new_current_poin - parseInt(point *acc_ganda.length);
                                                                            new_balance = balance;
                                                                            console.log("cifAccGanda balance 1 "+balance);
                                                                        }else{
                                                                            balance = new_current_poin;
                                                                            new_balance=new_current_poin - parseInt(point *acc_ganda.length);
                                                                            console.log("cifAccGanda balance 2 "+balance+ " new_balance "+new_balance);


                                                                        }
                                                                        }else{
                                                                        if(point > new_current_poin){
                                                                            
                                                                            balance = new_current_poin-point;
                                                                            new_balance = balance;
                                                                            console.log("non cifAccGanda balance 1 "+balance);
                                                                            
                                                                        }else{
                                                                            balance = new_current_poin;
                                                                            new_balance=new_current_poin - parseInt(point);
                                                                            console.log("non cifAccGandabalance 2 "+balance+ " new_balance "+new_balance);
                                                                            
                                                                        }
                                                                        }
                                                                        console.log("exchange exchangePoint "+exchangePoint);
                                                                        //var balance = new_current_poin;
                                                                        
                                                                        console.log("exchange balance 4 = "+balance)
                                                                        //console.log("process plus(+) exchange_list[0].to_program_id "+exchange_list[0].to_program_id);
                                                                        var result    = {program_name: exchange_list[0].program_name, to_program: exchange_list[0].to_program_name, exchange: {"point": point*acc_ganda.length, "result": exchangePoint, balance: new_balance}};
                                                                        //update_point_to: function(program_id, point, member_id, action, callback)
                                                                        log.update_point_to(exchange_list[0].to_program_id, exchange_list[0].to_point, member_list[0].member_id, 'plus', program_code, type, phone_tujuan, id_tujuan, code_tujuan, balance, cifAccGanda, decoded.payload.cif, function(rows){
                                                                            
                                                                            //response.end(res);
                                                                            if(typeof rows.affectedRows != 'undefined' && (rows.affectedRows == 1 || rows.affectedRows == 0)){
                                                                            console.log("enter minus ");
                                                                                
                                                                                log.update_point_to(exchange_list[0].from_program_id, point, member_list[0].member_id, 'minus', to_program, type, phone_tujuan, id_tujuan, code_tujuan, balance, cifAccGanda, decoded.payload.cif, function(rows){
                                                                                response.status_200("Success", result, function(cb){
                                                                                res.json(cb);
                                                                                });
                                                                            });
                                                                            }else{
                                                                                response.status_204("failed", "Not enough point ", function(cb){
                                                                                    res.json(cb);
                                                                                });
                                                                            }
                                                                        });
                                
                                                                    }else{
                                                                        //kondisi tnap poin diset
                                                                        response.status_204("Failed", "Data not found", function(cb){
                                                                            res.json(cb);
                                                                        }); 
                                                                    }
                                                                
                                                                });
                                                            }else{
                                                                response.status_204("Failed", "Rule Data not found", function(cb){
                                                                    //dont do calculation //error
                                                                    res.json(cb);
                                                                }); 
                                                            }         
                                                        });
                                                    }
                                                }else{
                                                    //total point null tidak ada
                                                    
                                                }
                                            //});

                                            
                                        }else{
                                            response.status_204("Failed", "User not found", function(cb){
                                            res.json(cb)
                                            });
                                        }
                                    //});
                            });
                            }else{
                            response.status_401("Failed", "Profile not found", function(cb){
                            res.json(cb);
                            }); 
                            }
                        });
                    */
                    //test
                    member.list(decoded.payload.cif, 1, 0, function(rows){
                        if(rows.length > 0){
                            var member_list = rows;
                            if(member_list.length > 0){
                                userCIF = rows[0].cif;
                                userID=rows[0].member_id;
                                var type = 'exchange';
                            // var program_code = "BTNC098";
                                //var to_program = "GJAC098";
                                var new_current_poin = rows[0].point;//point di M_program_join dari query tidak valid ini dirubah get M_cif_total_point
                                //var id_tujuan="";
                                //var code_tujuan ="";
                                var cifAccGanda =false;
                            
                                
            
                                //check black list
                                member.get_blacklist_cif(userCIF, function(blacklist){
                                    if(blacklist.length > 0){
                                        response.status_204("Failed", "Check Customer Service code 2", function(cb){
                                            res.json(cb);
                                            //callback(cb);
                                        });
                                    }
                            
                                });
                                // =====get total point ====
                                coreModel.get_total_point(userCIF, function(total_point){
                                    console.dir(total_point)
                                    console.log("get total point "+ total_point[0].Total);
                                    new_current_poin=total_point[0].Total;
                                    
                                });
            
                                // ======end get total point ======
            
                                //=======get rek point =======
            
                                var rek_point=0;
                                coreModel.get_rek_point(userCIF, function(rek_point_rows){
                                    rek_point=rek_point_rows.find(obj => obj.point>=point);
                                    console.dir(rek_point)
                                    console.log("get rek point "+ rek_point.point);
                                    if(rek_point_rows.length>0){
                                        var point_updated=rek_point.point-point;
                                        coreModel.update_rek_point_by_cif(userCIF,rek_point.ACCTNO,point_updated,new_current_poin-point,function(updated_point_rows){
                                            if(typeof updated_point_rows.affectedRows != 'undefined' && (updated_point_rows.affectedRows == 1 || updated_point_rows.affectedRows == 0)){
                                                exchange.get_dynamic(program_code, to_program, point, function(rows){
                                                    var exchange_list = rows;
                                                    exchange.get_toRatio(exchange_list[0].to_program_id, function(rows){           
                                                        if(rows.length > 0){
                                                            response.status_200("Success", rows, function(cb){
                                                                //multiply rule from get_toRation with current point
                                                                var pointRule =parseFloat(rows[0].to_ratio / rows[0].from_ratio);
                                                                console.log("rows[0].from_ratio  "+rows[0].from_ratio + " rows[0].to_ratio "+ rows[0].to_ratio);
                                                                if(exchange_list.length > 0){
                    
                                                                    var exchangePoint = parseFloat(point * pointRule);
                                                                    var balance = new_current_poin;
                                                                    console.log("exchangePoint value = "+exchangePoint);
                    
                                                                    //condition where exchangePoint is float fragment number it will be rejected
                                                                    if(exchangePoint<1){
                                                                        response.status_204("Failed", "invalid minimum point", function(cb){
                                                                            res.json(cb);
                                                                            //callback(cb);
                                                                        });
                                                                    }else{
                                                                        
                                                                        balance = new_current_poin-point;
                                                                        var result = {program_name: exchange_list[0].program_name, to_program: exchange_list[0].to_program_name, exchange: {"point": point , "result": exchangePoint, balance: balance}};
                    
                                                                        log.update_point_to(exchange_list[0].to_program_id, exchange_list[0].to_point, userID, 'plus', program_code, type, phone_tujuan, id_tujuan, code_tujuan, balance, cifAccGanda, userCIF, function(rows){
                                                                            //response.end(res);
                                                                            if(typeof rows.affectedRows != 'undefined' && (rows.affectedRows == 1 || rows.affectedRows == 0)){
                                                                                console.log("enter minus ");
                                                                                
                                                                                log.update_point_to(exchange_list[0].from_program_id, point, userID, 'minus', to_program, type, phone_tujuan, id_tujuan, code_tujuan, balance, cifAccGanda, userCIF, function(rows){
                                                                                    response.status_200("Success", result, function(cb){
                                                                                        res.json(cb);
                                                                                        //callback(cb);
                                                                                    
                                                                                    });
                                                                                });
                                                                            }else{
                                                                                //response.status_204("failed", "Not enough point ", function(cb){
                                                                                //    res.json(cb);
                                                                                // callback(cb);
                                                                                //});

                                                                                console.log("enter minus ");
                                                                                
                                                                                log.update_point_to(exchange_list[0].from_program_id, point, userID, 'minus', to_program, type, phone_tujuan, id_tujuan, code_tujuan, balance, cifAccGanda, userCIF, function(rows){
                                                                                    response.status_200("Success", result, function(cb){
                                                                                        res.json(cb);
                                                                                        //callback(cb);
                                                                                    
                                                                                    });
                                                                                });
                                                                            }
                                                                        });
                                                                        
                                                                    }
                                                                    
                                                                
                            
                                                                }else{
                                                                    //kondisi tnap poin diset
                                                                    response.status_204("Failed", "Data not found", function(cb){
                                                                        res.json(cb);
                                                                        //callback(cb);
                                                                    }); 
                                                                }
                                                            
                                                            });
                                                        }else{
                                                            response.status_204("Failed", "Rule Data not found", function(cb){
                                                                //dont do calculation //error
                                                                //res.json(cb);
                                                                callback(cb);
                                                            }); 
                                                        }         
                                                    });
                    
                                                });
                                            }
                                    
                                        });
                                    }
                                });
                                
                                //=====end get rek point ====
            
                            
                            }else{
                            response.status_204("Failed", "Member not found", function(cb){
                            //res.json(cb)
                            //console.log("failed"+res.json(cb));
                                console.dir("failed"+cb);
                                callback(cb);
                            }); 
                            }
                        }
                    });
                    
                    //end test
                }
            
            }
        });
        }else{
        response.status_401("Failed", "Token null", function(cb){
        res.json(cb)
        }); 
        }
    }

    });
  },
  proccess_wsdl: function(req, res) {
    var program_code  = (typeof req.body.program_code != 'undefined') ? req.body.program_code : '';
    var to_program    = (typeof req.body.to_program != 'undefined') ? req.body.to_program : '';
    var point         = (typeof req.body.point != 'undefined') ? req.body.point : '';
    var cif           = (typeof req.body.cif != 'undefined') ? req.body.cif : '';
    var phone         = (typeof req.body.phone != 'undefined') ? req.body.phone : '';
    var type = 'exchange';
    req.checkBody('phone', "phone can't be empty and must as numeric").notEmpty().isInt();
    var errors  = req.validationErrors();
    if(token != ''){
      jwt.verify(token, apikey, (err, data) => {
        if(err){
          response.status_401("Failed", "Unauthorized", function(cb){
          res.json(cb)
          });
        }else{
            if (errors) {
                response.status_204("Failed", errors, function(cb){
                res.json(cb);
                });
              }else{
                member.list(decoded.payload.cif, 1, 0, function(cif){
                    if(cif.length > 0){
                      exchange.get(program_code, to_program, point, function(rows){
                        var exchange_list = rows;
                        member.list(decoded.payload.cif, 1, 0, function(rows){
                          var member_list = rows;
                          
                          console.log("member_list[0] "+member_list[0]);
                          console.log("member_list size "+member_list.length);
                          if(member_list.length > 0){
                            if(exchange_list.length > 0){
                              if(point == exchange_list[0].from_point){
                              var balance = 0;
                              }else{
                              var balance = point - exchange_list[0].from_point;
                              }
                              //update_point_to: function(program_id, point, member_id, action, callback)
                              var result    = {program_name: exchange_list[0].program_name, to_program: exchange_list[0].to_program_name, exchange: {"point": point, "result": exchange_list[0].to_point, balance: balance}};
                              log.update_point_to(exchange_list[0].to_program_id, exchange_list[0].to_point, member_list[0].member_id, 'plus', program_code, type, function(rows){
                                if(typeof rows.affectedRows != 'undefined' && (rows.affectedRows == 1 || rows.affectedRows == 0)){
                                  log.update_point_to(exchange_list[0].from_program_id, point, member_list[0].member_id, 'minus', to_program, type, function(rows){
                                    response.status_200("Success", result, function(cb){
                                    res.json(cb);
                                    });
                                  });
                                }
                              });
                            }else{
                              response.status_204("Failed", "Data not found", function(cb){
                              res.json(cb)
                              }); 
                            }
                          }else{
                            response.status_204("Failed", "User not found", function(cb){
                            res.json(cb)
                            });
                          }
                        });
                      });
                    }else{
                      response.status_401("Failed", "Profile not found", function(cb){
                      res.json(cb)
                      }); 
                    }
                  });
              }
         
        }
      });
    }else{
      response.status_401("Failed", "Token null", function(cb){
      res.json(cb)
      }); 
    }
  },
  listAll: function(req, res) {
    var program_code  = (typeof req.body.program_code != 'undefined') ? req.body.program_code : '';
    //var to_program    = (typeof req.body.to_program != 'undefined') ? req.body.to_program : '';
    if(token != ''){
      jwt.verify(token, apikey, (err, data) => {
        if(err){
          response.status_401("Failed", "Unauthorized", function(cb){
          res.json(cb);
          });
        }else{ 
          member.list(decoded.payload.cif, 1, 0, function(cif){
            if(cif.length > 0){
              exchange.listAll(program_code, function(rows){
                if(rows.length > 0){
                  response.status_200("Success", rows, function(cb){
                  res.json(cb);
                  });
                }else{
                  response.status_204("Failed", "Data not found", function(cb){
                  res.json(cb);
                  }); 
                }
              });
            }else{
              response.status_401("Failed", "Profile not found", function(cb){
              res.json(cb)
              });
            }
          });
        }
      });
    }else{
      response.status_401("Failed", "Token null", function(cb){
      res.json(cb)
      }); 
    }
  },
  
};
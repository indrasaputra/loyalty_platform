module.exports = {
  /*Function get Token*/
  create: function(req, res) {
    var first_name    = (typeof req.body.first_name != 'undefined') ? req.body.first_name : '';
    var last_name     = (typeof req.body.last_name != 'undefined') ? req.body.last_name : '';
    var email         = (typeof req.body.email != 'undefined') ? req.body.email : '';
    var password      = (typeof req.body.password != 'undefined') ? req.body.password : '';
    var cif           = (typeof req.body.cif != 'undefined') ? req.body.cif : '';
    var phone         = (typeof req.body.phone != 'undefined') ? req.body.phone : '';
   // var rekening      = (typeof req.body.rekening != 'undefined') ? req.body.rekening : '';
    // first_name = core.decrypt(first_name);
    // last_name = core.decrypt(last_name);
    // email = core.decrypt(email);
    // password = core.decrypt(password);
    // cif = core.decrypt(cif);
    // phone = core.decrypt(phone);
    
    //req.checkBody('cif', "cif can't be empty and must as numeric").notEmpty().isInt();
    // req.checkBody('phone', "phone can't be empty and must as numeric").notEmpty().isInt();
    //req.checkBody('rekening', "rekening can't be empty and must as numeric").notEmpty().isInt();
    var errors  = req.validationErrors();
    if (errors) {
      response.status_204("Failed", errors, function(cb){
      res.json(cb);
      });
    }else{
        member.check(phone, cif, email, function(rows){
            console.log("members controller rows ="+rows+" length "+rows.length);
            if(rows.length>0){
                //user sudah ada
                response.status_204("Failed", "user Already register", function(cb){
                    res.json(cb);
                    }); 
            }else{
              console.log("members controller create start");
                member.create(first_name, last_name, email, password, cif, phone, function(rows){
                  console.log("members controller create execute");
                    if(typeof rows.affectedRows != 'undefined' && (rows.affectedRows == 1 || rows.affectedRows == 0)){
                      response.status_200("Success", "Insert success", function(cb){
                      res.json(cb);
                      });
                      member.insert_password_history(phone, password, function(result){
                        console.log("insert_password_history " + result.affectedRows);
                      });
                    }else{
                      response.status_204("Failed", "Insert failed", function(cb){
                      res.json(cb);
                      }); 
                    }
                  });
            }

        });
       
    }
  },
  //series 1 rows per insert
  create_from_feeder: function(req, res) {
    var first_name    = (typeof req.body.first_name != 'undefined') ? req.body.first_name : '';
    var last_name     = (typeof req.body.last_name != 'undefined') ? req.body.last_name : '';
    var email         = (typeof req.body.email != 'undefined') ? req.body.email : '';
    var password      = (typeof req.body.password != 'undefined') ? req.body.password : '';
    var cif           = (typeof req.body.cif != 'undefined') ? req.body.cif : '';
    var phone         = (typeof req.body.phone != 'undefined') ? req.body.phone : '';
    var rekening      = (typeof req.body.rekening != 'undefined') ? req.body.rekening : '';
    
    //req.checkBody('cif', "cif can't be empty and must as numeric").notEmpty().isInt();
    req.checkBody('phone', "phone can't be empty and must as numeric").notEmpty().isInt();
    //req.checkBody('rekening', "rekening can't be empty and must as numeric").notEmpty().isInt();
    var errors  = req.validationErrors();
    if (errors) {
      response.status_204("Failed", errors, function(cb){
      res.json(cb);
      });
    }else{
        member.check_from_feeder(phone, cif, email, rekening, function(rows){
            console.log("members controller rows ="+rows+" length "+rows.length);
            if(rows.length>0){
                //user sudah ada
                //continue to update 
                response.status_204("Failed", "user Already register", function(cb){
                    res.json(cb);
                }); 
            }else{
             // user belum ada bulk insert todo
             //todo create csv dari hasil sortir

              console.log("members controller create start");
              member.create_from_feeder(first_name, last_name, email, password, cif, phone, rekening, function(rows){
                  console.log("members controller create execute");
                    if(typeof rows.affectedRows != 'undefined' && (rows.affectedRows == 1 || rows.affectedRows == 0)){
                      response.status_200("Success", "Insert success", function(cb){
                      res.json(cb);
                      });
                    }else{
                      response.status_204("Failed", "Insert failed", function(cb){
                      res.json(cb);
                      }); 
                    }
                  });
            }

        });
       
    }
  },
  profile: function(req, res) {
    var member_id     = (typeof req.body.member_id != 'undefined') ? req.body.member_id : '';
    req.checkBody('member_id', "member_id can't be empty and must as numeric").notEmpty().isInt();
    if(token != ''){
      jwt.verify(token, apikey, (err, data) => {
        if(err){
          response.status_401("Failed", "Unauthorized", function(cb){
          res.json(cb)
          });
        }else{ 
          var errors  = req.validationErrors();
          if (errors) {
            response.status_204("Failed", errors, function(cb){
            res.json(cb)
            });
          }else{
            member.list(decoded.payload.cif, 1, 0, function(cif){
              if(cif.length > 0){
                member.detail(member_id, function(rows){
                  if(rows.length > 0){
                    response.status_200("Success", rows, function(cb){
                    res.json(cb)
                    });
                  }else{
                    response.status_204("Failed", "Data not found", function(cb){
                    res.json(cb)
                    }); 
                  }
                });
              }else{
                response.status_401("Failed", "Profile not found", function(cb){
                res.json(cb)
                });
              }
            });
          };
        }
      });
    }else{
      response.status_401("Failed", "Token null", function(cb){
      res.json(cb)
      });
    }
  },
  authentication: function(req, res) {
    var cif           = (typeof req.body.cif != 'undefined') ? req.body.cif : '';
    var first_name    = (typeof req.body.first_name != 'undefined') ? req.body.first_name : '';
    var last_name     = (typeof req.body.last_name != 'undefined') ? req.body.last_name : '';
    req.checkBody('cif', "cif can't be empty").notEmpty();
    var errors  = req.validationErrors();
    if (errors) {
      
      response.status_204("Failed", errors, function(cb){
      res.json(cb)
      });
    }else{
        member.authentication(cif, first_name, last_name, function(rows){
          if(rows.length > 0){
            var expires = moment().add(7, 'days').valueOf();
            var token = jwt.sign({ username: rows[0].email, cif: rows[0].cif, exp: expires},  apikey.toString());
            response.status_200(token, rows, function(cb){
            res.json(cb)
            });
          }else{
            response.status_204("Failed", "Member not found", function(cb){
            res.json(cb)
            }); 
          }
        });
    };
  },
  authentication_with_pass: function(req, res) {
    var password = (typeof req.body.password != 'undefined') ? req.body.password : '';
    var phone    = (typeof req.body.phone != 'undefined') ? req.body.phone : '';
    // console.log("phone = "+phone+", password = "+password);
    // phone = core.decrypt(phone);
    // console.log("phone decrypted = " + phone);
    // password = core.decrypt(password);
    // console.log("password decrypted = " + password);
    // req.checkBody('phone', "phone can't be empty and must as numeric").notEmpty().isInt();
    // req.checkBody('password', "password can't be empty").notEmpty();
    var errors  = req.validationErrors();
    if (errors) {
      response.status_204("Failed", errors, function(cb){
      res.json(cb);
      });
    }else{
        member.authentication_with_pass(phone, password, function(rows){
          if(rows.length > 0){
            var expires = moment().add(7, 'days').valueOf();
            var token = jwt.sign({ username: rows[0].email, cif: rows[0].cif, exp: expires},  apikey.toString());
            response.status_200(token, rows, function(cb){
            res.json(cb);
            });
          }else{
            response.status_204("Failed", "Member not found", function(cb){
            res.json(cb)
            }); 
          }
        });
    };
  },
  //h2h
  authentication_with_wsdl: function(req, res, next) {
   // var password = (typeof req.body.password != 'undefined') ? req.body.password : '';
    var phone    = (typeof req.body.phone != 'undefined') ? req.body.phone : '';
    req.checkBody('phone', "phone can't be empty and must as numeric").notEmpty().isInt();
   // req.checkBody('password', "password can't be empty and must as numeric").notEmpty();
    var errors  = req.validationErrors();
    
    if (errors) {
      var converted=serializerMember.render(errors);
      response.status_204xml("failed", converted, function(cb){
      //res.json(cb);
      //res.xmlparser();
      res.send(cb);
      });
    }else{
        member.authentication_with_wsdl(phone, function(rows){
          console.log('rows.length = '+rows.length+' '+rows);
          if(rows.length > 0){
            //var expires = moment().add(7, 'days').valueOf();
           // var token = jwt.sign({ username: rows[0].email, cif: rows[0].cif, exp: expires},  apikey.toString());
            response.status_200xml("success", rows, function(cb){
            //res.json(cb);
            //res.xmlparser();
            
            res.send(cb);
            });
          }else{
            response.status_204xml("failed", "Member not found", function(cb){
           //res.json(cb);
           //res.xmlparser(cb);
           res.send(cb);
            }); 
          }
        });
    }
  },
  authentication_with_phone: function(req, res) {
    //var password = (typeof req.body.password != 'undefined') ? req.body.password : '';
    var phone    = (typeof req.body.phone != 'undefined') ? req.body.phone : '';
    req.checkBody('phone', "phone can't be empty and must as numeric").notEmpty().isInt();
  // req.checkBody('password', "password can't be empty and must as numeric").notEmpty();
    var errors  = req.validationErrors();
    if (errors) {
      response.status_204("Failed", errors, function(cb){
        res.json(cb);
      });
    }else{
        member.authentication_with_phone(phone, function(rows){
          if(rows.length > 0){
            var expires = moment().add(7, 'days').valueOf();
            var token = jwt.sign({ username: rows[0].email, cif: rows[0].cif, exp: expires},  apikey.toString());
            response.status_200(token, rows, function(cb){
                userCIF=rows[0].cif;
                
                coreModel.get_total_point(userCIF, function(total_point){
                    if(total_point.length>0){
                        console.dir(total_point)
                        console.log("get total point "+ total_point[0].Total);
                        new_current_poin=total_point[0].Total;

                    }
                    arr =[];
                    arr=({
                
                        status              : 200,
                        message             : "success",
                        display_message     : token,
                        data                : {
                            "id": rows[0].id,
                            "first_name": rows[0].first_name,
                            "last_name": " ",
                            "email": rows[0].email,
                            "dob": rows[0].dob,
                            "cif": rows[0].cif,
                            "phone": rows[0].phone,
                            "rekening": rows[0].rekening,
                            "password": rows[0].password, 
                            "registered_dated": rows[0].registered_dated,
                            "member_id": rows[0].member_id,
                            "program_id": rows[0].program_id, 
                            "member_account": rows[0].member_account,
                            "member_password": rows[0].member_password,
                            "status": rows[0].status, 
                            "point": new_current_poin,
                            "point_expire": rows[0].point_expire,
                            "date_related": rows[0].date_related,
                            "total_point": rows[0].total_point
                        },
                        time : moment().format('YYYY-MM-DD hh:mm:ss')
                    
                    });
                    console.dir(arr)
                    res.json(arr);
                    
                });
            });
          }else{
            response.status_204("Failed", "Member not found", function(cb){
            res.json(cb)
            }); 
          }
        });
    };
  },
  update_member: function(req, res) {
    var member_id     = (typeof req.body.member_id != 'undefined') ? req.body.member_id : '';
    var phone    = (typeof req.body.phone != 'undefined') ? req.body.phone : '';
    var email    = (typeof req.body.email != 'undefined') ? req.body.email : '';
    var first_name    = (typeof req.body.first_name != 'undefined') ? req.body.first_name : '';
    var last_name    = (typeof req.body.last_name != 'undefined') ? req.body.last_name : '';

    req.checkBody('phone', "phone can't be empty and must as numeric").notEmpty().isInt();
    //req.checkBody('email', "email can't be empty ").notEmpty().email();
    req.checkBody('member_id', "member_id can't be empty and must as numeric").notEmpty().isInt();

    var errors  = req.validationErrors();
    if (errors) {
      response.status_204("Failed", errors, function(cb){
      res.json(cb);
      });
    }else{
        if(token != ''){
          jwt.verify(token, apikey, (err, data) => {
            if(err){
              response.status_401("Failed", "Unauthorized", function(cb){
              res.json(cb)
              });
            }else{ 
              var errors  = req.validationErrors();
              if (errors) {
                response.status_204("Failed", errors, function(cb){
                res.json(cb)
                });
              }else{
                member.detail_update(member_id, phone, email, first_name, last_name, function(cif){
                    if(typeof cif.affectedRows != 'undefined' && (cif.affectedRows == 1 || cif.affectedRows == 0)){
                    member.detail(member_id, function(rows){
                      if(rows.length > 0){
                        response.status_200("Success", rows, function(cb){
                            res.json(cb)
                        });
                      }else{
                        response.status_204("Failed", "Data not found", function(cb){
                            res.json(cb)
                        }); 
                      }
                    });
                  }else{
                    response.status_401("Failed 1", "Profile not found", function(cb){
                        res.json(cb)
                    });
                  }
                });
              };
            }
          });
        }else{
          response.status_401("Failed", "Token null", function(cb){
          res.json(cb)
          });
        }
    };
  },
  update_member_password: function(req, res) {
    var member_id     = (typeof req.body.member_id != 'undefined') ? req.body.member_id : '';
    var phone    = (typeof req.body.phone != 'undefined') ? req.body.phone : '';
    var old_pass    = (typeof req.body.old_pass != 'undefined') ? req.body.old_pass : '';
    var new_pass    = (typeof req.body.new_pass != 'undefined') ? req.body.new_pass : '';

    // req.checkBody('phone', "phone can't be empty and must as numeric").notEmpty().isInt();
    //req.checkBody('email', "email can't be empty ").notEmpty().email();
    // req.checkBody('member_id', "member_id can't be empty and must as numeric").notEmpty().isInt();

    var errors  = req.validationErrors();
    if (errors) {
      response.status_204("Failed", errors, function(cb){
      res.json(cb);
      });
    }else{
        if(token != ''){
          jwt.verify(token, apikey, (err, data) => {
            if(err){
              response.status_401("Failed", "Unauthorized", function(cb){
              res.json(cb)
              });
            }else{ 
              var errors  = req.validationErrors();
              if (errors) {
                response.status_204("Failed", errors, function(cb){
                res.json(cb)
                });
              }else{
                // member_id = core.decrypt(member_id);
                // phone = core.decrypt(phone);
                // old_pass = core.decrypt(old_pass);
                // new_pass = core.decrypt(new_pass);

                // console.log("decrypted data : " + member_id+" "+phone+" "+old_pass+" "+new_pass);
                member.authentication_with_pass(phone, old_pass, function(rows) {
                    if (rows.length>0) {
                        member.verify_new_password(phone, new_pass, function(rows) {
                            if (rows.length>0) {
                                response.status_401("Failed", "Password baru tidak boleh sama dengan 4 password sebelumnya", function(cb){
                                    res.json(cb)
                                });
                            } else {
                                member.detail_update_pass(member_id, phone, old_pass, new_pass, function(cif){
                                    if(typeof cif.affectedRows != 'undefined' && (cif.affectedRows == 1 || cif.affectedRows == 0)){
                                    member.detail(member_id, function(rows){
                                      if(rows.length > 0){
                                        response.status_200("Success", rows, function(cb){
                                            res.json(cb)
                                        });
                                        member.insert_password_history(phone, new_pass, function(result) {
                                            if (result.affectedRows>0) {
                                                console.log("Success insert password history");
                                            }
                                        });
                                      }else{
                                        response.status_204("Failed", "Data not found", function(cb){
                                            res.json(cb)
                                        }); 
                                      }
                                    });
                                  }else{
                                    response.status_401("Failed 1", "Profile not found", function(cb){
                                        res.json(cb)
                                    });
                                  }
                                });
                            }
                        });
                        
                    } else {
                        response.status_401("Failed", "Password lama salah", function(cb){
                            res.json(cb)
                        });
                    }
                });
                
              };
            }
          });
        }else{
          response.status_401("Failed", "Token null", function(cb){
          res.json(cb)
          });
        }
    };
  },
};
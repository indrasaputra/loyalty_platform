module.exports = {
  /*Function get Token*/
  token: function(req, res) {
    var auth;
    var payload_data;
    var client_id;
    var Admin		= (typeof req.headers.admin != 'undefined') ? req.headers.admin : '';
    console.log(Admin);
    if(Admin != ''){
    client_id		= Admin;
    payload_data	= Admin;
    }else{
      if (req.headers.authorization) {
      auth 			= new Buffer(req.headers.authorization.substring(6), 'base64').toString().split(':');
      client_id 		= auth[0].toString();
      payload_data	= auth[1].toString();
      }
    }
    var expires	= moment().add(1, 'days').valueOf();
    var token 	= jwt.sign({ client_id: client_id, exp: expires},  payload_data);
    response.status_200("Success", {"token": token}, function(cb){
	  res.json(cb)
		});
  },
};
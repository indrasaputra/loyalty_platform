var dbPass='root';//'123456';//'root';
//dikurangin 1 hari untuk simulasi
var date2905="2018-05-30 00:00:00";
var date2506="2018-06-25 00:00:00";
var date2606="2018-06-26 00:00:00";
var date2706="2018-06-27 00:00:00";
var dateNow=moment().format('Y-M-D h:mm:ss');
var config = require('config');
var conf_core_sys = config.get('CoreSys');
var dbHost = conf_core_sys.dbConfig.host;
var dbPort = conf_core_sys.dbConfig.port;
var dbUser = conf_core_sys.dbConfig.user;
var dbPassword = conf_core_sys.dbConfig.pass;

var pool = mysql.createPool({
    connectionLimit : 1000000,
    host: dbHost,
    user: dbUser,
    password: dbPassword,
    database: 'btn_loyalty_program',
    debug: false,
});

module.exports = {
    
    insert_cif: function(flag,path,callback){
        var connection = mysql.createConnection({
            //connectionLimit : 1000000,
            host: dbHost,
            user: dbUser,
            password: dbPassword,
            database: 'btn_loyalty_program',
            debug: false,
        });
            
        connection.connect();
    
        console.log("map_cif_phone Connected to Mysql"+moment().format('Y-M-D h:mm:ss'));
         var sql="LOAD DATA LOCAL INFILE '"+path+"' INTO TABLE M_cif_ganda FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n' "+
        "(@col1,@col2) "+
        "set cif=@col1,total_account=@col2 ";
    
        connection.query(sql, (err, result) => {
             if (err) {
                console.error('error connecting: ' + err.stack);
                return;
            }
            console.log(" M_cif_ganda result == "+result.length+"  M_cif_ganda result "+result.size+" time "+moment().format('Y-M-D h:mm:ss'));
            console.dir(result);
            //callback(result);
         });
        //connection.end();
        
        console.log(" M_cif_ganda insert member Done "+moment().format('Y-M-D h:mm:ss'));
       
        setTimeout(function(){
            // core.aktifasi_tbl(); 
        },30000);
    },

    insert_cif_new: function(flag,path,callback){
        var truncate="TRUNCATE TABLE M_cif_ganda";
        var sql="LOAD DATA LOCAL INFILE '"+path+"' INTO TABLE M_cif_ganda FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n' "+
        "(@col1,@col2) "+
        "set cif=@col1,total_account=@col2 ";

        pool.getConnection(function(err,connection){
            if (err) {
                return console.error(moment().format('Y-M-D HH:mm:ss.SSS')+' error: ' + err.message);
            }

            console.log(moment().format('Y-M-D HH:mm:ss.SSS')+' insert_cif_new Connected to the MySQL server Pool.');

            connection.query(truncate,function(err,rows){
                if (err) {
                    console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' insert_cif_new error execute query: ' + err.stack);
                    return;
                }

                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" insert_cif_new truncate result == "+rows.length+" "+moment().format('Y-M-D h:mm:ss'));
                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" insert_cif_new truncate table done");

                connection.query(sql,function(err,rows){
                    connection.release();
                    if (err) {
                        console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' insert_cif_new error executing query: ' + err.stack);
                        return;
                    }
    
                    console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" insert_cif_new insert result == "+rows.length+" "+moment().format('Y-M-D h:mm:ss'));
                    console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" insert_cif_new insert table done");
                
                    var myInt = setInterval(function () {
                        // core.aktifasi_tbl(); 
                        core.prepare_prog_join();
                        clearTimeout(myInt);
                    }, 15000);
                });
            });

            connection.on('error', function(err) {      
                console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' insertMemberNew error in connection database, status 100: ' + err.stack);
                return;     
            });
        });
    },

    insertMember:function (array){
      
        var connection = mysql.createConnection({
            //connectionLimit : 1000000,
            host: dbHost,
            user: dbUser,
            password: dbPassword,
            database: 'btn_loyalty_program',
            debug: false,
        });
            
        
        connection.connect();

        var path = '/var/www/html/csv_storage/db_member.csv'; 
            
        console.log("insertMember Connected to Mysql"+moment().format('Y-M-D h:mm:ss'));
        //id++,
        // item.CFSNME,
        // "",//lastname
        // "", //email
        // item.CFBIR6,
        // item.CIFNO,
        // item.SCPHONENO,
        // item.ACCTNO,
        // "123456",
        //moment().format('Y-M-D h:mm:ss'),//datetime
        var sql= "LOAD DATA LOCAL INFILE '"+path+"' INTO TABLE M_member FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n' (@col1,@col2,@col3,@col4,@col5,@col6,@col7,@col8,@col9) set first_name=@col1,last_name=@col2,email=@col3,dob=@col4,cif=@col5, phone=@col6, rekening=@col7, password=@col8 , registered_dated=@col9";
        // "LOAD DATA LOCAL INFILE '"+path+"' INTO TABLE M_member FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n' (@col1,@col2,@col3,@col4,@col5,@col6,@col7) set first_name=@col1,last_name=@col2,cif=@col3,rekening=@col4,registered_dated=@col5, phone=@col6, dob=@col7";
       console.log("sql "+sql);
        connection.query(sql, (err, result) => {
            // 'LOAD DATA INFILE \'/Users/adas/Downloads/signzy/db_inserts.txt\' INTO TABLE names (firstname, lastname) FIELDS TERMINATED BY \',\'', (err, result) => {
            if (err) {
                console.error('error connecting: ' + err.stack);
                return;
            }
            console.log("insertMember result == "+result.length+" insertMember result "+result.size+" time "+moment().format('Y-M-D h:mm:ss'));
            console.dir(result);
       });
      connection.end();

        console.log("insert member Done "+moment().format('Y-M-D h:mm:ss '));
    
      
        var myInt = setInterval(function () {
            //coreModel.process_map_cif_rek_start(array);
            // core.prepare_prog_join(array); ----- nyalain lagi
            clearTimeout(myInt);
        }, 15000);
    },

    insertMemberNew:function (array){
        var path = '/var/www/html/csv_storage/db_member_new.csv'; 
        var sql= 
        "LOAD DATA LOCAL INFILE '"+path+"' INTO TABLE M_member FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n' (@col1,@col2,@col3,@col4,@col5,@col6,@col7) set first_name=@col1,last_name=@col2,cif=@col3,rekening=@col4,registered_dated=@col5, phone=@col6, dob=@col7";
        console.log("sql "+sql);

        pool.getConnection(function(err,connection){
            if (err) {
                return console.error(moment().format('Y-M-D HH:mm:ss.SSS')+' error: ' + err.message);
            }

            console.log(moment().format('Y-M-D HH:mm:ss.SSS')+' insertMemberNew Connected to the MySQL server Pool.');

            connection.query(sql,function(err,rows){
                connection.release();
                if (err) {
                    console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' insertMemberNew error connecting: ' + err.stack);
                    return;
                }

                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" insertMemberNew result == "+rows.length+" end insert db "+moment().format('Y-M-D h:mm:ss'));
                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" insertMemberNew update table done");
            
                var myInt = setInterval(function () {
                    coreModel.process_map_cif_rek_new(array);
                    clearTimeout(myInt);
                }, 15000);
            });

            connection.on('error', function(err) {      
                console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' insertMemberNew error in connection database, status 100: ' + err.stack);
                return;     
            });
        });
    },

    process_map_cif_rek_new: function(){
        var path = '/var/www/html/csv_storage/db_member_new.csv'; 
        var sql= 
        "LOAD DATA LOCAL INFILE '"+path+"' INTO TABLE M_cif_map_rek FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n' "+
        "(@col1,@col2,@col3,@col4,@col5,@col6,@col7) "+
        "set CIFNO=@col3,ACCTNO=@col4 ,point=0";
        console.log("sql "+sql);

        pool.getConnection(function(err,connection){
            if (err) {
                return console.error(moment().format('Y-M-D HH:mm:ss.SSS')+' error: ' + err.message);
            }

            console.log(moment().format('Y-M-D HH:mm:ss.SSS')+' process_map_cif_rek_new Connected to the MySQL server Pool.');

            connection.query(sql,function(err,rows){
                connection.release();
                if (err) {
                    console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' process_map_cif_rek_new error connecting: ' + err.stack);
                    return;
                }

                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" process_map_cif_rek_new result == "+rows.length+" end insert db "+moment().format('Y-M-D h:mm:ss'));
                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" process_map_cif_rek_new update table done");
            
                var myInt = setInterval(function () {
                    coreModel.process_map_cif_phone_new();
                    clearTimeout(myInt);
                }, 15000);
            });

            connection.on('error', function(err) {      
                console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' process_map_cif_rek_new error in connection database, status 100: ' + err.stack);
                return;     
            });
        });
       
    },

    process_map_cif_phone_new: function(){
        var path = '/var/www/html/csv_storage/db_member_new.csv'; 
        var sql= 
        "LOAD DATA LOCAL INFILE '"+path+"' INTO TABLE M_cif_map_phone FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n' "+
        "(@col1,@col2,@col3,@col4,@col5,@col6,@col7) "+
        "set CIFNO=@col3,PHONE=@col6 ,point=0";
        console.log("sql "+sql);

        pool.getConnection(function(err,connection){
            if (err) {
                return console.error(moment().format('Y-M-D HH:mm:ss.SSS')+' error: ' + err.message);
            }

            console.log(moment().format('Y-M-D HH:mm:ss.SSS')+' process_map_cif_phone_new Connected to the MySQL server Pool.');

            connection.query(sql,function(err,rows){
                connection.release();
                if (err) {
                    console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' process_map_cif_phone_new error connecting: ' + err.stack);
                    return;
                }

                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" process_map_cif_phone_new result == "+rows.length+" end insert db "+moment().format('Y-M-D h:mm:ss'));
                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" process_map_cif_phone_new update table done");
            
                var myInt = setInterval(function () {
                    core.process_cif_filter_by_phone_new();
                    clearTimeout(myInt);
                }, 15000);
            });

            connection.on('error', function(err) {      
                console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' process_map_cif_phone_new error in connection database, status 100: ' + err.stack);
                return;     
            });
        });
      
    },

    process_map_cif_phone_start: function(){
        var connection = mysql.createConnection({
            //connectionLimit : 1000000,
            host: dbHost,
            user: dbUser,
            password: dbPassword,
            database: 'btn_loyalty_program',
            debug: false,
        });
            
        connection.connect();


        console.log("map_cif_phone Connected to Mysql"+moment().format('Y-M-D h:mm:ss'));
            var sql=//"INSERT INTO M_cif_map_phone (CIFNO,PHONE) SELECT m.cif, m.phone FROM M_member m, M_cif_map_rek r where m.rekening = r.ACCTNO";
            "LOAD DATA LOCAL INFILE '/var/www/html/csv_storage/db_member.csv' INTO TABLE M_cif_map_phone FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n' "+
       "(@col1,@col2,@col3,@col4,@col5,@col6,@col7,@col8) "+
        "set CIFNO=@col5,PHONE=@col6 ,point=0";
       // first_name=@col1,last_name=@col2,email=@col3,dob=@col4,cif=@col5, phone=@col6, rekening=@col7, password=@col8
        connection.query(sql, (err, result) => {
            if (err) {
                console.error('error connecting: ' + err.stack);
                return;
            }
            console.log("M_cif_map_phone result == "+result.length+" M_cif_map_phone result "+result.size+" time "+moment().format('Y-M-D h:mm:ss'));
           // console.dir(result);
        });
        //connection.end();
        
        console.log("M_cif_map_phone insert member Done "+moment().format('Y-M-D h:mm:ss'));
        //set
        setTimeout(function(){
            // core.process_cif_filter_by_phone();
        },30000);
        
      
    },

    process_map_cif_rek_start: function(){
        var connection = mysql.createConnection({
        //connectionLimit : 1000000,
        host: dbHost,
        user: dbUser,
        password: dbPassword,
        database: 'btn_loyalty_program',
        debug: false,
        });
            
        connection.connect();

        
        console.log("map_cif_rek Connected to Mysql"+moment().format('Y-M-D h:mm:ss'));
            var sql=//"INSERT INTO M_cif_map_rek (CIFNO, ACCTNO) SELECT cif, rekening FROM M_member"
            "LOAD DATA LOCAL INFILE '/var/www/html/csv_storage/db_member.csv' INTO TABLE M_cif_map_rek FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n' "+
        "(@col1,@col2,@col3,@col4,@col5,@col6,@col7,@col8) "+
        "set CIFNO=@col5,ACCTNO=@col7 ,point=0";

        connection.query(sql, (err, result) => {
                if (err) {
                console.error('error connecting: ' + err.stack);
                return;
            }
            console.log("M_cif_map_rek result == "+result.length+" M_cif_map_rek result "
            +result.size+" time "+moment().format('Y-M-D h:mm:ss'));
            console.dir(result);
        });
        //connection.end();
        
        console.log("M_cif_map_rek insert member Done "+moment().format('Y-M-D h:mm:ss'));
        // coreModel.process_map_cif_phone_start();
       
    },

    insert_M_Prog_Join: function (flag, path){
      
        // var connection = mysql.createConnection({
        //     //connectionLimit : 1000000,
        //     host: dbHost,
        //     user: dbUser,
        //     password: dbPassword,
        //     database: 'btn_loyalty_program',
        //     debug: false,
        // });
            
        // connection.connect();
            
        // console.log("insert_M_Prog_Join Connected to Mysql mjoin");
       
        var sql="LOAD DATA LOCAL INFILE '/var/www/html/csv_storage/db_insertsMPJ.csv' INTO TABLE M_program_join FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n' (@col1,@col2,@col3,@col4,@col5,@col6,@col7,@col8,@col9) set member_id=@col1,program_id=@col2,member_account=@col3,member_password=@col4,status=@col5, point=@col6, point_expire=@col7,date_related=@col8, total_point=@col9";
        //@col8=CBAL, @col9=hold

        pool.getConnection(function(err,connection){
            if (err) {
                return console.error(moment().format('Y-M-D HH:mm:ss.SSS')+' error: ' + err.message);
            }

            console.log("insert_M_Prog_Join Connected to Mysql pool");

            connection.query(sql,function(err,result){
                connection.release();
                if (err) {
                    console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' insert_m_prog_join error connecting: ' + err.stack);
                    return;
                }

                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" insert_m_prog_join result == "+result.affectedRows+" end insert db "+moment().format('Y-M-D h:mm:ss'));
                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" insert_m_prog_join update table done");
                console.dir(result);
            
                var myInt = setInterval(function () {
                    core.process_point_generation();
                    // core.aktifasi_tbl();
                    // core.prepare_prog_join();
                    clearTimeout(myInt);
                }, 15000);
            });

            connection.on('error', function(err) {      
                console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' insert_m_prog_join error in connection database, status 100: ' + err.stack);
                return;     
            });

        });

        // connection.query(sql, (err, result) => {
        //     // 'LOAD DATA INFILE \'/Users/adas/Downloads/signzy/db_inserts.txt\' INTO TABLE names (firstname, lastname) FIELDS TERMINATED BY \',\'', (err, result) => {
        //     if (err) {
        //         console.error('error connecting: ' + err.stack);
        //         return;
        //     }
        //     console.log("result == "+result.length+" result "+result.affectedRows);
        //     console.dir(result);
        // });
        // //connection.end();
        // // coreModel.process_map_cif_rek_start();
        // var myInt = setInterval(function () {
        //     // core.aktifasi_tbl(); 
        //     // core.prepare_prog_join();
        //     clearTimeout(myInt);
        // }, 15000);
    },

    join_agf_member: function(callback){
       
        var query='SELECT cif.cif, cif.phone, member.*, agf.ACCTNO FROM btn_loyalty_program.M_program_join member, btn_raw.agf  agf, btn_loyalty_program.M_member cif  WHERE member.member_account = agf.ACCTNO and member.member_account = cif.rekening'
        
        var options = {sql: query};
        sql.proccess_query(options, function(rows){
            callback(rows);
        });
      
    
    },

    update_phone:function (flag, path){

        var update =//"UPDATE M_cif_map_phone t2, (SELECT CIFNO, point as total FROM M_cif_map_rek ) t1 SET t2.point = t1.total WHERE t1.CIFNO = t2.CIFNO";
        "UPDATE M_cif_map_phone t2, (SELECT id, point as total FROM M_cif_map_rek ) t1 SET t2.point = t1.total WHERE t1.id = t2.id";
        
        console.log("update "+update);

        pool.getConnection(function(err,connection){
            if (err) {
                return console.error(moment().format('Y-M-D HH:mm:ss.SSS')+' error: ' + err.message);
            }

            console.log(moment().format('Y-M-D HH:mm:ss.SSS')+' UPDATE M_cif_map_phone Connected to the MySQL server Pool.');

            connection.query(update,function(err,rows){
                connection.release();
                if (err) {
                    console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' UPDATE M_cif_map_phone error connecting: ' + err.stack);
                    return;
                }

                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" UPDATE M_cif_map_phone result == "+rows.length+" end insert db "+moment().format('Y-M-D h:mm:ss'));
                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" UPDATE M_cif_map_phone update table done");

                switch(flag) {
                    case "in_member":
                        coreModel.insertMember(array);
                    break;
                    case "in_member_join":
                    console.log("in_member_join prepare insert")
                        //core.insert_M_Prog_Join();
                        console.log("mpj update start process_m_poin_history");
                        break;
                    case "event_other":
                        
                        break;
                    case "event_spesial":
                    
                        break;
                    case "agf_update":
                        //insert_M_Prog_Join();//atau update 
                        console.log("todo update start process_m_poin_history")
                        flag="aft_update"
                        // coreModel.nextProcess(flag, path);
                        var myInt = setInterval(function () { 
                            coreModel.nextProcess(flag, path);
                            clearTimeout(myInt);
                        }, 15000);
                    break;
                    case "aft_update":
                        console.log("aft update start process_m_poin_history");
                        flag="akd_update"
                        // coreModel.nextProcess(flag, path);
                        var myInt = setInterval(function () { 
                            coreModel.nextProcess(flag, path);
                            clearTimeout(myInt);
                        }, 15000);
                    break;
                    case "open_acc":
                        console.log("open_acc update start process_m_poin_history");
                        flag="mob_update"
                        // coreModel.nextProcess(flag, path);
                        var myInt = setInterval(function () { 
                            coreModel.nextProcess(flag, path);
                            clearTimeout(myInt);
                        }, 15000);
                    break;
                    case "akd_update":
                        console.log("akd update start process_m_poin_history");
                        flag="aib_update"
                        // coreModel.nextProcess(flag, path);
                        var myInt = setInterval(function () { 
                            coreModel.nextProcess(flag, path);
                            clearTimeout(myInt);
                        }, 15000);
                    break;
                    case "aib_update":
                        console.log("aib update start process_m_poin_history");
                        flag="open_acc"
                        // coreModel.nextProcess(flag, path);
                        var myInt = setInterval(function () { 
                            coreModel.nextProcess(flag, path);
                            clearTimeout(myInt);
                        }, 15000);
                    break;
                    
                    case "ppc_update":
                        console.log("ppc update start process_m_poin_history");
                        flag="tcrka_update";
                        // coreModel.nextProcess(flag, path);
                        var myInt = setInterval(function () { 
                            coreModel.nextProcess(flag, path);
                            clearTimeout(myInt);
                        }, 15000);
                    break;
                    case "dbol_update":
                        console.log("dbol update start process_m_poin_history");
                        flag="ptln_update";
                        // coreModel.nextProcess(flag, path);
                        var myInt = setInterval(function () { 
                            coreModel.nextProcess(flag, path);
                            clearTimeout(myInt);
                        }, 15000);
                    break;
                    
                    case "ptln_update":
                        console.log("ptln_update update start process_m_poin_history");
                        flag="ppc_update";
                        // coreModel.nextProcess(flag, path);
                        var myInt = setInterval(function () { 
                            coreModel.nextProcess(flag, path);
                            clearTimeout(myInt);
                        }, 15000);
                    break;
                    //setor_update
                    case "setor_update":
                    console.log("setor_update start process_m_poin_history")
                    flag="agf_update"
                    // coreModel.nextProcess(flag, path);
                    var myInt = setInterval(function () { 
                        coreModel.nextProcess(flag, path);
                        clearTimeout(myInt);
                    }, 15000);
                    break;
                    
                    case "tbl_update":
                        console.log("tbl_update update start ");
                        flag ="dbol_update"
                        // coreModel.nextProcess(flag, path);
                        var myInt = setInterval(function () { 
                            coreModel.nextProcess(flag, path);
                            clearTimeout(myInt);
                        }, 15000);
                        break;
                    case "bal_update":
                        console.log("bal_update update finish ");
                        // flag="mob_update"
                        // coreModel.nextProcess(flag, path);
                    break;
                    case "insert_acc_ganda":
                        console.log("insert_acc_ganda update start ");
                        //core.insert_acc_ganda();
                    break;
                    case "tcrka_update":
                        console.log("insert_acc_ganda update start ");
                        flag="setor_update"
                        // coreModel.nextProcess(flag, path);
                        var myInt = setInterval(function () { 
                            coreModel.nextProcess(flag, path);
                            clearTimeout(myInt);
                        }, 15000);
                    break;
                    case "mob_update":
                        console.log("mob_update update start ");
                        flag="all_done"
                        // coreModel.nextProcess(flag, path);
                        var myInt = setInterval(function () { 
                            coreModel.nextProcess(flag, path);
                            clearTimeout(myInt);
                        }, 15000);
                    break;
                    //daily
                    
                }
            
            });

            connection.on('error', function(err) {      
                console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' UPDATE M_cif_map_phone error in connection database, status 100: ' + err.stack);
                return;     
            });
        });



        // var connection = mysql.createConnection({
        //     //connectionLimit : 1000000,
        //     host: '172.15.3.140',
        //     user: 'root',
        //     password: dbPass,
        //     database: 'btn_loyalty_program',
        //     debug: false,
        // });
            
        // connection.connect();

        // connection.query(update, (err, result) => {
        //     if (err) {
        //         console.error('update_phone error connecting: ' + err.stack);
        //         return;
        //     }
        //     console.log("update_phone update insert temp table done");
        //     connection.end();
        //     console.log("update_phone connection close & done"+flag);
        // //
        // switch(flag) {
        //     case "in_member":
        //         coreModel.insertMember(array);
        //     break;
        //     case "in_member_join":
        //     console.log("in_member_join prepare insert")
        //         //core.insert_M_Prog_Join();
        //         console.log("mpj update start process_m_poin_history");
        //         break;
        //     case "event_other":
                
        //         break;
        //     case "event_spesial":
            
        //         break;
        //     case "agf_update":
        //         //insert_M_Prog_Join();//atau update 
        //         console.log("todo update start process_m_poin_history")
        //         flag="aft_update"
        //         // coreModel.nextProcess(flag, path);
        //         var myInt = setInterval(function () { 
        //             coreModel.nextProcess(flag, path);
        //             clearTimeout(myInt);
        //         }, 15000);
        //     break;
        //     case "aft_update":
        //         console.log("aft update start process_m_poin_history");
        //         flag="akd_update"
        //         // coreModel.nextProcess(flag, path);
        //         var myInt = setInterval(function () { 
        //             coreModel.nextProcess(flag, path);
        //             clearTimeout(myInt);
        //         }, 15000);
        //     break;
        //     case "open_acc":
        //         console.log("open_acc update start process_m_poin_history");
        //         flag="mob_update"
        //         // coreModel.nextProcess(flag, path);
        //         var myInt = setInterval(function () { 
        //             coreModel.nextProcess(flag, path);
        //             clearTimeout(myInt);
        //         }, 15000);
        //     break;
        //     case "akd_update":
        //         console.log("akd update start process_m_poin_history");
        //         flag="aib_update"
        //         // coreModel.nextProcess(flag, path);
        //         var myInt = setInterval(function () { 
        //             coreModel.nextProcess(flag, path);
        //             clearTimeout(myInt);
        //         }, 15000);
        //     break;
        //     case "aib_update":
        //         console.log("aib update start process_m_poin_history");
        //         flag="open_acc"
        //         // coreModel.nextProcess(flag, path);
        //         var myInt = setInterval(function () { 
        //             coreModel.nextProcess(flag, path);
        //             clearTimeout(myInt);
        //         }, 15000);
        //     break;
            
        //     case "ppc_update":
        //         console.log("ppc update start process_m_poin_history");
        //         flag="tcrka_update";
        //         // coreModel.nextProcess(flag, path);
        //         var myInt = setInterval(function () { 
        //             coreModel.nextProcess(flag, path);
        //             clearTimeout(myInt);
        //         }, 15000);
        //     break;
        //     case "dbol_update":
        //         console.log("dbol update start process_m_poin_history");
        //         flag="ptln_update";
        //         // coreModel.nextProcess(flag, path);
        //         var myInt = setInterval(function () { 
        //             coreModel.nextProcess(flag, path);
        //             clearTimeout(myInt);
        //         }, 15000);
        //     break;
            
        //     case "ptln_update":
        //         console.log("ptln_update update start process_m_poin_history");
        //         flag="ppc_update";
        //         // coreModel.nextProcess(flag, path);
        //         var myInt = setInterval(function () { 
        //             coreModel.nextProcess(flag, path);
        //             clearTimeout(myInt);
        //         }, 15000);
        //     break;
        //     //setor_update
        //     case "setor_update":
        //     console.log("setor_update start process_m_poin_history")
        //     flag="agf_update"
        //     // coreModel.nextProcess(flag, path);
        //     var myInt = setInterval(function () { 
        //         coreModel.nextProcess(flag, path);
        //         clearTimeout(myInt);
        //     }, 15000);
        //     break;
            
        //     case "tbl_update":
        //         console.log("tbl_update update start ");
        //         flag ="dbol_update"
        //         // coreModel.nextProcess(flag, path);
        //         var myInt = setInterval(function () { 
        //             coreModel.nextProcess(flag, path);
        //             clearTimeout(myInt);
        //         }, 15000);
        //         break;
        //     case "bal_update":
        //         console.log("bal_update update start ");
        //         // flag="mob_update"
        //         // coreModel.nextProcess(flag, path);
        //     break;
        //     case "insert_acc_ganda":
        //     console.log("insert_acc_ganda update start ");
        //     //core.insert_acc_ganda();
        //     break;
        //     case "tcrka_update":
        //     console.log("insert_acc_ganda update start ");
        //     flag="setor_update"
        //     // coreModel.nextProcess(flag, path);
        //     var myInt = setInterval(function () { 
        //         coreModel.nextProcess(flag, path);
        //         clearTimeout(myInt);
        //     }, 15000);
        //     break;
        //     case "mob_update":
        //     console.log("mob_update update start ");
        //     flag="all_done"
        //     // coreModel.nextProcess(flag, path);
        //     var myInt = setInterval(function () { 
        //         coreModel.nextProcess(flag, path);
        //         clearTimeout(myInt);
        //     }, 15000);
        //     break;
        //     //daily
            
        // }
        // });
        
        
    },

    update_rek:function (flag, path, dateStart, dateEnd){
     
        var update =
        "UPDATE M_cif_map_rek t2, (SELECT ACCTNO, SUM(point-expire_point) as total FROM M_point_history WHERE date_related BETWEEN '"+dateStart+"' AND '"+dateEnd+"' GROUP BY ACCTNO) t1 SET t2.point = t1.total WHERE t1.ACCTNO = t2.ACCTNO";
        // "UPDATE M_cif_map_rek t2, (SELECT ACCTNO, SUM(point) as total FROM M_point_history WHERE date_related BETWEEN '"+dateStart+"' AND '"+dateEnd+"' GROUP BY ACCTNO) t1 SET t2.point = t1.total WHERE t1.ACCTNO = t2.ACCTNO";
        //UPDATE M_cif_map_rek t2, (SELECT ACCTNO, SUM(point) as total FROM M_point_history GROUP BY ACCTNO) t1 SET t2.point = t1.total WHERE t1.ACCTNO = t2.ACCTNO
        // SHOW COLUMNS FROM target_table;
        //"INSERT INTO M_cif_map_rek SELECT * FROM table2 ON DUPLICATE KEY UPDATE point = VALUES(point)";
      //UPDATE M_cif_map_rek t2, (SELECT ACCTNO, SUM(point) as total FROM M_point_history WHERE expire_date BETWEEN '2018-07-18 00:00:00.000000' AND '2018-07-22 00:00:00.000000' GROUP BY ACCTNO) t1 SET t2.point = (t2.point-t1.total) WHERE t1.ACCTNO = t2.ACCTNO

        pool.getConnection(function(err,connection){
            if (err) {
                return console.error(moment().format('Y-M-D HH:mm:ss.SSS')+' error: ' + err.message);
            }

            console.log(moment().format('Y-M-D HH:mm:ss.SSS')+' update_rek INTO M_cif_map_rek Connected to the MySQL server Pool.');

            connection.query(update,function(err,rows){
                connection.release();
                if (err) {
                    console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' update_rek INTO M_cif_map_rek error connecting: ' + err.stack);
                    return;
                }

                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" update_rek INTO M_cif_map_rek result == "+rows.length+" end insert db "+moment().format('Y-M-D h:mm:ss'));
                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" update_rek INTO M_cif_map_rek update table done");
            
                var myInt = setInterval(function () {
                    coreModel.update_phone(flag,path);
                    clearTimeout(myInt);
                }, 15000);
            });

            connection.on('error', function(err) {      
                console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' update_rek INTO M_cif_map_rek error in connection database, status 100: ' + err.stack);
                return;     
            });
        });


        // var connection = mysql.createConnection({
        //     //connectionLimit : 1000000,
        //     host: '172.15.3.140',
        //     user: 'root',
        //     password: dbPass,
        //     database: 'btn_loyalty_program',
        //     debug: false,
        // });
            
        // connection.connect();

        // connection.query(update, (err, result) => {
        //     if (err) {
        //         console.error('update_rek INTO M_cif_map_rek update error connecting: ' + err.stack);
        //         return;
        //     }
        //     console.log("update_rek INTO M_cif_map_rek update table done");
          
        //     connection.end();
        //     console.log("update_rek connection close & done");
        //     // coreModel.update_phone(flag,path);//todo not sync
        //     var myInt = setInterval(function () {
        //         // core.tbl_update_mpj(flag, path); 
        //         coreModel.update_phone(flag,path);//todo not sync
        //         clearTimeout(myInt);
        //     }, 20000);
        // });
       
        //case 
        
    },

    update_rek_with_rulecode:function (flag, path, dateStart, dateEnd, rule_code){
        
        var dateNow = moment().format('YYYY-MM-DD');
        var update =
        "UPDATE M_cif_map_rek t2, (SELECT ACCTNO, SUM(point) as total FROM M_point_history WHERE date_related='"+dateNow+"' AND poin_code LIKE '%"+rule_code+"%' GROUP BY ACCTNO) t1 SET t2.point = t2.point+t1.total WHERE t1.ACCTNO = t2.ACCTNO";
        // "UPDATE M_cif_map_rek t2, (SELECT ACCTNO, SUM(point-expire_point) as total FROM M_point_history WHERE date_related BETWEEN '"+dateStart+"' AND '"+dateEnd+"' AND poin_code LIKE '%"+rule_code+"%' GROUP BY ACCTNO) t1 SET t2.point = t2.point+t1.total WHERE t1.ACCTNO = t2.ACCTNO";
        // "UPDATE M_cif_map_rek t2, (SELECT ACCTNO, SUM(point) as total FROM M_point_history WHERE date_related BETWEEN '"+dateStart+"' AND '"+dateEnd+"' GROUP BY ACCTNO) t1 SET t2.point = t1.total WHERE t1.ACCTNO = t2.ACCTNO";
        //UPDATE M_cif_map_rek t2, (SELECT ACCTNO, SUM(point) as total FROM M_point_history GROUP BY ACCTNO) t1 SET t2.point = t1.total WHERE t1.ACCTNO = t2.ACCTNO
        // SHOW COLUMNS FROM target_table;
        //"INSERT INTO M_cif_map_rek SELECT * FROM table2 ON DUPLICATE KEY UPDATE point = VALUES(point)";
      //UPDATE M_cif_map_rek t2, (SELECT ACCTNO, SUM(point) as total FROM M_point_history WHERE expire_date BETWEEN '2018-07-18 00:00:00.000000' AND '2018-07-22 00:00:00.000000' GROUP BY ACCTNO) t1 SET t2.point = (t2.point-t1.total) WHERE t1.ACCTNO = t2.ACCTNO

        pool.getConnection(function(err,connection){
            if (err) {
                return console.error(moment().format('Y-M-D HH:mm:ss.SSS')+' error: ' + err.message);
            }

            console.log(moment().format('Y-M-D HH:mm:ss.SSS')+' update_rek_with_rulecode INTO M_cif_map_rek Connected to the MySQL server Pool.');

            connection.query(update,function(err,rows){
                connection.release();
                if (err) {
                    console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' update_rek_with_rulecode INTO M_cif_map_rek error connecting: ' + err.stack);
                    return;
                }

                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" update_rek_with_rulecode INTO M_cif_map_rek result == "+rows.length+" end insert db "+moment().format('Y-M-D h:mm:ss'));
                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" update_rek_with_rulecode INTO M_cif_map_rek update table done");
            
                var myInt = setInterval(function () {
                    coreModel.update_phone(flag,path);
                    clearTimeout(myInt);
                }, 15000);
            });

            connection.on('error', function(err) {      
                console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' update_rek_with_rulecode INTO M_cif_map_rek error in connection database, status 100: ' + err.stack);
                return;     
            });
        });
        
    },

    update_rek_expire:function (dateStart, dateEnd){

        var currdate=moment().format('YYYY-MM-DD' );
        var update =
        "UPDATE M_cif_map_rek t2, (SELECT ACCTNO, SUM(expire_point) as total FROM M_point_history WHERE expire_date LIKE '%"+currdate+"%' GROUP BY ACCTNO) t1 SET t2.point = GREATEST(t2.point-t1.total, 0) WHERE t1.ACCTNO = t2.ACCTNO";
        // "UPDATE M_cif_map_rek t2, (SELECT ACCTNO, SUM(point-expire_point) as total FROM M_point_history GROUP BY ACCTNO) t1 SET t2.point = t1.total WHERE t1.ACCTNO = t2.ACCTNO";
        // "UPDATE M_cif_map_rek t2, (SELECT ACCTNO, SUM(expire_point) as total FROM M_point_history WHERE expire_date BETWEEN '"+dateStart+"' and '"+dateEnd+"' GROUP BY ACCTNO) t1 SET t2.point = GREATEST(t2.point-t1.total, 0) WHERE t1.ACCTNO = t2.ACCTNO";
        // "UPDATE M_cif_map_rek t2, (SELECT ACCTNO, SUM(point) as total FROM M_point_history WHERE expire_date BETWEEN '"+dateStart+"' and '"+dateEnd+"'  GROUP BY ACCTNO) t1 SET t2.point = t2.point-t1.total WHERE t1.ACCTNO = t2.ACCTNO";

        pool.getConnection(function(err,connection){
            if (err) {
                return console.error(moment().format('Y-M-D HH:mm:ss.SSS')+' error: ' + err.message);
            }

            console.log(moment().format('Y-M-D HH:mm:ss.SSS')+' update_rek_expire M_cif_map_rek Connected to the MySQL server Pool.');

            connection.query(update,function(err,result){
                console.log(moment().format('Y-M-D HH:mm:ss.SSS')+ " query : " + update);
                connection.release();
                if (err) {
                    console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' update_rek_expire M_cif_map_rek error connecting: ' + err.stack);
                    return;
                }

                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" update_rek_expire M_cif_map_rek result == "+result.affectedRows+" end insert db "+moment().format('Y-M-D h:mm:ss'));
                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" update_rek_expire M_cif_map_rek update table done");
            
                var myInt = setInterval(function () {
                    coreModel.update_phone_expire();
                    clearTimeout(myInt);
                }, 15000);
            });

            connection.on('error', function(err) {      
                console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' update_rek_expire M_cif_map_rek error in connection database, status 100: ' + err.stack);
                return;     
            });
        });
        
    },

    get_program_period: function(callback){
        var query = "SELECT * FROM M_program_period";
        console.log("get_program_period = "+query)
        var options = {sql: query};
            sql.proccess_query(options, function(rows){
                callback(rows);
            });
    },

    get_burn_date: function(callback){
        var query = "SELECT * FROM M_burn_date";
        console.log("get_burn_date = "+query)
        var options = {sql: query};
            sql.proccess_query(options, function(rows){
                callback(rows);
            });
    },

    burn_M_cif_map_rek:function (dateStart, dateEnd){
        var update =
        "UPDATE M_cif_map_rek SET point = 0";

        pool.getConnection(function(err,connection){
            if (err) {
                return console.error(moment().format('Y-M-D HH:mm:ss.SSS')+' error: ' + err.message);
            }

            console.log(moment().format('Y-M-D HH:mm:ss.SSS')+' burn_rek_expire M_cif_map_rek Connected to the MySQL server Pool.');

            connection.query(update,function(err,result){
                console.log(moment().format('Y-M-D HH:mm:ss.SSS')+ " query : " + update);
                connection.release();
                if (err) {
                    console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' burn_rek_expire M_cif_map_rek error connecting: ' + err.stack);
                    return;
                }

                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" burn_rek_expire M_cif_map_rek result == "+result.affectedRows+" end insert db "+moment().format('Y-M-D h:mm:ss'));
                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" burn_rek_expire M_cif_map_rek update table done");
            
                var myInt = setInterval(function () {
                    coreModel.burn_M_cif_map_phone();
                    clearTimeout(myInt);
                }, 10000);
            });

            connection.on('error', function(err) {      
                console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' burn_rek_expire M_cif_map_rek error in connection database, status 100: ' + err.stack);
                return;     
            });
        });
        
    },

    burn_M_cif_map_phone:function (dateStart, dateEnd){
        var update =
        "UPDATE M_cif_map_phone SET point = 0";

        pool.getConnection(function(err,connection){
            if (err) {
                return console.error(moment().format('Y-M-D HH:mm:ss.SSS')+' error: ' + err.message);
            }

            console.log(moment().format('Y-M-D HH:mm:ss.SSS')+' burn_rek_expire M_cif_map_phone Connected to the MySQL server Pool.');

            connection.query(update,function(err,result){
                console.log(moment().format('Y-M-D HH:mm:ss.SSS')+ " query : " + update);
                connection.release();
                if (err) {
                    console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' burn_rek_expire M_cif_map_phone error connecting: ' + err.stack);
                    return;
                }

                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" burn_rek_expire M_cif_map_phone result == "+result.affectedRows+" end insert db "+moment().format('Y-M-D h:mm:ss'));
                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" burn_rek_expire M_cif_map_phone update table done");
            });

            connection.on('error', function(err) {      
                console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' burn_rek_expire M_cif_map_phone error in connection database, status 100: ' + err.stack);
                return;     
            });
        });
        
    },

    reset_fail_otp:function (){
        var update =
        "UPDATE M_otp_fail_attempt SET attempt_count = 0";

        pool.getConnection(function(err,connection){
            if (err) {
                return console.error(moment().format('Y-M-D HH:mm:ss.SSS')+' error: ' + err.message);
            }

            console.log(moment().format('Y-M-D HH:mm:ss.SSS')+' reset otp Connected to the MySQL server Pool.');

            connection.query(update,function(err,result){
                console.log(moment().format('Y-M-D HH:mm:ss.SSS')+ " query : " + update);
                connection.release();
                if (err) {
                    console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' reset otp error connecting: ' + err.stack);
                    return;
                }

                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" reset otp result == "+result.affectedRows+" end insert db "+moment().format('Y-M-D h:mm:ss'));
                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" reset otp update table done");
            });

            connection.on('error', function(err) {      
                console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' reset otp error in connection database, status 100: ' + err.stack);
                return;     
            });
        });
        
    },

    update_phone_expire:function (){

        var update ="UPDATE M_cif_map_phone t2, (SELECT id, point as total FROM M_cif_map_rek ) t1 SET t2.point = t1.total WHERE t1.id = t2.id";
        
        console.log("update "+update);

        pool.getConnection(function(err,connection){
            if (err) {
                return console.error(moment().format('Y-M-D HH:mm:ss.SSS')+' error: ' + err.message);
            }

            console.log(moment().format('Y-M-D HH:mm:ss.SSS')+' update_phone_expire Connected to the MySQL server Pool.');

            connection.query(update,function(err,result){
                console.log(moment().format('Y-M-D HH:mm:ss.SSS')+ " query : " + update);
                connection.release();
                if (err) {
                    console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' update_phone_expire error connecting: ' + err.stack);
                    return;
                }

                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" update_phone_expire result == "+result.affectedRows+" end insert db "+moment().format('Y-M-D h:mm:ss'));
                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" update_phone_expire update table done");
            
            });

            connection.on('error', function(err) {      
                console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' update_phone_expire error in connection database, status 100: ' + err.stack);
                return;     
            });
        });

        // var connection = mysql.createConnection({
        //     //connectionLimit : 1000000,
        //     host: '172.15.3.140',
        //     user: 'root',
        //     password: dbPass,
        //     database: 'btn_loyalty_program',
        //     debug: false,
        // });
            
        // connection.connect();

        // connection.query(update, (err, result) => {
        //     if (err) {
        //         console.error('update_phone error connecting: ' + err.stack);
        //         return;
        //     }
        //     console.log("update_phone update insert temp table done result : "+ result.affectedRows);
        //     connection.end();
        //     console.log("update_phone connection close & done");
            
        // });
        
        
    },

    //core.aktifasi_tbl(); //1 1004 
       //core.aktifasi_dbol(); //2 1002     
        //core.aktifasi_ptln(); //3 1003 //15000 
        //core.aktifasi_ppc(); //4 1001
        //core.aktifasi_tcrka();//5 1006 
        //core.aktifasi_setor();//6 1005   

        //core.aktifasi_agf(); //7 
       //core.aktifasi_aft(); //8   
        //core.aktifasi_akd(); //9
       // core.aktifasi_aib(); //10   
       //core.aktifasi_openAcc(); //11

       //core.aktifasi_balance(); //12 bulanan di start di tgl 27 ga perlu diulang
        //core.aktifasi_mob()//13
    nextProcess:function (flag, path){
        console.log("nextProcess flag  = "+flag);
        switch(flag) {
            case "in_member":
                coreModel.insertMember(array);
            break;
            case "in_member_join":
            console.log("in_member_join prepare insert")
                coreModel.insert_M_Prog_Join(flag, path);
                break;
            case "event_other":
                
                break;
            case "event_spesial":
            
                break;
            case "agf_update":
                //insert_M_Prog_Join();//atau update 
                console.log("todo update m prog join")
                //coreModel.update_phone(flag, path);
                core.aktifasi_agf()

            break;
            case "aft_update":
                console.log("aft update start");
                core.aktifasi_aft();
            break;
            case "akd_update":
                console.log("akd update start");
                core.aktifasi_akd();
            break;
            case "aib_update":
                console.log("aib update start");
                core.aktifasi_aib();
            break;
            
            case "ppc_update":
                console.log("ppc update start");
                core.aktifasi_ppc();
            break;
            case "dbol_update":
                console.log("dbol update start");
                core.aktifasi_dbol();
            break;
            
            case "ptln_update":
                console.log("ptln_update update start ");
                core.aktifasi_ptln();
            break;
            //setor_update
            case "setor_update":
            console.log("setor_update jgn lupa dihidupkan")
            core.aktifasi_setor();
            break;
            
            case "tbl_update": 
                console.log("tbl_update update start ");
                core.aktifasi_tbl();
            break;
            case "bal_update":
                console.log("bal_update update start ");
                core.aktifasi_balance();
            break;
            case "insert_acc_ganda":
            console.log("insert_acc_ganda update start ");
            core.insert_acc_ganda(flag, path);
            break;
            case "tcrka_update":
            console.log("tcrka_update update start ");
            core.aktifasi_tcrka();
            break;
            case "cif_ganda":
            console.log("cif_ganda insert start ");
            //core.tcrka_update_mpj(flag, path);
            coreModel.insert_cif(flag,path)
            break;
            case "open_acc":
                console.log("open_acc update start process_m_poin_history");
                core.aktifasi_openAcc()
            break;
            case "mob_update":
                console.log("mob_update insert start ");
                //core.tbl_update_mob(flag, path); 
                core.aktifasi_mob()
            break;
           
            case "all_done":
            // console.log("all_done all proses end ");
            console.log(moment().format('Y-M-D HH:mm:ss.SSS')+ " all_done all m_poin_history proses done, continue to update_mpj");
            core.test();
            flag="tbl_update";
            core.process_update_mpj(flag);
            break;

            default:
            break;
            //daily
            
        }
    },

    //deprecated not use any more only for testing
    check_cif:function(callback){
        
        var cif_total_poin=[]
        //"UPDATE M_cif_map_phone t2, (SELECT id, point as total FROM M_cif_map_rek ) t1 SET t2.point = t1.total WHERE t1.id = t2.id"
        //"UPDATE M_cif_map_rek t2, (SELECT ACCTNO, SUM(point) as total FROM M_point_history WHERE date_related BETWEEN '2018-07-18 00:00:00.000000' AND '2018-07-19 00:00:00.000000' GROUP BY ACCTNO) t1 SET t2.point = t1.total WHERE t1.ACCTNO = t2.ACCTNO"
      
        var query=
         "SELECT CIFNO, (SUM(point)) AS Total FROM M_cif_map_rek GROUP BY CIFNO";
         //update point single user by cif
        // "SELECT CIFNO, (SUM(point)) AS Total FROM M_cif_map_rek WHERE CIFNO LIKE '%N926344%' GROUP BY CIFNO"

        //"SELECT mrek.id,mrek.CIFNO,mrek.ACCTNO as rek , (SUM(mhis.point)) as total FROM M_point_history mhis, M_cif_map_rek mrek WHERE mhis.ACCTNO= mrek.ACCTNO GROUP BY mrek.id"
       
        var options = {sql: query};
        sql.proccess_query(options, function(rows){
            console.dir("check_cif "+rows)
            let path = '/var/www/html/csv_storage/db_total_point.csv';  
        //fs.unlinkSync(path);
             var file = fs.createWriteStream('/var/www/html/csv_storage/db_total_point.csv');
             var id=1;
             rows.forEach(function(item){
                 //console.dir(item)
                 //console.log("item "+item.CIFNO)
                 cif_total_poin.push(
                     [
                        id++,
                        item.CIFNO,
                        item.Total
                     ]
                 )
             });
             console.log("cif_total_poin "+cif_total_poin.length)
             core.writeFile(file, path, cif_total_poin, "update_poin");
            //callback(rows);
        });
    },

    //deprecated not use any more only for testing realated to check_cif()
    insert_point:function (flag, path){
       

       
        var createTemp ="CREATE TEMPORARY TABLE IF NOT EXISTS table2 AS SELECT * FROM M_cif_total_point";
        var disForKeyCheck ="SET foreign_key_checks = 0";
        var dropPrimary ="DROP INDEX `PRIMARY` ON temporary_table";
        var emptyData= "TRUNCATE TABLE table2";
        var dropTemptable="DROP TEMPORARY TABLE table2;";
        var optCreateTemp ={sql: createTemp};
        var insert = "LOAD DATA LOCAL INFILE '"+path+"'INTO TABLE table2 FIELDS TERMINATED BY ';' "+
        "LINES TERMINATED BY '\n' (@col1,@col2,@col3) "+
        "set id=@col1, CIFNO=@col2,total=@col3";
        //"set id=@col1,member_id=@col2,program_id=@col3,member_account=@col4,member_password=@col5,status=@col6, point=@col7, point_expire=@col8,date_related=@col9";
        var update =
        // SHOW COLUMNS FROM target_table;
        "INSERT INTO M_cif_total_point SELECT * FROM table2 ON DUPLICATE KEY UPDATE total = VALUES(total)";
        var options = {sql:insert};
        var emptingData ={sql:emptyData};
        var optDropTempTable={sql: dropTemptable};

        var connection = mysql.createConnection({
            //connectionLimit : 1000000,
            host: dbHost,
            user: dbUser,
            password: dbPassword,
            database: 'btn_loyalty_program',
            debug: false,
        });
            
        connection.connect();
        var query='SELECT * FROM M_cif_total_point';
        
        var options = {sql: query};
        sql.proccess_query(options, function(rows){
            //callback(rows);
            if(rows.length>0){
                connection.query(createTemp, (err, result) => {
                    // 'LOAD DATA INFILE \'/Users/adas/Downloads/signzy/db_inserts.txt\' INTO TABLE names (firstname, lastname) FIELDS TERMINATED BY \',\'', (err, result) => {
                    if (err) {
                        console.error('update_point createTemp error connecting: ' + err.stack);
                        return;
                    }
                    //console.log("result == "+result.length+" result "+result.size);
                    console.dir(result);
                    connection.query(insert, (err, result) => {
                        if (err) {
                            console.error('update_point insert temp error connecting: ' + err.stack);
                            return;
                        }
                        console.log("update_point insert temp table done");
                        connection.query(update, (err, result) => {
                            if (err) {
                                console.error('update_point INTO M_cif_map_rek update error connecting: ' + err.stack);
                                return;
                            }
                            console.log("update_point INTO M_cif_map_rek update table done");
                            connection.query(optDropTempTable,(err, result)=>{
                                console.log("update_point drop temp table done");
                                connection.end();
                                console.log("update_point connection close & done");
                                
                            });
                        });
                    });
                    
                });
            }else{
                //insert new data
                console.log('insert new data: ');
                var sql="LOAD DATA LOCAL INFILE '"+path+"'INTO TABLE M_cif_total_point FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n' (@col1,@col2,@col3) set CIFNO=@col2,total=@col3";
                //@col8=CBAL, @col9=hold
                connection.query(sql, (err, result) => {
                    // 'LOAD DATA INFILE \'/Users/adas/Downloads/signzy/db_inserts.txt\' INTO TABLE names (firstname, lastname) FIELDS TERMINATED BY \',\'', (err, result) => {
                    if (err) {
                        console.error('error connecting: ' + err.stack);
                        return;
                    }
                    console.log("result == "+result.length+" result "+result.size);
                    console.dir(result);
                });
            }
        });
        


        
        
        
    },

    check_point_expire:function(cif,callback){
        //dateStart 1 day before , dateEnd tommorow
        var dateStart=moment().subtract(8, 'day').format('Y-M-D h:mm:ss')
        var dateEnd= moment().add(1, 'day').format('Y-M-D h:mm:ss')
        var query=
        "SELECT expire_date, ACCTNO, SUM(point-expire_point) as total FROM M_point_history WHERE CIFNO LIKE '%"+cif+"%' AND `expire_date` BETWEEN '"+dateStart+"' and '"+dateEnd+"' GROUP BY ACCTNO, expire_date";
        // "SELECT expire_date, ACCTNO, SUM(point) as total FROM M_point_history WHERE CIFNO LIKE '%"+cif+"%' AND `expire_date` BETWEEN '"+dateStart+"' and '"+dateEnd+"' GROUP BY ACCTNO, expire_date";
       // "SELECT ACCTNO,point) as total FROM M_point_history WHERE  CIFNO LIKE '%"+cif+"%' AND expire_date BETWEEN '"+dateStart+"' and '"+dateEnd+"' GROUP BY CIFNO";
        //SELECT CIFNO, SUM(point) as total FROM M_point_history WHERE CIFNO LIKE '%N926344%' AND `expire_date` BETWEEN '2018-07-18 00:00:00.000000' AND '2018-07-23 00:00:00.000000' GROUP BY CIFNO
        var options = {sql: query};
        sql.proccess_query(options, function(rows){
            
                callback(rows)
        });
    },

    get_total_point:function(cif,callback){
        var query=
        "SELECT CIFNO, (SUM(point)) AS Total FROM M_cif_map_rek WHERE CIFNO LIKE '%"+cif+"%' GROUP BY CIFNO";
        var options = {sql: query};
        sql.proccess_query(options, function(rows){
            callback(rows);
        });

    },

    get_rek_point:function(cif,callback){
        var query=
        "SELECT * FROM M_cif_map_rek WHERE CIFNO LIKE '%"+cif+"%'";
        var options = {sql: query};
        sql.proccess_query(options, function(rows){
            callback(rows);
        });

    },

    update_rek_point_by_cif:function(cif,acctno, point_updated,new_total,callback){
        var update ="UPDATE M_cif_map_rek set point = "+point_updated+" where ACCTNO = "+acctno;
        var member_query="select phone from M_member where rekening = "+acctno;
        
        if(acctno==cif){
            console.log("cif == acctno ");
            get_rek="select * from M_cif_map_rek where CIFNO LIKE '%"+acctno+"%' ";
            get_phone="select * from M_cif_map_phone where CIFNO LIKE '%"+acctno+"%' ";
            update ="UPDATE M_cif_map_rek set point = 0 where CIFNO LIKE '%"+acctno+"%' ";
            update_phone="UPDATE M_cif_map_phone set point = 0 where CIFNO LIKE '%"+acctno+"%' ";
            var options = {sql: update};
            var options_phone = {sql: update_phone};
            var options_get_rek = {sql: get_rek};
            var options_get_phone= {sql: get_phone};
            var acc=0;
            sql.proccess_query(options_get_rek, function(rows){
                if(rows.length>0){
                    acc=rows[0].ACCTNO;
                    update1 ="UPDATE M_cif_map_rek set point = "+point_updated+" where ACCTNO ="+acc;       
                    var options1 = {sql: update1};
                    sql.proccess_query(options, function(rows){
                        if(typeof rows.affectedRows != 'undefined' ){

                            sql.proccess_query(options1, function(rows){
                                console.log("update point rek map start " + rows.length+" rows.affectedRows "+rows.affectedRows )
                                if(typeof rows.affectedRows != 'undefined' ){
                                    sql.proccess_query(options_phone, function(rows){
                                        if(typeof rows.affectedRows != 'undefined' ){
                                            console.log("update point phone map start")
                                            sql.proccess_query(options_get_phone, function(rows){
                                                if(rows.length>0){
                                                    phone=rows[0].PHONE;
                                                    update_phone1="UPDATE M_cif_map_phone set point = "+point_updated+" where PHONE = "+phone;
                                                    var options_phone1 = {sql: update_phone1};
                                                    sql.proccess_query(options_phone1, function(rows){
                                                        console.log("update point phone map =0")
                                                        if(typeof rows.affectedRows != 'undefined'){
                                                            var update_total_point="UPDATE M_cif_total_point set total = "+new_total+" where CIFNO LIKE '%"+cif+"%'";
                                                            sql.proccess_query(update_total_point, function(rows){
                                                                callback(rows);
                                                                console.log('log update_rek_poin_by_cif'+rows);
                                                                console.dir('dir update_rek_poin_by_cif'+rows);
                                                            });
                                                        }
                                                    });
                                                }
                
                                            });
                                        }
                                    });
                                    
                                    
                                }
                            });
                        }
                    });
                    
                 }

            });
            
        
        }else{
            update ="UPDATE M_cif_map_rek set point = "+point_updated+" where ACCTNO = "+acctno;
            member_query="select phone from M_member where rekening = "+acctno;
            var options = {sql: update};
            var options_get = {sql: member_query};
            
            sql.proccess_query(options, function(rows){

                if(typeof rows.affectedRows != 'undefined' && (rows.affectedRows == 1 || rows.affectedRows == 0)){
                    sql.proccess_query(options_get, function(rows){
                        //callback(rows);
                        if (rows.length>0){
                            var update_cif_phone ="UPDATE M_cif_map_phone set point = "+point_updated+" where PHONE = "+rows[0].phone;
                            sql.proccess_query(update_cif_phone, function(rows){
                                if(typeof rows.affectedRows != 'undefined' && (rows.affectedRows == 1 || rows.affectedRows == 0)){
                                    var update_total_point="UPDATE M_cif_total_point set total = "+new_total+" where CIFNO LIKE '%"+cif+"%'";
                                    sql.proccess_query(update_total_point, function(rows){
                                        callback(rows);
                                    });
                                }
                                
                                
                            });
                        }
                    });
                }
            });

        }
        
    },

    deduct_rek_point_by_cif:function(cif, point_deducted,callback) {
        var update ="UPDATE M_cif_map_rek set point = GREATEST(point-"+point_deducted+",0) where CIFNO LIKE '%"+cif+"%' ";
        var options = {sql: update};
        sql.proccess_query(options, function(result){
            console.log("update M_cif_map_rek " + result.affectedRows);
            var update2 = "UPDATE M_cif_map_phone SET point=(SELECT point FROM M_cif_map_rek WHERE CIFNO LIKE '%"+cif+"%') WHERE CIFNO LIKE '%"+cif+"%' ";
            var options2 = {sql: update2};
            sql.proccess_query(options2, function(result_update){
                console.log("update M_cif_map_phone " + result_update.affectedRows);
                callback(result);
            });
        });
    },

    get_raw_mob:function(dateStart,dateEnd,callback){
        //update `mstr_mob` SET DATE = '2018-05-26 00:00:00.000000' WHERE `id` < 200001 AND id > 10000
        console.log("models get_raw_mob start");
        //var options_mob ='SELECT * FROM mstr_mob ';
        //'2018-05-31 00:00:00.000000' AND '2018-06-27 00:00:00.000000'
        var whereSQL= "and DATE BETWEEN '"+dateStart+"' and '"+dateEnd+"' ";
        var options_raw ="select * from mstr_mob";
        //"SELECT m.*, ml.*  FROM balance m join mstr_mob ml WHERE  m.ACCTNO = ml.ACCTNO ";//+whereSQL;
        //"SELECT m.*, ml.*  FROM balance m join mstr_mob ml WHERE  m.ACCTNO = ml.ACCTNO "+whereSQL;
        var options = {sql:options_raw };
        console.log("models get_raw_mob start");
        sql3.proccess_query3(options, function(rows){
            console.log("get_raw_mob length"+rows.length);
            callback(rows);
        });
        
    },

    get_special_event: function(dateYesterday,callback){
        var query = "SELECT * FROM M_spesial_day WHERE event_date LIKE '%"+dateYesterday+"%' ";
        console.log("get_special_event = "+query)
        var options = {sql: query};
            sql.proccess_query(options, function(rows){
                callback(rows);
            });
    },

    get_special_event_point: function(dateToday, bonus, callback){
        var decimal = bonus/100;
        var query = "SELECT ACCTNO, member_id, CIFNO, FLOOR(SUM(point*"+decimal+")) AS bonus_point FROM `M_point_history` WHERE date_related LIKE '%"+dateToday+"%' GROUP BY ACCTNO, member_id, CIFNO";
        console.log("get_special_event_point = "+query)
        var options = {sql: query};
            sql.proccess_query(options, function(rows){
                callback(rows);
            });
    },

    get_birthday_event: function(event_name, callback){
        var query = "SELECT * FROM M_spesial_day WHERE name='"+event_name+"' ";
        console.log("get_birthday_event = "+query)
        var options = {sql: query};
            sql.proccess_query(options, function(rows){
                callback(rows);
            });
    },

    get_birthday_event_point: function(dateYesterday, dateToday, bonus, callback){
        var decimal = bonus/100;
        var query = "SELECT h.ACCTNO, h.member_id, h.CIFNO, FLOOR(SUM(h.point*"+decimal+")) AS bonus_point FROM `M_point_history` h INNER JOIN M_member m ON h.ACCTNO = m.rekening WHERE h.date_related LIKE '%"+dateToday+"%' AND m.dob LIKE '%"+dateYesterday+"%' GROUP BY ACCTNO, member_id, CIFNO";
        console.log("get_birthday_event_point = "+query)
        var options = {sql: query};
            sql.proccess_query(options, function(rows){
                callback(rows);
            });
    },

    get_giift_rule: function(callback){
        var query = "SELECT * FROM M_giift_point_value";
        console.log("get M_giift_point_value = "+query)
        var options = {sql: query};
            sql.proccess_query(options, function(rows){
                callback(rows);
            });
    },

    insert_giift_list: function(callback){
        var truncate="TRUNCATE TABLE M_giift_list";
        var path = '/var/www/html/csv_storage/db_giift_list.csv'; 
        var sql= 
        "LOAD DATA LOCAL INFILE '"+path+"' INTO TABLE M_giift_list FIELDS TERMINATED BY '|' LINES TERMINATED BY '`' "+
        "(@col1,@col2,@col3,@col4,@col5,@col6,@col7,@col8,@col9,@col10,@col11,@col12,@col13,@col14) "+
        "set dmo_id=@col1,type=@col2,name=@col3,img=@col4,value=@col5,price=@col6,points=@col7,unit=@col8,bonus=@col9,fees=@col10,tc=@col11,description=@col12,country=@col13,category=@col14";
        console.log("sql "+sql);

        pool.getConnection(function(err,connection){
            if (err) {
                return console.error(moment().format('Y-M-D HH:mm:ss.SSS')+' error: ' + err.message);
            }

            console.log(moment().format('Y-M-D HH:mm:ss.SSS')+' insert_giift_list Connected to the MySQL server Pool.');

            connection.query(truncate,function(err,rows){
                if (err) {
                    console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' insert_giift_list error execute query: ' + err.stack);
                    return;
                }

                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" insert_giift_list truncate result == "+rows.affectedRows+" "+moment().format('Y-M-D h:mm:ss'));
                console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" insert_giift_list truncate table done");

                connection.query(sql,function(err,rows){
                    connection.release();
                    if (err) {
                        console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' insert_giift_list error executing query: ' + err.stack);
                        return;
                    }
    
                    console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" insert_giift_list insert result == "+rows.affectedRows+" "+moment().format('Y-M-D h:mm:ss'));
                    console.log(moment().format('Y-M-D HH:mm:ss.SSS') +" insert_giift_list insert table done");
                    callback(rows);
                });
            });

            connection.on('error', function(err) {      
                console.error(moment().format('Y-M-D HH:mm:ss.SSS') +' insert_giift_list error in connection database, status 100: ' + err.stack);
                return;     
            });
        });
    },

    get_M_history:function(transcode,callback){
        var query="SELECT m.*, g.tc FROM btn_loyalty_member.`M_history` m LEFT JOIN btn_loyalty_program.`M_giift_list` g ON m.`giift_dmo_id` = g.`dmo_id` WHERE m.`transcode_btn`='"+transcode+"'"
        // "SELECT * FROM M_history WHERE transcode_btn='"+transcode+"'";
        var options = {sql: query};
        sql2.proccess_query2(options, function(rows){
            callback(rows);
        });
    },

    get_giift_email:function(callback) {
        var query="SELECT * from M_giift_email";
        var options = {sql: query};
        sql.proccess_query(options, function(rows){
            callback(rows);
        });
    },
    

};
module.exports = {
  list: function(program_code, to_program, callback) {
    var options = {sql: 'SELECT p.program_name,  e.program_name as to_program_name, t.from_ratio, t.to_ratio FROM M_program p, T_point_management t left JOIN M_program e  ON e.id = t.to_program_id WHERE p.program_code = "'+program_code+'" and t.from_program_id = p.id and e.program_code = "'+to_program+'"'};
    sql.proccess_query(options, function(rows){
      callback(rows);
    });
  },
  get: function(program_code, to_program, point, callback) {
    var options = {sql: 'SELECT p.program_name,  e.program_name as to_program_name, t.from_point, t.to_point, t.from_program_id, t.to_program_id FROM M_program p, T_point_management t left JOIN M_program e  ON e.id = t.to_program_id WHERE p.program_code = "'+program_code+'" and t.from_program_id = p.id and e.program_code = "'+to_program+'" and t.from_point <= '+point+' order by t.from_point desc limit 1'};
    sql.proccess_query(options, function(rows){
    callback(rows);
    });
  },
  get_dynamic: function(program_code, to_program, point, callback) {
    var options = {sql: 'SELECT p.program_name,  e.program_name as to_program_name, t.from_point, t.to_point, t.from_program_id, t.to_program_id FROM M_program p, T_point_management t left JOIN M_program e  ON e.id = t.to_program_id WHERE p.program_code = "'+program_code+'" and t.from_program_id = p.id and e.program_code = "'+to_program+'" order by t.from_point desc limit 1'};
    sql.proccess_query(options, function(rows){
      callback(rows);
    });
  },
  listAll: function(program_code, callback) {
    /*
    var options = {sql: 
      //  'SELECT * from T_point_management WHERE to_program_id = '+rows[0].id+''};
      'SELECT p.program_name,  e.program_name as to_program_name, t.from_ratio, t.to_ratio FROM M_program p, T_point_management t left JOIN M_program e  ON e.id = t.to_program_id WHERE p.program_code = "'+program_code+'" and t.from_program_id = p.id '};
        //'SELECT p.program_name,  e.program_name as to_program_name, t.from_point, t.to_point FROM M_program p, T_point_management t left JOIN M_program e  ON e.id = t.to_program_id WHERE p.program_code = "'+program_code+'" and t.from_program_id = p.id and e.program_code = "'+to_program+'"'};
      console.dir(options);
    
      sql.proccess_query(options, function(rows){
        callback(rows);
      });
      */

    var optProg ={ sql:
    'SELECT id from M_program WHERE program_code LIKE "'+ program_code+ '"'
    };
    sql.proccess_query(optProg, function(rows){

      if(rows.length >0){
        var options = {sql: 
          'SELECT * from T_point_management WHERE to_program_id = '+rows[0].id+''};
        //'SELECT p.program_name,  e.program_name as to_program_name, t.from_ratio, t.to_ratio FROM M_program p, T_point_management t left JOIN M_program e  ON e.id = t.to_program_id WHERE p.program_code = "'+program_code+'" and t.from_program_id = p.id '};
          //'SELECT p.program_name,  e.program_name as to_program_name, t.from_point, t.to_point FROM M_program p, T_point_management t left JOIN M_program e  ON e.id = t.to_program_id WHERE p.program_code = "'+program_code+'" and t.from_program_id = p.id and e.program_code = "'+to_program+'"'};
        //console.dir(options);
      
        sql.proccess_query(options, function(rows){
          callback(rows);
        });
      }else{
        callback(rows);
      }
      
    });
    
  },
  get_toRatio: function(program_id, callback){
    var options = {sql: 
      'SELECT * from T_point_management WHERE to_program_id = '+program_id+''};
     //console.dir(options);
  
    sql.proccess_query(options, function(rows){
      callback(rows);
    });
  },
};

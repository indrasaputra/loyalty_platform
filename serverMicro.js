//"use strict";/Users/macbook/Sites/dev_btn
//"use strict";
process.env.NODE_ENV = "development";
var TYPE = 'redis';
var express = require('express');
var app = express();
var memberService = express();
var logService = express();
var exchangeService = express();
var redeemService = express();
var soap = require('soap');
var http = express();
var _ = require('underscore');
//const Json2csvParser = require('json2csv').Parser;


var config = require('config');
var xmlparser = require('express-xml-bodyparser');
var xrpc = require('xrpc');
var cluster = require('cluster');
var cronDOB = require('node-cron');
var cronFD = require('node-cron');
var cronFM = require('node-cron');
var cronEX = require('node-cron'); //expire date poin
var cronMemberRaw = require('node-cron');
var cronRaw = require('node-cron');
var cronGiift = require('node-cron');
var cronPointRemoval = require('node-cron');
var cronOTP = require('node-cron');
var cronBurn = require('node-cron');

global.fs = require('fs');
global.EasyXml = require('easyxml');
global.serializerMember = new EasyXml({
    singularize: true,
    rootArray: 'members',
    rootElement: 'response',
    dateFormat: 'ISO',
    manifest: false
});
global.serializerRecipe = new EasyXml({
    singularize: true,
    rootArray: 'members',
    rootElement: 'response',
    dateFormat: 'ISO',
    manifest: false
});
global.body_parser = require('body-parser');
global.jwt = require('jsonwebtoken');
global.mysql = require("mysql");
global.mysqlUtilities = require('mysql-utilities');
global.moment = require('moment');
global.response = require('./library/generalResponse');
global.conf_core_sys = config.get('CoreSys');
global.sql = require('./m_mysql_proccess');
global.sql2 = require('./m_mysql_proccess2');
global.sql3 = require('./m_mysql_proccess3');
global.exchange = require('./models/Exchange');
global.merchant = require('./models/Merchant');
global.redeem = require('./models/Redeem');
global.member = require('./models/Member');
global.program = require('./models/Program');
global.log = require('./models/Log');
global.category = require('./models/Category');
global.sub_category = require('./models/Sub_category');
global.admin = require('./models/Admin');
global.coreModel = require('./models/Core');
//global.http 					      = require('http');
global.expressValidator = require('express-validator');
var routerController = require('express-route-controller');
var routerControllerMember = require('express-route-controller');
var routerControllerExchange = require('express-route-controller');
var routerControllerLog = require('express-route-controller');
var routerControllerRedeem = require('express-route-controller');
global.util = require('util');
global.upperCase = require('upper-case');
global.log_controller = require('./controllers/Log');
global.core = require('./core/core');
global.crypto = require('crypto');
// global.bcrypt = require('bcrypt-nodejs');
global.cryptkey = "av3DYGLkwBsErphcyYp+imUW4QKs19hU"; // nFyyYcXwURU";
// global.iv = cryptkey.substr(0,16);
global.dbHost = conf_core_sys.dbConfig.host;
global.dbPort = conf_core_sys.dbConfig.port;
global.dbUser = conf_core_sys.dbConfig.user;
global.dbPassword = conf_core_sys.dbConfig.pass;
global.request = require('request');
global.giiftUri = 'https://rest.giift.com/sandbox';
global.nodemailer = require("nodemailer");

// console.log(conf_core_sys.dbConfig.pass);
// console.log("db2  = "+conf_core_sys.dbConfig2.dbName);
var numCPUs = require('os').cpus().length;
app.use(xmlparser());
app.use(body_parser.json());
app.use(expressValidator());
app.use(function(req, res, next) {
    global.token = (typeof req.body.token != 'undefined') ? req.body.token : '';
    global.apikey = (typeof req.headers.apikey != 'undefined') ? req.headers.apikey : '';
    global.decoded = jwt.decode(token, { complete: true });
    console.log(token);
    console.log(apikey);
    next();

});
//memberService
// var memberRoutes = require('./memberRouter');
// memberService.use('/', memberRoutes);
memberService.use(xmlparser());
memberService.use(body_parser.json());
memberService.use(expressValidator());
memberService.use(function(req, res, next) {
    global.token = (typeof req.body.token != 'undefined') ? req.body.token : '';
    global.apikey = (typeof req.headers.apikey != 'undefined') ? req.headers.apikey : '';
    global.decoded = jwt.decode(token, { complete: true });
    console.log(token);
    console.log(apikey);
    next();

});
//exchangeService
exchangeService.use(xmlparser());
exchangeService.use(body_parser.json());
exchangeService.use(expressValidator());
exchangeService.use(function(req, res, next) {
    global.token = (typeof req.body.token != 'undefined') ? req.body.token : '';
    global.apikey = (typeof req.headers.apikey != 'undefined') ? req.headers.apikey : '';
    global.decoded = jwt.decode(token, { complete: true });
    console.log(token);
    console.log(apikey);
    next();

});
//redeemService
redeemService.use(xmlparser());
redeemService.use(body_parser.json());
redeemService.use(expressValidator());
redeemService.use(function(req, res, next) {
    global.token = (typeof req.body.token != 'undefined') ? req.body.token : '';
    global.apikey = (typeof req.headers.apikey != 'undefined') ? req.headers.apikey : '';
    global.decoded = jwt.decode(token, { complete: true });
    console.log(token);
    console.log(apikey);
    next();

});
//logService
logService.use(xmlparser());
logService.use(body_parser.json());
logService.use(expressValidator());
logService.use(function(req, res, next) {
    global.token = (typeof req.body.token != 'undefined') ? req.body.token : '';
    global.apikey = (typeof req.headers.apikey != 'undefined') ? req.headers.apikey : '';
    global.decoded = jwt.decode(token, { complete: true });
    console.log(token);
    console.log(apikey);
    next();

});

//test xml response
app.post('/member/authentication_with_wsdl', xmlparser({ trim: false, explicitArray: false }), function(req, res, next) {
    // check req.body

    if (Object.keys(req.body).length) {
        console.log('Parsed XML', req.body);
        var phone = (typeof req.body.phone != 'undefined') ? req.body.phone : '';
        req.checkBody('phone', "phone can't be empty and must as numeric").notEmpty().isInt();
        // req.checkBody('password', "password can't be empty and must as numeric").notEmpty();
        //var errors  = req.validationErrors();
        member.authentication_with_wsdl(phone, function(rows) {
            if (rows.length > 0) {
                var expires = moment().add(7, 'days').valueOf();
                //var token = jwt.sign({ username: rows[0].email, cif: rows[0].cif, exp: expires},  apikey.toString());
                response.status_200xml("Success", rows, function(cb) {
                    console.log("cb " + cb);
                    //print(cb);
                    res.send(cb);
                    //res.json(cb);
                    //res.xmlparser();
                    //res.sendFile(cb);
                });
            } else {

            }
        });
        //res.send('OK!');
    } else {
        res.send('Not OK :(');
    }
    console.log('post body length', req.rawBody.length); // this is a string
    return res.end();
});

//soap server
var xml = require('fs').readFileSync('globalweather.wsdl', 'utf8').replace('\ufeff', '');
http.use(body_parser.raw({ type: function() { return true; }, limit: '5mb' }));
http.use(function(req, res, next) {
    var body = req.body.toString();
    var body = body.replace('\ufeff', '');
    req.body = Buffer.from(body);
    next();
});
var userCIF = "";
var myService = {
    GlobalUser: { //my service
        GlobalUserSoap: { // MyPort 
            GetMerchant: function(args, callback) {
                var category_id = args.category_id;
                var limit = args.limit;
                var offset = args.offset;
                merchant.list(category_id, limit, offset, function(rows) {
                    if (rows.length > 0) {
                        response.status_200("Success", rows, function(cb) {
                            //res.json(cb)
                            callback(
                                cb
                            );
                        });
                    } else {
                        response.status_204("Failed", "Data not found", function(cb) {
                            //res.json(cb)
                            callback(cb);
                        });
                    }
                });
            },
            GetCategory: function(args, callback) {
                var category_id = args.category_id;
                var limit = args.limit;
                var offset = args.offset;
                category.list(category_id, limit, offset, function(rows) {
                    if (rows.length > 0) {
                        response.status_200("Success", rows, function(cb) {
                            //res.json(cb)
                            callback(
                                cb
                            );
                        });
                    } else {
                        response.status_204("Failed", "Data not found", function(cb) {
                            //res.json(cb)
                            callback(
                                cb
                            );
                        });
                    }
                });
            },
            GetExchange: function(args, callback) { //function
                // do some work
                phone = args.UserPhone;
                var point = args.UserPoint;
                phone_tujuan = args.PhoneTujuan;
                ref_number = args.RefNumber;

                response.status_204("Failed", "Maaf, penukaran poin saat ini tidak bisa dilakukan. Silakan hubungi customer service BTN", function(cb) {
                    callback(cb);
                });

                // if(parseInt(point) ==0 || parseInt(point) < 0){
                //     response.status_204("Failed", "point value not valid", function(cb){
                //       callback(cb);
                //       });
                //      // process.exit();=]
                //   }
                //   var program_code = "BTNC098";
                //   var to_program = "GJAC098";

                // member.authentication_with_phone(phone, function(rows){
                //     var apiKey = 'NxoklkZDL0Cz8GrHAFYzViA8cVv16wP5';
                //     var member_list=rows;
                //     var result = {program_name: program_code, to_program: to_program, ref_number: ref_number, exchange: {"point": point , "result": "", data_member: member_list}};
                //     if(member_list.length > 0){

                //         var expires = moment().add(7, 'days').valueOf();
                //         var token = jwt.sign({ username: rows[0].email, cif: rows[0].cif, exp: expires},  apiKey.toString());
                //         response.status_200(token, rows, function(cb){
                //             //res.json(cb);
                //             console.dir(rows);
                //             console.dir("ok "+cb.cif+" rows[0].cif = "+rows[0].cif+" id "+rows[0].member_id);
                //             userCIF = rows[0].cif;
                //             userID=rows[0].member_id;
                //             var type = 'exchange';
                //             // var program_code = "BTNC098";
                //             // var to_program = "GJAC098";
                //             var new_current_poin = rows[0].point;//point di M_program_join dari query tidak valid ini dirubah get M_cif_total_point


                //             //pindah ke core.verifiedPoint
                //             // var id_tujuan="";
                //             // var code_tujuan ="";
                //             // var cifAccGanda =false;
                //             console.dir("new_current_poin "+new_current_poin);

                //             //check black list
                //             member.get_blacklist_cif(userCIF, function(blacklist){
                //                 if(blacklist.length > 0){
                //                     response.status_204("Hubungi kantor cabang terdekat", result, function(cb){ //"Failed Check Customer Service code 2", result, function(cb){
                //                         //res.json(cb);
                //                         callback(cb);
                //                     });
                //                 }else{
                //                     console.dir("get_blacklist_cif length = "+blacklist.length);

                //                     // =====get total point ====
                //                     coreModel.get_total_point(userCIF, function(total_point){
                //                         console.dir(total_point)
                //                         console.log("get total point "+ total_point[0].Total);
                //                         new_current_poin=total_point[0].Total;

                //                         //=======get rek point =======
                //                         var rek_point=0;
                //                         var flag ="exchange";
                //                         console.log("get new_current_poin = "+ new_current_poin);
                //                         if(new_current_poin>=1000){
                //                             if(new_current_poin < point){
                //                                 result = {program_name: program_code, to_program: to_program, ref_number: ref_number, exchange: {"point": point , "result":"","balance": new_current_poin, data_member: member_list}};
                //                                 response.status_204("Tingkatkan poin Anda untuk dapat melakukan penukaran poin",result , function(cb){ //"Failed Point yang Anda tukar melebihi point yang anda punya",result , function(cb){
                //                                     //res.json(cb);
                //                                     callback(cb);
                //                                 });
                //                             }else{
                //                                 core.verifiedPoint(userCIF,point,rek_point, new_current_poin, program_code,to_program,type,phone_tujuan,userID,ref_number,member_list, function(rows){
                //                                     callback(rows);
                //                                     console.dir(" hasil callback "+rows)
                //                                     console.dir(rows)
                //                                 });
                //                             }


                //                         } else{
                //                             if(new_current_poin == 0) {
                //                                 result = {program_name: program_code, to_program: to_program, ref_number: ref_number, exchange: {"point": point , "result":"","balance": new_current_poin, data_member: member_list}};
                //                                 response.status_204("Cek kembali jumlah poin Anda",result , function(cb){ //"Failed Point Anda tidak mencukupi",result , function(cb){
                //                                     //res.json(cb);
                //                                     callback(cb);
                //                                 });
                //                             } else {
                //                                 result = {program_name: program_code, to_program: to_program, ref_number: ref_number, exchange: {"point": point , "result":"","balance": new_current_poin, data_member: member_list}};
                //                                 response.status_204("Tingkatkan poin Anda untuk dapat melakukan penukaran poin",result , function(cb){ //"Failed Point Anda tidak mencukupi",result , function(cb){
                //                                     //res.json(cb);
                //                                     callback(cb);
                //                                 });
                //                             }
                //                         }

                //                         //=====end get rek point ====
                //                     });
                //                     // ======end get total point ======
                //                 }

                //             });




                //         });
                //     }else{
                //         //var result = {program_name: program_code, to_program: to_program, ref_number: ref_number, exchange: {"point": point , "result": "", data_member: member_list}};
                //       response.status_204("Failed Member not found", result, function(cb){
                //       //res.json(cb)
                //       //console.log("failed"+res.json(cb));
                //         console.dir("failed"+cb);
                //         callback(cb);
                //       }); 
                //     }
                // });
            },

            GetRedeem: function(args, callback) { //function
                // do some work

                response.status_204("Failed", "Maaf, penukaran poin saat ini tidak bisa dilakukan. Silakan hubungi customer service BTN", function(cb) {
                    callback(cb);
                });

                // const program_code = args.program_code;
                // var point = args.UserPoint;
                // const category_id = args.category_id;
                // const phone = args.UserPhone;
                // const merchant_id = args.merchant_id;
                // var type = 'redeem';

                // var normal_point=point;
                // var phone_tujuan="";
                // // var id_tujuan="";
                // // var code_tujuan ="";
                // // var cifAccGanda=false;
                // console.log("GetRedeem program_code callback "+args.program_code," phone "+phone+" merchant_id "+merchant_id);
                // member.authentication_with_phone(phone, function(rows){

                //       //not yet
                //     if(rows.length > 0 ){
                //         var member_list =rows;
                //         var apiKey = 'NxoklkZDL0Cz8GrHAFYzViA8cVv16wP5';
                //         var new_current_poin = rows[0].total_point;
                //         var poin_expire_date = moment.utc(rows[0].point_expire, "Y-M-D h:mm:ss ");
                //         var member_id =rows[0].member_id;
                //         var userCIF = rows[0].cif;
                //         //var totalp

                //         //console.dir(rows);
                //         console.log("length rows "+rows.length+ " new_current_poin "+new_current_poin);
                //         //check black list
                //         member.get_blacklist_cif(userCIF, function(rows){
                //             if(rows.length > 0){
                //                 response.status_204("Failed Check Customer Service code 2", member_list, function(cb){
                //                     callback(cb);
                //                 });
                //             }
                //             coreModel.get_total_point(userCIF, function(total_point){
                //                 console.dir(total_point)
                //                 console.log("get total point "+ total_point[0].Total);
                //                 new_current_poin=total_point[0].Total;
                //                 normal_point=point;
                //                 if(total_point.length>0){

                //                     if(new_current_poin>=1000){
                //                         program.list_program_by_code(program_code, function(rows){
                //                             var program = rows;
                //                             merchant.list(category_id, 1000000, 0, function(merchant_rows){
                //                                 var redeemAmountArr =[];
                //                                // console.dir(merchant_rows)
                //                                 if(merchant_rows.length>0){
                //                                     console.log("normal_point 1 = "+normal_point);

                //                                     redeem.get(program_code, merchant_id, normal_point, function(rows){
                //                                         var redeem_rows = rows;
                //                                         console.log("redeem start rows length = "+redeem.length);
                //                                         console.dir(redeem);

                //                                         if(redeem_rows.length>0){

                //                                             if(parseInt(new_current_poin)!=0 && parseInt(new_current_poin)>=redeem_rows[0].point && new_current_poin>=normal_point){

                //                                                 console.dir(redeem_rows)
                //                                                 console.log("nominal new_current_poin lolos "+new_current_poin+">="+redeem_rows[0].point);
                //                                                 redeemAmountArr.push(redeem_rows[0].amount);

                //                                                 console.log(Math.max(...redeemAmountArr));
                //                                                 var b = [].concat(redeemAmountArr); // clones "a"
                //                                                 b.sort(function (redeemAmountArr, b) { return redeemAmountArr.x - b.x; });
                //                                                 var minAmount = b[0];
                //                                                 var maxAmount = b[b.length - 1];
                //                                                 //console.log("program name = "+redeem[0].merchant_id);
                //                                                 console.log("maxAmount");
                //                                                 console.dir(maxAmount);
                //                                                 console.log("minAmount");
                //                                                 console.dir(minAmount);
                //                                                 var rek_point=0;

                //                                                 core.verifiedRedeemPoint(userCIF,point,rek_point, new_current_poin, program_code,program,type,minAmount,
                //                                                     maxAmount,phone_tujuan,normal_point,redeem_rows,member_id,merchant_id, member_list,function(rows){
                //                                                     callback(rows);
                //                                                 });

                //                                                 //end
                //                                             }else{
                //                                                 //poin tidak cukup
                //                                                 response.status_204("Failed Not Enough poin",  member_list, function(cb){
                //                                                     //res.json(cb);
                //                                                     callback(cb);

                //                                                 }); 
                //                                             }

                //                                         }else{
                //                                             //failed data not found
                //                                             response.status_204("Failed redeem data not exist",  member_list, function(cb){
                //                                             // res.json(cb);
                //                                                 callback(cb)

                //                                             }); 
                //                                         }


                //                                     });

                //                                 }else{
                //                                     response.status_204("Failed redeem data not exist",  member_list, function(cb){
                //                                         // res.json(cb);
                //                                             callback(cb)

                //                                         }); 
                //                                 }

                //                             });

                //                         });
                //                     }else{
                //                         response.status_204("Failed Point Anda tidak mencukupi", member_list, function(cb){
                //                             //res.json(cb);
                //                             callback(cb);

                //                         }); 
                //                     }
                //                 }

                //             });

                //         });



                //     }else{
                //           //
                //         response.status_204("Failed Merchant not found", member_list, function(cb){
                //             //res.json(cb);
                //             callback(cb)

                //         }); 
                //     }
                // });

            },
            GetUserByPhone: function(args, callback) { //function
                console.log("callback " + args.UserPhone);
                phone = args.UserPhone;
                member.authentication_with_phone(phone, function(rows) {
                    var apiKey = 'NxoklkZDL0Cz8GrHAFYzViA8cVv16wP5';

                    if (rows.length > 0) {
                        var expires = moment().add(7, 'days').valueOf();
                        var token = jwt.sign({ username: rows[0].email, cif: rows[0].cif, exp: expires }, apiKey.toString());
                        response.status_200(token, rows, function(cb) {
                            //res.json(cb);
                            console.dir("rows " + rows);
                            console.dir("ok " + cb.cif + " rows[0].cif = " + rows[0].cif + " id " + rows[0].member_id);
                            userCIF = rows[0].cif;
                            console.dir(rows);

                            var new_current_poin = 0;
                            var prog_start_date = '20190201';
                            var prog_end_date = '20200131';
                            coreModel.get_program_period(function(rows_period) {
                                if (rows_period.length > 0) {
                                    prog_start_date = moment(rows_period[0].start_date, 'YYYYMMDD').format('YYYYMMDD');
                                    prog_end_date = moment(rows_period[0].end_date, 'YYYYMMDD').format('YYYYMMDD');
                                }
                            });
                            coreModel.get_total_point(userCIF, function(total_point) {
                                if (total_point.length > 0) {
                                    console.dir(total_point)
                                    console.log("get total point " + total_point[0].Total);
                                    new_current_poin = total_point[0].Total;

                                }
                                callback({

                                    status: 200,
                                    message: "success",
                                    display_message: "success",
                                    data: {
                                        Point: new_current_poin,
                                        Name: rows[0].first_name + ' ' + rows[0].last_name,
                                        Periode: prog_start_date + prog_end_date
                                    },
                                    time: moment().format('YYYY-MM-DD hh:mm:ss')

                                });

                            });

                            /*   
                                log.get_acc_ganda_cif(userCIF,function(totalPoin){
                                    console.log(" log.get_acc_ganda  = "+totalPoin.length)
                                    if(totalPoin.length>1){
                                        new_current_poin = totalPoin[totalPoin.length-1].total_point;
                                        console.log(" new_current_poin 1 = "+new_current_poin)
                                        
                                    }else{
                                        new_current_poin = rows[0].point;
                                        console.log(" new_current_poin 2 = "+new_current_poin)
                                    }
                                    callback({
                                
                                        status              : 200,
                                        message             : "success",
                                        display_message     : "success",
                                        data                : {
                                            Point: new_current_poin,
                                            Name : rows[0].first_name+' '+rows[0].last_name
                                        },
                                        time : moment().format('YYYY-MM-DD hh:mm:ss')
                                    

                                        //name2: args.UserPhone,
                                        //   status: 200,
                                        //   message: Success,
                                        //   display_message:,
                                    
                                    });

                                });
                                
                                //callback(cb);
                            */
                        });
                    } else {
                        response.status_204("Failed", "Member not found", function(cb) {
                            //res.json(cb)
                            //console.log("failed"+res.json(cb));
                            console.dir("failed" + cb);
                            callback(cb);
                        });
                    }
                });
                // callback({
                //     name2: args.CountryName,
                //     cif: rows[0].cif
                // });
            }
        },
        GlobalUserSoap12: { // MyPort 
            GetUser: function(args, callback) { //function
                // do some work

                callback({
                    name3: args.CountryName,
                    name6: args.CityName
                });
            },
            GetUserByPhone: function(args, callback) { //function
                // do some work

                callback({
                    name4: args.CountryName
                });
            }
        }
    }
};



//soap server end

//8076 memberService
routerControllerMember(memberService, {
    controllers: __dirname + '/controllers',
    routes: {
        '/member/profile': { action: 'Member#profile', method: 'post' },
        '/member/update_member': { action: 'Member#update_member', method: 'post' },
        '/member/authentication': { action: 'Member#authentication', method: 'post' },
        '/member/authentication_with_pass': { action: 'Member#authentication_with_pass', method: 'post' },
        '/member/authentication_with_wsdl': { x: xmlparser({ trim: false, explicitArray: false }), action: 'Member#authentication_with_wsdl', method: 'post' },
        '/member/authentication_with_phone': { action: 'Member#authentication_with_phone', method: 'post' },
        '/member/create': { action: 'Member#create', method: 'post' },
        '/member/create_from_feeder': { action: 'Member#create_from_feeder', method: 'post' },
    }
});
//8077 logService
routerControllerLog(logService, {
    controllers: __dirname + '/controllers',
    routes: {
        '/log/proccess': { action: 'Log#proccess', method: 'post' },
        '/log/redeem_history': { action: 'Log#redeem_history', method: 'post' },
        '/log/redeem_history_by_user': { action: 'Log#redeem_history_by_user', method: 'post' },
        '/log/exchange_history': { action: 'Log#exchange_history', method: 'post' },
        '/log/exchange_history_by_user': { action: 'Log#exchange_history_by_user', method: 'post' },
        '/log/total_poin': { action: 'Log#total_poin', method: 'post' },
        '/log/exchange_report': { action: 'Log#exchange_report', method: 'post' },
        '/log/redeem_report': { action: 'Log#redeem_report', method: 'post' },
        '/log/rule_create': { action: 'Log#rule_create', method: 'post' },
        '/log/rule_update': { action: 'Log#rule_update', method: 'post' },
        '/log/rule_list': { action: 'Log#rule_list', method: 'post' },
        //process_feed_daily
        '/log/process_feed_daily': { action: 'Log#process_feed_daily', method: 'post' },
    }
});
//8079 redeemService
routerControllerRedeem(redeemService, {
    controllers: __dirname + '/controllers',
    routes: {
        '/redeem/list': { action: 'Redeem#list', method: 'post' },
        '/redeem/proccess': { action: 'Redeem#proccess', method: 'post' },
        '/merchant/list': { action: 'Merchant#list', method: 'post' },
        '/merchant/create_item': { action: 'Merchant#create_item', method: 'post' },
        '/merchant/create_category': { action: 'Merchant#create_category', method: 'post' },

    }
});
//8078 exchangeService
routerControllerExchange(exchangeService, {
    controllers: __dirname + '/controllers',
    routes: {
        '/program/list': { action: 'Program#list', method: 'post' },
        '/exchange/list': { action: 'Exchange#list', method: 'post' },
        '/exchange/proccess': { action: 'Exchange#proccess', method: 'post' },
        '/exchange/grandprize': { action: 'Exchange#grandprize', method: 'post' },
        '/exchange/proccess_wsdl': { action: 'Exchange#proccess_wsdl', method: 'post' },
        '/exchange/listAll': { action: 'Exchange#listAll', method: 'post' },
    }
});

//8081 standard all apps atau core
routerController(app, {
    controllers: __dirname + '/controllers',
    routes: {
        '/category/list': { action: 'Category#list', method: 'post' },
        '/category/create': { action: 'Category#create', method: 'post' },
        '/voucher/create': { action: 'Voucher#create', method: 'post' },
        '/voucher/merge': { action: 'Voucher#merge', method: 'get' },
        '/voucher/manual': { action: 'Voucher#manual', method: 'get' },

        '/log/proccess': { action: 'Log#proccess', method: 'post' },
        '/log/redeem_history': { action: 'Log#redeem_history', method: 'post' },
        '/log/redeem_history_by_user': { action: 'Log#redeem_history_by_user', method: 'post' },
        '/log/exchange_history': { action: 'Log#exchange_history', method: 'post' },
        '/log/exchange_history_by_user': { action: 'Log#exchange_history_by_user', method: 'post' },
        '/log/history_by_user': { action: 'Log#history_by_user', method: 'post' },
        '/log/total_poin': { action: 'Log#total_poin', method: 'post' },
        '/log/exchange_report': { action: 'Log#exchange_report', method: 'post' },
        '/log/redeem_report': { action: 'Log#redeem_report', method: 'post' },
        '/log/rule_create': { action: 'Log#rule_create', method: 'post' },
        '/log/rule_update': { action: 'Log#rule_update', method: 'post' },
        '/log/rule_list': { action: 'Log#rule_list', method: 'post' },
        '/log/news_list': { action: 'Log#news_list', method: 'post' },
        '/log/news_cat_list': { action: 'Log#news_cat_list', method: 'post' },
        '/log/news_by_cat': { action: 'Log#news_by_cat', method: 'post' },
        '/log/news_by_id': { action: 'Log#news_by_id', method: 'post' },
        //process_feed_daily
        '/log/process_feed_daily': { action: 'Log#process_feed_daily', method: 'post' },
        '/log/manual_burn_point': { action: 'Log#manual_burn_point', method: 'get' },
        '/log/adjust_point_by_rule_code': { action: 'Log#adjust_point_by_rule_code', method: 'post' },

        '/member/profile': { action: 'Member#profile', method: 'post' },
        //update_member
        '/member/update_member': { action: 'Member#update_member', method: 'post' },
        //update_member_password
        '/member/update_member_password': { action: 'Member#update_member_password', method: 'post' },
        '/member/authentication': { action: 'Member#authentication', method: 'post' },
        '/member/authentication_with_pass': { action: 'Member#authentication_with_pass', method: 'post' },
        '/member/authentication_with_wsdl': { x: xmlparser({ trim: false, explicitArray: false }), action: 'Member#authentication_with_wsdl', method: 'post' },
        '/member/authentication_with_phone': { action: 'Member#authentication_with_phone', method: 'post' },
        '/member/create': { action: 'Member#create', method: 'post' },
        '/member/test_edw': { action: 'Member#test_edw', method: 'post' },
        '/member/check_phone': { action: 'Member#check_phone', method: 'post' },

        '/program/list': { action: 'Program#list', method: 'post' },
        '/exchange/list': { action: 'Exchange#list', method: 'post' },
        '/exchange/proccess': { action: 'Exchange#proccess', method: 'post' },
        '/exchange/grandprize': { action: 'Exchange#grandprize', method: 'post' },
        '/exchange/proccess_wsdl': { action: 'Exchange#proccess_wsdl', method: 'post' },
        '/exchange/listAll': { action: 'Exchange#listAll', method: 'post' },

        '/merchant/list': { action: 'Merchant#list', method: 'post' },
        '/merchant/list_by_item': { action: 'Merchant#list_by_item', method: 'post' },
        '/merchant/create_item': { action: 'Merchant#create_item', method: 'post' },
        '/merchant/create_category': { action: 'Merchant#create_category', method: 'post' },

        '/redeem/list': { action: 'Redeem#list', method: 'post' },
        '/redeem/proccess': { action: 'Redeem#proccess', method: 'post' },
        '/redeem/purchase_giift': { action: 'Redeem#purchase_giift', method: 'post' },
        '/redeem/process_giift': { action: 'Redeem#process_giift', method: 'post' },
        '/redeem/process_giift2': { action: 'Redeem#process_giift2', method: 'post' },
        // '/redeem/process_giift_v2'  : {action: 'Redeem#process_giift_v2', method:'post'},
        '/redeem/get_giift_list': { action: 'Redeem#get_giift_list', method: 'get' },
        '/redeem/voucher_history': { action: 'Redeem#voucher_history', method: 'post' },
        '/event/list': { action: 'Event#list', method: 'get' },
        // '/event/create' 		      : {action : 'Event#create', method :'post'},
    }
});

var port = conf_core_sys.etc.port;
var portMember = conf_core_sys.etc.portMember;
var portRedeem = conf_core_sys.etc.portRedeem;
var portExchange = conf_core_sys.etc.portExchange;
var portLog = conf_core_sys.etc.portLog;
var portSoap = conf_core_sys.etc.portSoap;

//============== start all service with its own server base on port=============    //
try {
    console.log("=================app start ==============");
    app.listen(port, '0.0.0.0', function() {

        console.log('app all non gate listening on *:' + port);

    });
    app.on('close', function() {
        console.log("app close");
        sql.con.end(function(error) {
            console.log("sql close");
            if (error) throw error;

        });
        sql2.con.end(function(error) {
            console.log("sql2 close");
            if (error) throw error;

        });
    });
    console.log('API Start On PORT  ' + port);

    //member
    memberService.listen(portMember, '0.0.0.0', function() {

        console.log('memberService listening on *:' + portMember);

    });
    memberService.on('close', function() {
        console.log("memberService close");
        sql.con.end(function(error) {
            console.log("memberService sql close");
            if (error) throw error;

        });
        sql2.con.end(function(error) {
            console.log("memberService sql2 close");
            if (error) throw error;

        });
    });

    //log
    logService.listen(portLog, '0.0.0.0', function() {

        console.log('logService listening on *:' + portLog);

    });
    logService.on('close', function() {
        console.log("logService close");
        sql.con.end(function(error) {
            console.log("logService sql close");
            if (error) throw error;

        });
        sql2.con.end(function(error) {
            console.log("logService sql2 close");
            if (error) throw error;

        });
    });

    //exchange
    exchangeService.listen(portExchange, '0.0.0.0', function() {

        console.log('exchangeService listening on *:' + portExchange);

    });

    exchangeService.on('close', function() {
        console.log("exchangeService close");
        sql.con.end(function(error) {
            console.log(" exchangeService sql close");
            if (error) throw error;

        });
        sql2.con.end(function(error) {
            console.log("exchangeService sql2 close");
            if (error) throw error;

        });
    });

    //redeem
    redeemService.listen(portRedeem, '0.0.0.0', function() {

        console.log('redeemService listening on *:' + portRedeem);

    });
    redeemService.on('close', function() {
        console.log("redeemService close");
        sql.con.end(function(error) {
            console.log("redeemService sql close");
            if (error) throw error;

        });
        sql2.con.end(function(error) {
            console.log("redeemService sql2 close");
            if (error) throw error;

        });
    });



    //============== soap server =============    //
    http.listen(portSoap, '0.0.0.0', function() {
        console.log("server soap port = " + portSoap + " baru");
        soap.listen(http, '/wsdl', myService, xml);
    });
    http.on('close', function() {
        console.log("appSoap close");
        sql.con.end(function(error) {
            console.log("sql close");
            if (error) throw error;

        });
        sql2.con.end(function(error) {
            console.log("sql2 close");
            if (error) throw error;

        });
    });
    console.log('appSoap Start On PORT ' + portSoap + ' baru');
    //end soap server

    //publish
    var zmq = require('zeromq'),
        sock = zmq.socket('pub'),
        sub = zmq.socket('sub');

    sock.bindSync('tcp://127.0.0.1:3002');
    console.log('Publisher bound to port 3002');

    // var interval = setInterval(function(){
    // //console.log('sending a multipart message envelope');
    // sock.send(['cron_process_start', 'process']);

    // }, 5000);
    //subscribe to server edw
    sub.connect('tcp://127.0.0.1:3003');
    sub.subscribe('edw_process_start');

    sub.subscribe('edw_process_insert_balance');
    sub.subscribe('edw_process_insert_mob');
    sub.subscribe('edw_process_insert_BIKOLE');
    sub.subscribe('edw_process_insert_aft');
    sub.subscribe('edw_process_insert_agf');
    sub.subscribe('edw_process_insert_ib');
    sub.subscribe('edw_process_insert_DDDHIS');
    sub.subscribe('edw_process_insert_OPEN_ACC');
    sub.subscribe('edw_process_insert_akd');
    sub.subscribe('edw_process_insert_tran_debit');

    sub.on('message', function(topic, message) {

        switch (topic.toString()) {
            case 'cron_process_start':
                if (message.toString() === 'process') {
                    console.log(moment().format('Y-M-D HH:mm:ss') + " servermicro  start process msg diterima edw")
                        //process_start();
                } else if (message.toString() === 'done') {
                    console.log(moment().format('Y-M-D HH:mm:ss') + " servermicro  done process msg diterima edw")
                        //clearInterval(interval);
                } else {

                }
                break;
            case 'edw_process_insert_balance':
                if (message.toString() === 'process') {
                    console.log(moment().format('Y-M-D HH:mm:ss') + " edw  start process msg diterima servermicro")
                } else if (message.toString() === 'done') {
                    //clearInterval(interval);
                    console.log(moment().format('Y-M-D HH:mm:ss') + " done edw_process_insert_balance msg diterima servermicro")
                    var myInt = setInterval(function() {
                        sock.send(['process_insert_mob', 'process']);
                        clearTimeout(myInt);
                    }, 30000);
                    // sock.send(['process_insert_mob', 'process']);
                } else {

                }
                break;
            case 'edw_process_insert_mob':
                if (message.toString() === 'process') {
                    //process_insert_mob();
                    console.log(moment().format('Y-M-D HH:mm:ss') + " process edw_process_insert_mob diterima servermicro")
                } else if (message.toString() === 'done') {
                    //clearInterval(interval);
                    console.log(moment().format('Y-M-D HH:mm:ss') + " done edw_process_insert_mob msg diterima servermicro")
                    var myInt = setInterval(function() {
                        sock.send(['process_insert_BIKOLE', 'process']);
                        clearTimeout(myInt);
                    }, 60000);
                    // sock.send(['process_insert_BIKOLE', 'process']);
                } else {

                }
                break;
            case 'edw_process_insert_BIKOLE':
                if (message.toString() === 'process') {
                    console.log(moment().format('Y-M-D HH:mm:ss') + " proses edw_process_insert_BIKOLE diterima servermicro")
                } else if (message.toString() === 'done') {
                    console.log(moment().format('Y-M-D HH:mm:ss') + " done edw_process_insert_BIKOLE diterima servermicro")
                    var myInt = setInterval(function() {
                        sock.send(['process_insert_aft', 'process']);
                        clearTimeout(myInt);
                    }, 30000);
                    // sock.send(['process_insert_aft', 'process']);
                } else {

                }
                break;
            case 'edw_process_insert_aft':
                if (message.toString() === 'process') {
                    console.log(moment().format('Y-M-D HH:mm:ss') + " proses edw_process_insert_aft diterima servermicro")
                } else if (message.toString() === 'done') {
                    //clearInterval(interval);
                    console.log(moment().format('Y-M-D HH:mm:ss') + " done edw_process_insert_aft diterima servermicro")
                    var myInt = setInterval(function() {
                        sock.send(['process_insert_agf', 'process']);
                        clearTimeout(myInt);
                    }, 15000);
                    // sock.send(['process_insert_agf', 'process']);
                } else {

                }
                break;
            case 'edw_process_insert_agf':
                if (message.toString() === 'process') {
                    console.log(moment().format('Y-M-D HH:mm:ss') + " proses edw_process_insert_agf diterima servermicro")
                } else if (message.toString() === 'done') {
                    console.log(moment().format('Y-M-D HH:mm:ss') + " done edw_process_insert_agf diterima servermicro")
                    var myInt = setInterval(function() {
                        sock.send(['process_insert_ib', 'process']);
                        clearTimeout(myInt);
                    }, 15000);
                    // sock.send(['process_insert_ib', 'process']);
                } else {

                }
                break;
            case 'edw_process_insert_ib':
                if (message.toString() === 'process') {
                    console.log("proses edw_process_insert_ib diterima servermicro")
                } else if (message.toString() === 'done') {
                    console.log(moment().format('Y-M-D HH:mm:ss') + " done edw_process_insert_ib diterima servermicro")
                    var myInt = setInterval(function() {
                        sock.send(['process_insert_DDDHIS', 'process']);
                        clearTimeout(myInt);
                    }, 15000);
                    // sock.send(['process_insert_DDDHIS', 'process']);
                } else {

                }
                break;
            case 'edw_process_insert_DDDHIS':
                if (message.toString() === 'process') {
                    console.log(moment().format('Y-M-D HH:mm:ss') + " proses edw_process_insert_DDDHIS diterima servermicro")
                } else if (message.toString() === 'done') {
                    console.log(moment().format('Y-M-D HH:mm:ss') + " done edw_process_insert_DDDHIS diterima servermicro")
                    var myInt = setInterval(function() {
                        sock.send(['process_insert_OPEN_ACC', 'process']);
                        clearTimeout(myInt);
                    }, 30000);
                    // sock.send(['process_insert_OPEN_ACC', 'process']);
                } else {

                }
                break;
            case 'edw_process_insert_OPEN_ACC':
                if (message.toString() === 'process') {
                    console.log(moment().format('Y-M-D HH:mm:ss') + " proses edw_process_insert_OPEN_ACC diterima servermicro")
                } else if (message.toString() === 'done') {
                    console.log(moment().format('Y-M-D HH:mm:ss') + " done edw_process_insert_OPEN_ACC diterima servermicro")
                    var myInt = setInterval(function() {
                        sock.send(['process_insert_akd', 'process']);
                        clearTimeout(myInt);
                    }, 15000);
                    // sock.send(['process_insert_akd', 'process']);
                } else {

                }
                break;
            case 'edw_process_insert_akd':
                if (message.toString() === 'process') {
                    console.log(moment().format('Y-M-D HH:mm:ss') + " proses edw_process_insert_akd diterima servermicro")
                } else if (message.toString() === 'done') {
                    console.log(moment().format('Y-M-D HH:mm:ss') + " done edw_process_insert_akd diterima servermicro")
                    var myInt = setInterval(function() {
                        sock.send(['process_insert_tran_debit', 'process']);
                        clearTimeout(myInt);
                    }, 15000);
                    // sock.send(['process_insert_tran_debit', 'process']);
                } else {

                }
                break;
            case 'edw_process_insert_tran_debit':
                if (message.toString() === 'process') {
                    console.log(moment().format('Y-M-D HH:mm:ss') + " proses edw_process_insert_tran_debit diterima servermicro")
                } else if (message.toString() === 'done') {
                    console.log(moment().format('Y-M-D HH:mm:ss') + " done edw_process_insert_tran_debit diterima servermicro")
                    console.log(moment().format('Y-M-D HH:mm:ss') + " ======== <br> all process done<br>  ==========")
                        //start execute serverMicro;
                        //core.process_member(); //ini yg dimatiin
                        // core.process_member_raw(); 
                    var myInt = setInterval(function() {
                        // core.aktifasi_tbl();
                        core.process_member_raw();
                        clearTimeout(myInt);
                    }, 20000);
                    // core.aktifasi_tbl();
                } else {

                }
                break;

            default:

                break;
        }
    })


    cronRaw.schedule(conf_core_sys.cronSchedule.raw, function() { //every 1 minute
        console.log(moment().format('Y-M-D HH:mm:ss') + " cronRaw start seteiap 1 menit");
        // var currdate=moment().format('YYYY-MM-DD');
        // coreModel.get_program_period(function(rows){
        //     var start_date = moment(rows[0].start_date, 'YYYY-MM-DD').format('YYYY-MM-DD');
        //     var end_date = moment(rows[0].end_date, 'YYYY-MM-DD').format('YYYY-MM-DD');
        //     console.log(moment().format('Y-M-D HH:mm:ss') +" Program Period: from "+start_date+" to "+end_date);
        //     if (start_date <= currdate) { // && currdate <= end_date) {
        //process point generation
        sock.send(['process_insert_balance', 'process']);
        //     } else {
        //         console.log(moment().format('Y-M-D HH:mm:ss') +" Not in program period. No point generation.");
        //     }
        // });

    });

    var cronBalance = require('node-cron');
    cronBalance.schedule(conf_core_sys.cronSchedule.balance, function() { //every 1 minute
        var currdate = moment().format('YYYY-MM-DD');
        console.log(moment().format('Y-M-D HH:mm:ss') + " cronBalance start seteiap 1 bulan");
        coreModel.get_program_period(function(rows) {
            var start_date = moment(rows[0].start_date, 'YYYY-MM-DD').format('YYYY-MM-DD');
            var end_date = moment(rows[0].end_date, 'YYYY-MM-DD').format('YYYY-MM-DD');
            console.log(moment().format('Y-M-D HH:mm:ss') + " Program Period: from " + start_date + " to " + end_date);
            if (start_date <= currdate) { // && currdate <= end_date) {
                //process point generation
                core.aktifasi_balance();
            } else {
                console.log(moment().format('Y-M-D HH:mm:ss') + " Not in program period. No point generation.");
            }
        });
    });

    var cronSpEvent = require('node-cron');
    cronSpEvent.schedule(conf_core_sys.cronSchedule.spEvent, function() { //every 1 minute
        console.log(moment().format('Y-M-D HH:mm:ss') + " cronSpEvent start");
        var currdate = moment().format('YYYY-MM-DD');
        coreModel.get_program_period(function(rows) {
            var start_date = moment(rows[0].start_date, 'YYYY-MM-DD').format('YYYY-MM-DD');
            var end_date = moment(rows[0].end_date, 'YYYY-MM-DD').format('YYYY-MM-DD');
            console.log(moment().format('Y-M-D HH:mm:ss') + " Program Period: from " + start_date + " to " + end_date);
            if (start_date <= currdate) { // && currdate <= end_date) {
                //process point generation
                // core.process_special_event();
                core.process_special_event_new();
            } else {
                console.log(moment().format('Y-M-D HH:mm:ss') + " Not in program period. No point generation.");
            }
        });
    });

    var cronDobEvent = require('node-cron');
    cronDobEvent.schedule(conf_core_sys.cronSchedule.dobEvent, function() { //every 1 minute
        console.log(moment().format('Y-M-D HH:mm:ss') + " cronDobEvent start ");
        var currdate = moment().format('YYYY-MM-DD');
        coreModel.get_program_period(function(rows) {
            var start_date = moment(rows[0].start_date, 'YYYY-MM-DD').format('YYYY-MM-DD');
            var end_date = moment(rows[0].end_date, 'YYYY-MM-DD').format('YYYY-MM-DD');
            console.log(moment().format('Y-M-D HH:mm:ss') + " Program Period: from " + start_date + " to " + end_date);
            if (start_date <= currdate) { // && currdate <= end_date) {
                //process point generation
                core.process_birthday_event();
            } else {
                console.log(moment().format('Y-M-D HH:mm:ss') + " Not in program period. No point generation.");
            }
        });
    });

    cronGiift.schedule(conf_core_sys.cronSchedule.giift, function() { //every 1 minute
        console.log(moment().format('Y-M-D HH:mm:ss') + " cronGiift start");
        core.processGiiftList();
        // core.processMergeMerchantBtn();
        // coreModel.merge_voucher_list2(function(rows){
        //     console.log(rows);
        // });
    });

    // task 2.1.2 by feri, start 29/8/2019
    cronPointRemoval.schedule(conf_core_sys.cronSchedule.tiap_bulan, function() {
        console.log(moment().format('Y-M-D HH:mm:ss') + " cronPointRemoval start");
        // core.processPenghapusanPoint();
        core.processPointRemoval();
    });

    cronGiift.schedule(conf_core_sys.cronSchedule.giift, function() { //every 1 minute
        console.log(moment().format('Y-M-D HH:mm:ss') + " cronGiift start");
        // core.processGiiftList();
        core.processMergeMerchantBtn();
        // coreModel.merge_voucher_list2(function(rows){
        //     console.log(rows);
        // });
    });

    cronOTP.schedule(conf_core_sys.cronSchedule.reset_fail_otp, function() { //every 1 minute
        console.log(moment().format('Y-M-D HH:mm:ss') + " cronOTP start");
        coreModel.reset_fail_otp();
    });

    cronBurn.schedule(conf_core_sys.cronSchedule.burn_point, function() { //every 1 minute
        console.log(moment().format('Y-M-D HH:mm:ss') + " cronBurn start");
        core.process_burn_point();
    });



    //============== cronjob =============
    //var valid = cronDOB.validate('20 6 * * *');
    //console.log("cron valid ? "+valid);
    //cronDOB.schedule('* * * * *', function(){

    // cronDOB.schedule('22 6 * * *', function(){
    //         //console.log('running a task every minute');
    //         var offset =0;
    //         var limit =1;
    //         var currdate=moment().format('YYYY-MM-DD');

    //         var check = moment(currdate, 'YYYY-MM-DD');
    //         var month = check.format('MM');
    //         var day   = check.format('DD');
    //         var year  = check.format('YYYY');
    //         var dob = ""+month+"-"+day;
    //         member.list_dob(dob, function(rows){           
    //         if(rows.length > 0){
    //             response.status_200("Success", rows, function(cb){
    //             console.log("Success = "+cb);
    //             console.dir(rows);
    //             var member=rows;
    //             var member_id=member[0].id;
    //             console.log("Success member_id= "+member_id);
    //             var member_arr=[];
    //             rows.forEach(function(item) {
    //                 member_arr.push(item.id);
    //             });
    //             console.log("member_arr "+member_arr);
    //             });
    //         }else{
    //             response.status_204("Failed", "Data not found", function(cb){
    //             console.log("Failed = "+cb);
    //             console.dir(cb);
    //             }); 
    //         }         
    //         });

    // });

    /*
    //check member raw data & update
    //var options = {sql: 'insert into M_member (first_name, last_name, email, password, cif, phone, rekening, registered_dated) values ("'+first_name+'", "'+last_name+'", "'+email+'", "'+password+'", "'+cif+'", '+phone+', '+rekening+', "'+moment().format('Y-M-D h:mm:ss')+'")'};
        
        
    cronMemberRaw.schedule('22 0 * * *', function(){
            console.log('running a task every midnite');
           // core.process_member_raw();
        
    });
    */


    //check expire on 00 midnight
    // 5-Mar-2019: cron expire dinonaktifkan karena sekarang expire poin berdasarkan burn date (berakhirnya masa periode):
    // cronEX.schedule(conf_core_sys.cronSchedule.expire, function(){
    // //    core.process_expire_point();
    //         //cronEX.schedule('* * * * *', function(){
    //         //console.log('running a task every minute');
    //         var offset =0;
    //         var limit =1;
    //         var currdate=moment().format('YYYY-MM-DD');

    //         var check = moment(currdate, 'YYYY-MM-DD');
    //         var month = check.format('MM');
    //         var day   = check.format('DD');
    //         var year  = check.format('YYYY');
    //         var dob = ""+month+"-"+day;
    //         log.expire_point(currdate, function(result){           
    //             if(result.length > 0){
    //                 response.status_200("Success", result, function(cb){
    //                 console.log("Success = "+cb);
    //                 console.dir(result);
    //                 // var member=result;
    //                 // var member_id=member[0].id;
    //                 // console.log("Success member_id= "+member_id);
    //                 // var member_arr=[];
    //                 // rows.forEach(function(item) {
    //                 //     member_arr.push(item.id);
    //                 // });
    //                 // console.log("member_arr "+member_arr);
    //                 //core.test();
    //                 var myInt = setInterval(function () {
    //                     core.process_expire_point();
    //                     clearTimeout(myInt);
    //                 }, 15000);
    //                 });
    //             }else{
    //                 response.status_204("Failed", "Data not found", function(cb){
    //                 console.log("Failed = "+cb);
    //                 console.dir(cb);
    //                 var myInt = setInterval(function () {
    //                     core.process_expire_point();
    //                     clearTimeout(myInt);
    //                 }, 15000);
    //                 }); 
    //             }         
    //         });

    // });

    //core.process_expire_point();

    // core.test();//jalan akhir untuk total poin 


    //===run core suppose from cron

    //rule_code ='agf'
    //path = '/var/www/html/btn_point_loyalty/db_update_agf.csv';  
    //core.update_total_point_perday()
    //core.process_poin_perday();
    //core.process_expire_point()//get expire point per day
    //core.process_m_poin_expire(rule_code, path)
    // var date2506="2018-06-25 00:00:00";
    // var dateStart=date2506;//moment().subtract(8, 'day').format('Y-M-D h:mm:ss')
    // var dateEnd= moment().add(1, 'day').format('Y-M-D h:mm:ss')
    // var flag="";
    // path="";
    // coreModel.update_rek(flag, path, dateStart, dateEnd );
    // 
    //=============================//run process data//=====================

    //core.test();//jalan akhir untuk total poin 

    // core.process_member_raw(); //buat test 

    // core.process_member()//initiate member
    // coreModel.process_map_cif_rek_start();
    // coreModel.process_map_cif_phone_start();
    // core.process_cif_filter_by_phone();// acc ganda populTE 
    // core.prepare_prog_join();

    //ini bisa berulang dalam satu akun valid per transaksi
    // core.aktifasi_tbl(); //1 1004
    //    core.aktifasi_dbol(); //2 1002     
    // core.aktifasi_ptln(); //3 1003 //15000 
    //core.aktifasi_ppc(); //4 1001
    // core.aktifasi_tcrka();//5 1006 
    // core.aktifasi_setor();//6 1005   

    //core.aktifasi_agf(); //7 
    //core.aktifasi_aft(); //8   
    // core.aktifasi_akd(); //9
    // core.aktifasi_aib(); //10   
    //core.aktifasi_openAcc(); //11

    // core.aktifasi_balance(); //12 bulanan di start di tgl 27 ga perlu diulang
    // core.aktifasi_mob()//13

    // core.process_special_event();
    // core.process_birthday_event();
    // core.process_update_mpj('tbl_update');
    // core.process_member_new();
    // coreModel.process_map_cif_phone_new();
    // coreModel.update_rek_expire('2018-11-15','2015-11-17');
    // core.process_burn_point();

    // core.testEncDec('testing');
    // core.testDecrypt('c7d7e21fb06cc5095ef4b7d6e766dcee:e95286849934ea5ae1f1095fadc90192');
    // sock.send(['process_insert_balance', 'process']);
    // core.getGiiftToken();
    // core.getGiiftList();
    // core.processGiiftList();
    // core.sendEmailVoucher("BTN636-1547019849626","indrasaputra@gmail.com", null);
    // core.sendEmail("indrasaputra@gmail.com", "testing");
    // coreModel.reset_fail_otp();
    // core.process_dynamic_rule_code();




    //=============================//end run process data//=====================

} catch (e) {
    console.log("Error : Test\n" + e);
    var port2 = port;
    app.listen(port2);
    memberService.listen(port2);
    console.log('API Start On PORT  ' + port2);
    //}
}